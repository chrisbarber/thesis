function test_set=get_test_set(experiment,fold_idx)

%include('data/');

test_set=get_fold_data(experiment,fold_idx);

end

