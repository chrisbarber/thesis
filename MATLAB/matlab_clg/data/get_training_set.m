function training_set=get_training_set(experiment,fold_idx)

%include('util/');
%include('data/');

training_set_folds=1:experiment.num_folds;
training_set_folds(fold_idx)=[];
timesteps=unique(sort(cell2mat(cellfun(@ravel,ravel(experiment.folds(training_set_folds)),'UniformOutput',0))));
timesteps=setdiff(timesteps,experiment.folds{fold_idx});
temp_experiment=experiment;
temp_experiment.folds={timesteps};
training_set=get_fold_data(temp_experiment,1);
training_set.fold_idx=fold_idx;

end

