% Return row indexes of variables in fold_data or window by site, unit, etc.
% Note that for any value except wildcard, nan's are not included.
%
% * [] or {} is wildcard
% * can specify an individual value or a list e.g.
%   * site - 2 or [2 3]
%   * units - 'ew' or {'ew','ns'}


function idx=get_var_idx(data,experiment,site,units,is_deterministic,deterministic_idx)

idx=(1:data.N);

if ~isempty(site)
    site_idx=find(ismember(data.site,site));
    idx=intersect(idx,site_idx);
end

if ~isempty(units)
    if iscell(units)
        units_labels=[];
        for i=1:length(units)
            units_labels=union(units_labels,strmatch(units{i},experiment.station_variables));
        end
    else
        units_labels=strmatch(units,experiment.station_variables);
    end

    units_idx=find(ismember(data.units,units_labels));
    idx=intersect(idx,units_idx);
end

if ~isempty(is_deterministic)
    is_deterministic_idx=find(ismember(data.is_deterministic,is_deterministic));
    idx=intersect(idx,is_deterministic_idx);
end

if~isempty(deterministic_idx)
    deterministic_idx_idx=find(ismember(data.deterministic_idx,deterministic_idx));
    idx=intersect(idx,deterministic_idx_idx);
end

end

