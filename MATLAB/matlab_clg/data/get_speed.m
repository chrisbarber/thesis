% given fold_data, predictions, or window, return a structure of the same type populated
% w/ speed values as computed from u & v.  if speed is already present in the input, this
% function returns those rows in a structure.

function speed=get_speed(fold_data,experiment)

if isempty(strmatch('spd',experiment.station_variables))
    speed.d=[];
    speed.is_deterministic=[];
    speed.site=[];
    speed.units=[];
    speed.deterministic_idx=[];

    % extract u,v components and convert to speed, if necessary

    u_unit_idx=strmatch('ew',experiment.station_variables);
    v_unit_idx=strmatch('ns',experiment.station_variables);

    u_idx=find(u_unit_idx==fold_data.units);
    v_idx=find(v_unit_idx==fold_data.units);

    if 1==numel(u_unit_idx) && 1==numel(v_unit_idx)
        sites=my_unique(fold_data.site);
        sites=sites(~isnan(sites));
        for i=1:length(sites)
            site_idx=find(sites(i)==fold_data.site);
            site_u_idx=my_intersect(site_idx,u_idx);
            site_v_idx=my_intersect(site_idx,v_idx);
            assert(1==numel(site_u_idx) && 1==numel(site_v_idx));

            % calculate and append speed
            site_speed=sum(fold_data.d([site_u_idx site_v_idx],:).^2,1).^0.5;
            speed.d=[speed.d; site_speed];

            speed.is_deterministic=[speed.is_deterministic; 0];
            speed.site=[speed.site; sites(i)];
            speed.units=[speed.units; nan];
            speed.deterministic_idx=[speed.deterministic_idx; nan];
        end
    else
        error('should be only 1 ew and ns variable per station');
    end

    speed.N=size(speed.d,1);
    speed.T=size(speed.d,2);
    speed.timesteps=fold_data.timesteps;
else
    spd_unit_idx=strmatch('spd',experiment.station_variables);
    spd_idx=find(spd_unit_idx==fold_data.units);

    speed=fold_data;
    speed.N=numel(spd_idx);
    speed.d=fold_data.d(spd_idx,:);
    speed.is_deterministic=fold_data.is_deterministic(spd_idx);
    speed.site=fold_data.site(spd_idx);
    speed.units=fold_data.units(spd_idx);
    speed.deterministic_idx=fold_data.deterministic_idx(spd_idx);
end

end

