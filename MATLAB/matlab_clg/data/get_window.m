function window=get_window(data,window_length,horizon,window_serial_idx)

interval_idx=find(window_serial_idx>=data.interval_serial_idx,1,'last');
interval_offset=window_serial_idx-data.interval_serial_idx(interval_idx);
window_start=data.interval_starts(interval_idx)+interval_offset;

window.timesteps=[window_start:window_start+window_length-1 window_start+window_length+horizon-1];
[junk,junk,relative_idx]=my_intersect(window.timesteps,data.timesteps);
window.d=data.d(:,relative_idx);
window.N=data.N;
window.T=window_length+1;
window.is_deterministic=data.is_deterministic;
window.site=data.site;
window.units=data.units;
window.deterministic_idx=data.deterministic_idx;

end

