function results=test(seed)

% TODO: move seed into cv()?
rand('state',seed);
randn('state',seed);

include('model/');
include('util/');
include('em/');
include('experiment/');
include('inference/');
include('data/');
include('baseline/');

%load('ncdc-ALL21-WI.mat');

% Set up model

%names=ncdc.stations(1,1)';
%spd_or_uv='uv';
%num_deterministic=0;
num_regimes=4;
window_length=3;
markov_order=1;
horizon=1;

model_config=new_config(num_regimes,window_length,markov_order,horizon);

% Set up experiment

folds={[],[],[],[],[],[],[],[],[],[]};
fold=1;
start_time=1;
while start_time<=47474
    this_chunk=(start_time:(start_time+7*24-1))';
    this_chunk=this_chunk(this_chunk<=47474);

    folds{fold}=[folds{fold}; this_chunk];

    start_time=start_time+7*24;

    fold=fold+1;
    if fold>10
        fold=1;
    end
end


root='C:/Users/Chris/Documents/MATLAB/matlab_clg/';
data_src='ncdc-ALL21-WI.mat';
variable_name='ncdc';
rand_seed=seed;
stations=[2:11 14:22];
%stations=9;
num_station_variables=2;
station_variables={'ew','ns'};
station_joint={[1 2]};
global_joint={};
deterministic_idx=[];
num_stations=numel(stations);
num_deterministic=0;
startdate=[2004 8 1 0 0 0];
enddate=[2009 12 31 0 0 0];
%num_folds=4;
num_folds=10;
%folds={3747:12506,12507:21266,21267:30026,30027:38786};
%folds={1:100,101:200};
em_config.random_restarts=1;
em_config.convergence=0.001;
em_config.max_iterations=100;
em_config.random_param_variance=0.01;
em_config.regime_pseudocount=0;
em_config.variance_pseudocount=2;
em_config.algorithm='junction_tree';
predict.point_prediction_method='expected_value';
baseline.model='persistent_gaussian';

experiment=new_experiment(root,data_src,variable_name,model_config,rand_seed,num_stations,num_deterministic,stations,num_station_variables,station_variables,station_joint,global_joint,deterministic_idx,startdate,enddate,num_folds,folds,em_config,predict,baseline);
experiment.queue='./queue.txt';

model=new_model(experiment);

% Run CV experiment

results=cv(model,experiment);

end

