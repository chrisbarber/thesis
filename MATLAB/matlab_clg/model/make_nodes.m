function [nodes by_slice ess_nodes window]=make_nodes(experiment)

ess_nodes=[]; % nodes for which sufficient statistics need to be computed

empty_node=struct('regime',[],'tstep',[],'site',[],'units',[],'node',[],'sz',[],'vars',[],'joint',[],'pa_nodes',[],'pa_vars',[],'pa_joint',[],'pa_n',[],'pa_sz',[],'source_nodes',[],'is_deterministic',[]);

node_label=1;
var_label=1;
nodes=[];

for r=1:experiment.model_config.num_regimes
    for t=1:experiment.model_config.window_length+1

        if 0<experiment.num_deterministic
            % create deterministic node
            new_node=empty_node;
            new_node.regime=r;
            new_node.tstep=t;

            new_node.node=node_label;
            node_label=node_label+1;
            new_node.sz=experiment.num_deterministic;
            new_node.vars=(var_label:var_label+new_node.sz-1)';
            var_label=var_label+new_node.sz;
            new_node.joint=repmat(new_node.node,[new_node.sz 1]);

            new_node.site=nan([new_node.sz 1]);
            new_node.units=nan([new_node.sz 1]);
            new_node.deterministic_idx=(1:experiment.num_deterministic)';

            new_node.pa_nodes=[];
            new_node.pa_vars=[];
            new_node.pa_joint=[];
            new_node.pa_n=0;
            new_node.pa_sz=0;

            new_node.source_nodes=[];

            new_node.is_deterministic=1;

            nodes=[nodes; new_node];
        end


        % create rv nodes

        for i=1:length(experiment.station_joint)
            for j=1:experiment.num_stations
                new_node=empty_node;
                new_node.regime=r;
                new_node.tstep=t;
                new_node.is_deterministic=0;

                % set node index
                new_node.node=node_label;
                node_label=node_label+1;

                % allocate random variables
                new_node.sz=numel(experiment.station_joint{i});
                new_node.vars=(var_label:(var_label+new_node.sz-1))';
                var_label=var_label+new_node.sz;

                % specify site and station-variable for each var
                new_node.site=repmat(j,[new_node.sz 1]);
                new_node.units=ravel(experiment.station_joint{i});
                new_node.deterministic_idx=nan([new_node.sz 1]);

                % all vars are joint
                new_node.joint=repmat(new_node.node,[new_node.sz 1]);

                nodes=[nodes; new_node];
            end
        end

        for i=1:length(experiment.global_joint)
            new_node=empty_node;
            new_node.regime=r;
            new_node.tstep=t;
            new_node.is_deterministic=0;

            % set node index
            new_node.node=node_label;
            node_label=node_label+1;

            % allocate random variables
            new_node.sz=experiment.num_stations*numel(experiment.global_joint{i});
            new_node.vars=(var_label:(var_label+new_node.sz-1))';
            var_label=var_label+new_node.sz;

            % specify site and station-variable for each var
            new_node.site=repmat((1:experiment.num_stations)',[numel(experiment.global_joint{i}) 1]);;
            new_node.units=ravel(repmat(experiment.global_joint{i}(:),[1 experiment.num_stations]));
            new_node.deterministic_idx=nan([new_node.sz 1]);

            % all vars are joint
            new_node.joint=repmat(new_node.node,[new_node.sz 1]);

            nodes=[nodes; new_node];
        end
    end
end

% set parents for non-deterministic nodes

for i=1:length(nodes)
    if ~nodes(i).is_deterministic
        nodes(i).pa_nodes=[];
        nodes(i).pa_vars=[];
        nodes(i).pa_joint=[];
        for j=1:length(nodes)
            if(nodes(i).regime==nodes(j).regime)
                if (~nodes(j).is_deterministic && (nodes(i).tstep-nodes(j).tstep)>=1 && (nodes(i).tstep-nodes(j).tstep)<=experiment.model_config.markov_order) ...
                || (nodes(j).is_deterministic && nodes(i).tstep==nodes(j).tstep)
                    nodes(i).pa_nodes=[nodes(i).pa_nodes; nodes(j).node];
                    nodes(i).pa_vars=[nodes(i).pa_vars; nodes(j).vars];
                    nodes(i).pa_joint=[nodes(i).pa_joint; nodes(j).joint];
                end
            end
        end
        nodes(i).pa_n=numel(nodes(i).pa_nodes);
        nodes(i).pa_sz=numel(nodes(i).pa_vars);
    end
end

% create data structure allowing nodes to be accessed by regime/tstep

by_slice=cell([experiment.model_config.num_regimes experiment.model_config.window_length+1]);
for i=1:length(nodes)
    r=nodes(i).regime;
    t=nodes(i).tstep;

    by_slice{r,t}=[by_slice{r,t}; i];
end

% go through and set the sufficient statistics source nodes

for i=1:length(nodes)
    if ~nodes(i).is_deterministic
        if t==1
            nodes(i).source_nodes=nodes(i).node;
        elseif t==experiment.model_config.window_length+1
            nodes(i).source_nodes=nodes(i).node;
        else
            % same node, but from a different timeslice

            if 1==experiment.model_config.horizon
                last_slice=by_slice{nodes(i).regime,experiment.model_config.window_length+1};
                nodes(i).source_nodes=nodes(last_slice(i)).node;
            else
                second_to_last_slice=by_slice{nodes(i).regime,experiment.model_config.window_length};
                nodes(i).source_nodes=nodes(second_to_last_slice(i)).node;
            end
        end
    end
end

% enumerate ess source nodes

ess_nodes=[];
for i=1:length(nodes)
    ess_nodes=[ess_nodes; nodes(i).source_nodes];
end
ess_nodes=my_unique(ravel(ess_nodes));

% create window info

window.T=experiment.model_config.window_length+1;
window.N=0;
window.is_deterministic=[];
window.site=[];
window.units=[];
window.deterministic_idx=[];
some_slice=nodes(by_slice{1,1});
for i=1:length(some_slice)
    window.N=window.N+some_slice(i).sz;
    window.is_deterministic=[window.is_deterministic; repmat(some_slice(i).is_deterministic,[some_slice(i).sz 1])];
    window.site=[window.site; some_slice(i).site];
    window.units=[window.units; some_slice(i).units];
    window.deterministic_idx=[window.deterministic_idx; some_slice(i).deterministic_idx]; 
end

[junk,sort_idx]=sortrows([window.is_deterministic window.site window.units window.deterministic_idx]);
window.is_deterministic=window.is_deterministic(sort_idx);
window.site=window.site(sort_idx);
window.units=window.units(sort_idx);
window.deterministic_idx=window.deterministic_idx(sort_idx);

for r=1:experiment.model_config.num_regimes
    window.vars{r}=get_vars(nodes,by_slice,window,r);
end

end


function window_vars=get_vars(nodes,by_slice,window,regime)

window_vars=nan([window.N window.T]);
for t=1:window.T
    slice_nodes=nodes(by_slice{regime,t});

    for i=1:window.N
        this_var=find_vars(slice_nodes,window.is_deterministic(i),window.site(i),window.units(i),window.deterministic_idx(i));
        assert(1==numel(this_var));
        window_vars(i,t)=this_var;
    end
end

end

function vars=find_vars(slice_nodes,is_deterministic,site,unit,deterministic_idx)

vars=[];
for i=1:length(slice_nodes)
    if is_deterministic
        if slice_nodes(i).is_deterministic
            idx_of_deterministic=find(deterministic_idx==slice_nodes(i).deterministic_idx);
            vars=[vars; slice_nodes(i).vars(idx_of_deterministic)];
        end
    else
        if ~slice_nodes(i).is_deterministic
            idx_of_site=find(site==slice_nodes(i).site);
            idx_of_unit=find(unit==slice_nodes(i).units);

            % return only vars where site and unit match
            vars=[vars; slice_nodes(i).vars(my_intersect(idx_of_site,idx_of_unit))];
        end
    end
end

end

