% query specific variables (implies variable elimination), as opposed to enter_evidence
% query_mask is a logical array the same shape as the window, specifying the query variables

function posterior=query(model,window,query_mask)

posterior=enter_evidence(model,window,'var_elim',query_mask);

end
 
