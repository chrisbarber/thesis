function model = new_model(experiment)

%include('inference/');
%include('util/');

assert(isequal((1:experiment.num_station_variables)',sort(vertcat(cell2mat(ravel(cellfun(@ravel,experiment.station_joint,'UniformOutput',false))),cell2mat(ravel(cellfun(@ravel,experiment.global_joint,'UniformOutput',false)))))));

model.config=experiment.model_config;
model.config.num_deterministic=experiment.num_deterministic;
model.config.num_station_variables=experiment.num_station_variables;
model.config.num_sites=experiment.num_stations;

[model.graph.nodes model.graph.by_slice model.graph.ess_nodes model.graph.window]=make_nodes(experiment);
model.params.clg=make_cpds(model);

% Initialize regime probabilities to uniform
model.params.regime_pr=normalize(zeros([model.config.num_regimes 1]),1);

model=update_factors(model);

end

