function window_vars=get_window_vars(model,window,regime)

[junk,sort_idx]=sortrows([window.is_deterministic window.site window.units window.deterministic_idx]);
reorder_idx(sort_idx)=1:window.N;
window_vars=model.graph.window.vars{regime}(reorder_idx,:);

end

