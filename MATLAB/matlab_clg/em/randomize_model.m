function model = randomize_model(model,experiment)

for i=1:length(model.graph.nodes)
    node=model.graph.nodes(i);
    if ~node.is_deterministic
        clg=model.params.clg(node.regime,node.tstep);
        [junk,junk,idx]=my_intersect(node.vars,clg.vars);
        [junk,junk,pa_idx]=my_intersect(node.pa_vars,clg.pa_vars);

        % randomize means and weights
        
        b0=mvnrnd(0,experiment.em.random_param_variance,node.sz);
        b=reshape(mvnrnd(0,experiment.em.random_param_variance,node.sz*node.pa_sz),[node.sz node.pa_sz]);
        model.params.clg(node.regime,node.tstep).b0(idx)=b0;
        model.params.clg(node.regime,node.tstep).b(idx,pa_idx)=b;

        % randomize covariance

        p=1;
        while p~=0
            s=reshape(mvnrnd(0,experiment.em.random_param_variance,node.sz^2),[node.sz node.sz]);
            s=triu(s,1)+triu(s,1)'+abs(s.*eye(size(s)));
            [junk,p]=chol(s);
        end
        model.params.clg(node.regime,node.tstep).s(idx,idx)=s;
    end
end

model=update_factors(model);

end

