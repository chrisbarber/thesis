function [sx sy sxx syy sxy]=get_ess(node,posterior)

%include('inference/');

% get relevant posterior marginal MVN

if strcmp(posterior.algorithm,'junction_tree')
    % retreive the relevant clique
    clique_idx=max([1 node.tstep-posterior.markov_order]);
    mvn=posterior.cliques_mvn{node.regime}(clique_idx);
elseif strcmp(posterior.algorithm,'naive') || strcmp(posterior.algorithm,'var_elim')
    mvn=posterior.mvn{node.regime};
else
    error(['Unsupported inference algorithm: ' posterior.algorithm]);
end

[sx sy sxx syy sxy]=mex_get_ess(node,mvn,posterior.obs_vars{node.regime},posterior.obs_vals);

% %%%%%%%%%%%%%%%%%%%%%%%%
% 
% x_vars=node.pa_vars;
% y_vars=node.vars;
% 
% % useful intersections & indices
% 
% [x_obs_vars,x_obs_idx,x_obs_val_idx]=my_intersect(x_vars,posterior.obs_vars{node.regime});
% [y_obs_vars,y_obs_idx,y_obs_val_idx]=my_intersect(y_vars,posterior.obs_vars{node.regime});
% [junk,x_mis_idx]=my_setdiff(x_vars,x_obs_vars);
% [junk,y_mis_idx]=my_setdiff(y_vars,y_obs_vars);
% 
% % intitialize sufficient statistics matrices
% 
% sx=nan([node.pa_sz 1]);
% sy=nan([node.sz 1]);
% % covariance will be zero where at least 1 RV is observed
% sxx=zeros([node.pa_sz node.pa_sz]);
% syy=zeros([node.sz node.sz]);
% sxy=zeros([node.pa_sz node.sz]);
% 
% % expected means for observed variables are just the observations
% sx(x_obs_idx)=posterior.obs_vals(x_obs_val_idx);
% sy(y_obs_idx)=posterior.obs_vals(y_obs_val_idx);
% 
% [junk,junk,x_mis_mvn_idx]=my_intersect(x_vars,mvn.vars);
% [junk,junk,y_mis_mvn_idx]=my_intersect(y_vars,mvn.vars);
% 
% % these are just the mus from the posterior
% sx(x_mis_idx)=mvn.b0(x_mis_mvn_idx);
% sy(y_mis_idx)=mvn.b0(y_mis_mvn_idx);
% 
% % find the covariances (if either was observed it is 0 from initialization)
% sxx(x_mis_idx,x_mis_idx)=mvn.s(x_mis_mvn_idx,x_mis_mvn_idx);
% syy(y_mis_idx,y_mis_idx)=mvn.s(y_mis_mvn_idx,y_mis_mvn_idx);
% sxy(x_mis_idx,y_mis_idx)=mvn.s(x_mis_mvn_idx,y_mis_mvn_idx);
% 
% % add E[X_i]*E[X_j] to get the desired statistics E[X_i * X_j]
% sxx=sxx+sx*sx';
% syy=syy+sy*sy';
% sxy=sxy+sx*sy';
% 
% if max(abs(ravel(sx-sx2)))>0.000001
%     sdfskjdh=0;
% end
% if max(abs(ravel(sy-sy2)))>0.000001
%     sdsfiudyf=0;
% end
% if max(abs(ravel(sxx-sxx2)))>0.000001
%     sdfsdf=0;
% end
% if max(abs(ravel(syy-syy2)))>0.000001
%     sdfsudify=0;
% end
% if max(abs(ravel(sxy-sxy2)))>0.000001
%     sdfysdiufy=0;
% end

end

