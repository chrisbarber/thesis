function [ess ll]=e_step(model,data,experiment)

%include('data/');
%include('model/');
%include('util/');

nsamples=data.num_windows;

% initialize ess to zeros / regularization parameters

ess.w=zeros([model.config.num_regimes 1])+experiment.em.regime_pseudocount;
for i=1:length(model.graph.ess_nodes)
    variance_pseudocount=experiment.em.variance_pseudocount;
    node_idx=model.graph.ess_nodes(i);
    ess.nodes(i)=empty_ess(model.graph.nodes(node_idx),variance_pseudocount);
end

% accumulate ess over all window postitions

ll=0;
pool_size=max(1,matlabpool('size'));
t_start=tic;
[experiment,txt]=progress(experiment,'push','e_step');
parfor window_serial_idx=1:nsamples
    t_loop_start=tic;

    window=get_window(data,model.config.window_length,model.config.horizon,window_serial_idx);
    posterior=enter_evidence(model,window,experiment.em.algorithm);
    ess_w=exp(posterior.regime_posterior);
    % compute ess only for nodes which are needed
    ess_nodes=[];
    for i=1:length(model.graph.ess_nodes)
        node_idx=model.graph.ess_nodes(i);
        [sx sy sxx syy sxy]=get_ess(model.graph.nodes(node_idx),posterior);
        w=exp(posterior.regime_posterior(model.graph.nodes(node_idx).regime));
        %ess.nodes(i)=accum_ess(ess.nodes(i),sx,sy,sxx,syy,sxy,w);

        ess_nodes(i).sx=w*sx;
        ess_nodes(i).sy=w*sy;
        ess_nodes(i).sxx=w*sxx;
        ess_nodes(i).syy=w*syy;
        ess_nodes(i).sxy=w*sxy;
    end

    % accumulate expected sufficient statistics (ess is a reduction variable)
    ess=accum_ess(ess,struct('w',ess_w,'nodes',ess_nodes));

    % accumulate log-likelihood (ll is a reduction variable)
    ll=ll+posterior.ll;

    % output progress message
    t_loop_elapsed=toc(t_loop_start);
    t_total_elapsed=toc(t_start);
    t_total=t_loop_elapsed*nsamples/(pool_size);
    t_eta=max(0,t_total-t_total_elapsed);
    eta_str=[num2str(floor(t_eta/3600),'%02d') ':' num2str(floor(mod(t_eta,3600)/60),'%02d')];

    disp([txt 'iteration progress ' num2str(min(100,round(100*t_total_elapsed/t_total))) '%, eta ' eta_str]);
end
experiment=progress(experiment,'pop');

end

function ess=empty_ess(node,variance_pseudocount)

ess.sx=zeros([node.pa_sz 1]);
ess.sy=zeros([node.sz 1]);
ess.sxx=variance_pseudocount*eye([node.pa_sz node.pa_sz]);
ess.syy=variance_pseudocount*eye([node.sz node.sz]);
ess.sxy=zeros([node.pa_sz node.sz]);

end

function ess1=accum_ess(ess1,ess2)

ess1.w=ess1.w+ess2.w;

for i=1:length(ess1.nodes)
    ess1.nodes(i).sx=ess1.nodes(i).sx+ess2.nodes(i).sx;
    ess1.nodes(i).sy=ess1.nodes(i).sy+ess2.nodes(i).sy;
    ess1.nodes(i).sxx=ess1.nodes(i).sxx+ess2.nodes(i).sxx;
    ess1.nodes(i).syy=ess1.nodes(i).syy+ess2.nodes(i).syy;
    ess1.nodes(i).sxy=ess1.nodes(i).sxy+ess2.nodes(i).sxy;
end

end

%function ess2=accum_ess(ess,sx,sy,sxx,syy,sxy,w);
%
%ess2.sx=ess.sx+w*sx;
%ess2.sy=ess.sy+w*sy;
%ess2.sxx=ess.sxx+w*sxx;
%ess2.syy=ess.syy+w*syy;
%ess2.sxy=ess.sxy+w*sxy;
%
%end
