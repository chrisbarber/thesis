/* implements a portion of get_ess.m in mex, for speed */

#include "mex.h"
#include <stdlib.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

    /* check # arguments */

    if(4 != nrhs) {
        mexErrMsgTxt("Wrong number of input arguments");
    }
    if(5 != nlhs) {
        mexErrMsgTxt("Wrong number of output arguments");
    }

    /* get inputs */
    
    mxArray *x_vars = mxGetField(prhs[0], 0, "pa_vars"); /* x_vars=node.pa_vars; */
    mxArray *y_vars = mxGetField(prhs[0], 0, "vars"); /* y_vars=node.vars; */
    mxArray *mis_vars = mxGetField(prhs[1], 0, "vars"); /* mis_vars=mvn.vars; */
    mxArray *mu = mxGetField(prhs[1], 0, "b0"); /* mu = mvn.b0; */
    mxArray *sigma = mxGetField(prhs[1], 0, "s"); /* sigma = mvn.s; */
    const mxArray *obs_vars = prhs[2];
    const mxArray *obs_vals = prhs[3]; /* obs_vals */
    double *px_vars = mxGetPr(x_vars);
    double *py_vars = mxGetPr(y_vars);
    double *pmis_vars = mxGetPr(mis_vars);
    double *pmu = mxGetPr(mu);
    double *psigma = mxGetPr(sigma);
    double *pobs_vars = mxGetPr(obs_vars);
    double *pobs_vals = mxGetPr(obs_vals);
    
    int nx = mxGetNumberOfElements(x_vars);
    int ny = mxGetNumberOfElements(y_vars);
    int nmis = mxGetNumberOfElements(mis_vars);
    int nobs = mxGetNumberOfElements(obs_vals);
    
    /* allocate outputs */
    
    mxArray *sx = mxCreateDoubleMatrix(nx, 1, mxREAL);
    mxArray *sy = mxCreateDoubleMatrix(ny, 1, mxREAL);
    mxArray *sxx = mxCreateDoubleMatrix(nx, nx, mxREAL);
    mxArray *syy = mxCreateDoubleMatrix(ny, ny, mxREAL);
    mxArray *sxy = mxCreateDoubleMatrix(nx, ny, mxREAL);
    double *psx = mxGetPr(sx);
    double *psy = mxGetPr(sy);
    double *psxx = mxGetPr(sxx);
    double *psyy = mxGetPr(syy);
    double *psxy = mxGetPr(sxy);
    
    /* compute ess */

    int i, j, mis_idx, obs_idx;
    
    /* find whether vars are observed and their indexes into mis_vars and obs_vars */

    bool *x_obs = (bool *)mxMalloc(sizeof(bool)*nx);
    bool *y_obs = (bool *)mxMalloc(sizeof(bool)*ny);
    int *x_idx = (int *)mxMalloc(sizeof(int)*nx);
    int *y_idx = (int *)mxMalloc(sizeof(int)*ny);
    
    for(i = 0; i < nx; ++i) {
        mis_idx = 0;
        obs_idx = 0;
        while(mis_idx < nmis && pmis_vars[mis_idx] < px_vars[i]) ++mis_idx;
        while(obs_idx < nobs && pobs_vars[obs_idx] < px_vars[i]) ++obs_idx;
        x_obs[i] = obs_idx < nobs && pobs_vars[obs_idx] == px_vars[i];
        
        if(x_obs[i]) {
            x_idx[i] = obs_idx;
        } else {
            x_idx[i] = mis_idx;
        }
    }
    
    for(i = 0; i < ny; ++i) {
        mis_idx = 0;
        obs_idx = 0;
        while(mis_idx < nmis && pmis_vars[mis_idx] < py_vars[i]) ++mis_idx;
        while(obs_idx < nobs && pobs_vars[obs_idx] < py_vars[i]) ++obs_idx;
        y_obs[i] = obs_idx < nobs && pobs_vars[obs_idx] == py_vars[i];
        
        if(y_obs[i]) {
            y_idx[i] = obs_idx;
        } else {
            y_idx[i] = mis_idx;
        }
    }
    
    /* compute sx */

    for(i = 0; i < nx; ++i) {
        if(x_obs[i]) {
            psx[i] = pobs_vals[x_idx[i]];
        } else {
            psx[i] = pmu[x_idx[i]];
        }
    }

    /* compute sy */
    
    for(i = 0; i < ny; ++i) {
        if(y_obs[i]) {
            psy[i] = pobs_vals[y_idx[i]];
        } else {
            psy[i] = pmu[y_idx[i]];
        }
    }

    /* fill in covariances for sxx */
    /* only upper triangle is filled in, with lower left for later */
    /* where >=1 var is observed are left at 0 from initialization */
    
    for(i = 0; i < nx; ++i) {
        if(!x_obs[i]) {
            for(j = i; j < nx; ++j) {
                if(!x_obs[j]) {
                    psxx[nx*j+i] = psigma[nmis*x_idx[j]+x_idx[i]];
                }
            }
        }
    }

    /* fill in covariances for syy */
    /* only upper triangle is filled in, with lower left for later */
    /* where >=1 var is observed are left at 0 from initialization */
    
    for(i = 0; i < ny; ++i) {
        if(!y_obs[i]) {
            for(j = i; j < ny; ++j) {
                if(!y_obs[j]) {
                    psyy[ny*j+i] = psigma[nmis*y_idx[j]+y_idx[i]];
                }
            }
        }
    }
    
    /* fill in covariances for sxy */
    /* where >=1 var is observed are left at 0 from initialization */

    for(i = 0; i < nx; ++i) {
        if(!x_obs[i]) {
            for(j = 0; j < ny; ++j) {
                if(!y_obs[j]) {
                    psxy[nx*j+i] = psigma[nmis*y_idx[j]+x_idx[i]];
                }
            }
        }
    }
    
    /* add E[X_i]*E[X_j] to get the desired statistics E[X_i * X_j] */
    
    /* sxx */    
    for(i = 0; i < nx; ++i) {
        for(j = i; j < nx; ++j) {
            psxx[nx*j+i] = psxx[nx*j+i]+psx[i]*psx[j];
            psxx[nx*i+j] = psxx[nx*j+i]; /* fill in lower triangle */
        }
    }
    
    /* syy */    
    for(i = 0; i < ny; ++i) {
        for(j = i; j < ny; ++j) {
            psyy[ny*j+i] = psyy[ny*j+i]+psy[i]*psy[j];
            psyy[ny*i+j] = psyy[ny*j+i]; /* fill in lower triangle */
        }
    }

    /* sxy */    
    for(i = 0; i < nx; ++i) {
        for(j = 0; j < ny; ++j) {
            psxy[nx*j+i] = psxy[nx*j+i]+psx[i]*psy[j];
        }
    }

    /* free dynamic arrays */
    
    mxFree(x_obs);
    mxFree(y_obs);
    mxFree(x_idx);
    mxFree(y_idx);
    
    /* populate output arguments */

    plhs[0] = sx;
    plhs[1] = sy;
    plhs[2] = sxx;
    plhs[3] = syy;
    plhs[4] = sxy;
}
