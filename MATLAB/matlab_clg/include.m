function r=include(s)

PROJECT_ROOT='C:/Users/Chris/Documents/MATLAB/matlab_clg/';

s=[PROJECT_ROOT s];
prev_state=warning('off','MATLAB:rmpath:DirNotFound');
rmpath(s);
warning(prev_state.state,'MATLAB:rmpath:DirNotFound');
addpath(s);

end

