% called by model/predict().  do point prediction based on probabilistic predictions.

function d=point_predict(gmm,method)

%include('util/');

if strcmp(method,'expected_value')
    d=exp(gmm.regime_posterior(1)).*gmm.mvn{1}.b0;
    for i=2:gmm.num_regimes
        d=d+exp(gmm.regime_posterior(i)).*gmm.mvn{i}.b0;
    end
elseif strcmp(method,'maximum_likelihood')

    % evaluate the posterior density at each regime's mode (which is just that regime's mean)

    density=-inf([gmm.num_regimes 1]);
    for mode_idx=1:gmm.num_regimes
        for i=1:gmm.num_regimeum_regimess
            this_component=gmm.regime_posterior(i)+logmvnpdf(gmm.mvn{mode_idx}.b0,gmm.mvn{i}.b0,gmm.mvn{i}.s);
            density(mode_idx)=logadd(density(mode_idx),this_component);
        end
    end

    % find the mode of the posterior and use that as the prediction

    [junk,idx_of_mode]=max(density); % note that this arbitrarily returns the first if there are identical max values
    d=gmm.mvn{idx_of_mode}.b0;
else
    error(['unsupported point prediction method: ' method]);
end

end

