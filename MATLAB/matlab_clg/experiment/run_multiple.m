function results_collection=run_multiple(experiments_collection)

%include('util/');
%include('experiment/');

results_collection.date_run=now();
results_collection.num_experiments=length(experiments_collection.experiments);
results_collection.experiments=experiments_collection.experiments;
results_collection.root=experiments_collection.root;

% TODO: create TOC in root to help with experiment folders

results_collection.results=run_parallel('run_experiment',{},cellfun(@(x) {x},experiments_collection.experiments,'UniformOutput',0),experiments_collection.root,[experiments_collection.root 'tmp/queue.txt']);

end

