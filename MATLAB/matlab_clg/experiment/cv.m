function results=cv(model,experiment)

assert(1<experiment.num_folds);

%include('util/');
%include('experiment/');

results.date_run=now();
results.experiment=experiment;

experiment=progress(experiment,'clear');
experiment=progress(experiment,'push','cv fold');

results.fold=run_parallel('run_fold',{model,experiment},cellfun(@(x) {x},mat2cell((1:experiment.num_folds)',ones([experiment.num_folds 1]),1),'UniformOutput',0),experiment.root,experiment.queue);

experiment=progress(experiment,'pop');

end

