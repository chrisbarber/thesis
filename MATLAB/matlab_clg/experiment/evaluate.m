function evaluation=evaluate(predictions,test_set,experiment)

%include('data/');
%include('experiment/');

evaluation.data_idx=predictions.data_idx;
evaluation.N=predictions.N;
evaluation.T=predictions.T;
evaluation.site=predictions.site;
evaluation.units=predictions.units;
evaluation.err=predictions.d-test_set.d(predictions.data_idx,:);
evaluation.point_prediction_made=predictions.point_prediction_made;
evaluation.MAE=nanmean(abs(evaluation.err),2);
evaluation.RMSE=nanmean(evaluation.err.^2,2).^0.5;
evaluation.ll=predictions.ll;

speed_pred=get_speed(predictions,experiment);
speed_obs=get_speed(test_set,experiment);
[spd_site,speed_pred_idx]=sort(speed_pred.site);
[junk,speed_obs_idx]=sort(speed_obs.site);
evaluation.spd_site=spd_site;
evaluation.spd_prediction_made=~isnan(speed_pred.d(speed_pred_idx,:));
evaluation.spd_err=speed_pred.d(speed_pred_idx,:)-speed_obs.d(speed_obs_idx,:);
evaluation.spd_MAE=nanmean(abs(evaluation.spd_err),2);
evaluation.spd_RMSE=nanmean(evaluation.spd_err.^2,2).^0.5;

end

