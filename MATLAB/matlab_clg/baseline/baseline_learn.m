function model=baseline_learn(fold_data,experiment)

if strcmp(experiment.baseline.model,'persistent_gaussian')
    model.data_idx=find(~fold_data.is_deterministic);
    model.horizon=experiment.model_config.horizon;
    model.sample_mean=nanmean(fold_data.d(model.data_idx,:),2);
    model.sample_mean(isnan(model.sample_mean))=0.1; % for the rare case there are no observations at a particular site
    model.sample_cov=diag(nanvar(fold_data.d(model.data_idx,:),0,2));
    model.sample_cov(isnan(model.sample_cov))=0.1; % likewise
else
    error(['unsupported baseline model ' experiment.baseline.model]);
end

end
