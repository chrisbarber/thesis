function xinv=my_inv(x)

fudge=0.001;
lastwarn('');
xinv=x\eye(size(x));

if ~isempty(lastwarn)
    x=(x+x')/2; % fix asymmetric
    
    while 1
        lastwarn('');
        xinv=x\eye(size(x));
        if isempty(lastwarn)
            break;
        else
            x=x+fudge*eye(size(x,1)); % ridge adjustment
            fudge=fudge*10;
        end
    end
end

end
