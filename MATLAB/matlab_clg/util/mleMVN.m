% given a complete dataset D where D(i,j) is the ith RV in instance j
% construct mvn whose parameters are the Maximum Likelihood Estimate over D
% see http://en.wikipedia.org/wiki/Multivariate_normal#Estimation_of_parameters

function mvn = mleMVN(D)

mvn.m=mean(D,2);
mvn.s=cov(D');

end
