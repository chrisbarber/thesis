function logsumV = logsumvec(V)

% sums elements of a vector using a constant factor ("alpha")
% exponentiation, sum, and logarithm

if ~isvector(V)
    error('V must be a vector');
end

log_alpha=max(V(:));
logsumV=log(sum(exp(V-log_alpha)))+log_alpha;