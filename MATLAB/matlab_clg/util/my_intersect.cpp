/* implements the [c,ia,ib]=intersect(a,b) case of the matlab intersect function
this function however, will output duplicate values appearing in a (but not in b) */

#include "mex.h"
#include <stdlib.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

    int i, j;

    /* check # arguments */

    if(2 != nrhs) {
        mexErrMsgTxt("Wrong number of input arguments");
    }
    if(3 < nlhs) {
        mexErrMsgTxt("Wrong number of output arguments");
    }

    /* get input sizes (both should be vectors) */

    int ma, na, mb, nb, la, lb, max_output_len;

    ma = mxGetM(prhs[0]);
    na = mxGetN(prhs[0]);
    mb = mxGetM(prhs[1]);
    nb = mxGetN(prhs[1]);

    if((ma > 1 && na > 1) || (mb > 1 && nb > 1)) {
        mexErrMsgTxt("Input arguments must be vectors");
    }

    la = mxGetNumberOfElements(prhs[0]);
    lb = mxGetNumberOfElements(prhs[1]);

    if(0==la || 0==lb) {
        plhs[0] = mxCreateDoubleMatrix(0, 0, mxREAL);

        if(nlhs >= 2) {
            plhs[1] = mxCreateDoubleMatrix(0, 0, mxREAL);
        }
        if(nlhs >= 3) {
            plhs[2] = mxCreateDoubleMatrix(0, 0, mxREAL);
        }
    } else {
        max_output_len = la > lb ? la : lb; /* max of vector lengths */

        /* get input data */

        double *a, *b;

        a = mxGetPr(prhs[0]);
        b = mxGetPr(prhs[1]);

        /* allocate arrays for output */

        mxArray *c = mxCreateDoubleMatrix(max_output_len, 1, mxREAL);
        mxArray *ia = mxCreateDoubleMatrix(max_output_len, 1, mxREAL);
        mxArray *ib = mxCreateDoubleMatrix(max_output_len, 1, mxREAL);

        /* search through to compute intersection, c, as well as ia and ib */

        int k = 0;

        double *pc, *pia, *pib;
        pc = mxGetPr(c);
        pia = mxGetPr(ia);
        pib = mxGetPr(ib);

        i = 0;
        j = 0;
        while(i < la) {
            if(j >= lb) {
                break;
            } else if(a[i] < b[j]) {
                ++i;
            } else if(a[i] > b[j]) {
                ++j;
            } else {
                /* a[i] == b[j] */
                pc[k] = a[i];
                pia[k] = (double)(i + 1);
                pib[k] = (double)(j + 1);
                ++i;
                ++j;
                ++k;
            }
        }

        /* shrink outputs to appropriate size */

        mxSetM(c, k);
        mxSetM(ia, k);
        mxSetM(ib, k);

        /* allocate and populate output arguments */

        plhs[0] = c;

        if(nlhs >= 2) {
            plhs[1] = ia;
        } else {
            mxDestroyArray(ia);
        }
        if(nlhs >= 3) {
            plhs[2] = ib;
        } else {
            mxDestroyArray(ib);
        }
    }
}

