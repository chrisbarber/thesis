/* implements the [b,ia,ib]=my_setdiff(a) case of the matlab setdiff function */
/* TODO: currently this function outputs ia and ib according to the sorted copy of a.  this is not very useful but i'm not using it now. */

#include "mex.h"
#include <stdlib.h>

int double_cmp(const void *a, const void *b);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

    int i, j;

    /* check # arguments */

    if(1 != nrhs) {
        mexErrMsgTxt("Wrong number of input arguments");
    }
    if(3 < nlhs) {
        mexErrMsgTxt("Wrong number of output arguments");
    }

    /* get input sizes (both should be vectors) */

    int ma, na, la;

    ma = mxGetM(prhs[0]);
    na = mxGetN(prhs[0]);

    if(ma > 1 && na > 1) {
        mexErrMsgTxt("Input must be a vector");
    }

    la = mxGetNumberOfElements(prhs[0]);

    if(0==la) {
        plhs[0] = mxCreateDoubleMatrix(0, 0, mxREAL);

        if(nlhs >= 2) {
            plhs[1] = mxCreateDoubleMatrix(0, 0, mxREAL);
        }
        if(nlhs >= 3) {
            plhs[2] = mxCreateDoubleMatrix(0, 0, mxREAL);
        }
    } else {
        /* allocate arrays for output */

        mxArray *c = mxCreateDoubleMatrix(la, 1, mxREAL);
        mxArray *ia = mxCreateDoubleMatrix(la, 1, mxREAL);
        mxArray *ib = mxCreateDoubleMatrix(la, 1, mxREAL);

        /* copy a into a new vector and sort it */

        mxArray *sorted_a = mxDuplicateArray(prhs[0]);
        double *psorted_a = mxGetPr(sorted_a);

        qsort(psorted_a, la, sizeof(double), double_cmp);

        /* search through to compute unique, b, as well as ia and ib */

        int k = 0;

        double *pc, *pia, *pib;
        pc = mxGetPr(c);
        pia = mxGetPr(ia);
        pib = mxGetPr(ib);

        for(i = 0; i < la; ++i) {
            pib[i] = (double)(k + 1);

            if(la - 1 == i || psorted_a[i]<psorted_a[i+1]) {
                pc[k] = psorted_a[i];
                pia[k] = (double)(i + 1);
                ++k;
            }
        }       

        /* shrink c and ia to appropriate size */

        mxSetM(c, k);
        mxSetM(ia, k);

        /* allocate and populate output arguments */

        plhs[0] = c;

        if(nlhs >= 2) {
            plhs[1] = ia;
        } else {
            mxDestroyArray(ia);
        }
        if(nlhs >= 3) {
            plhs[2] = ib;
        } else {
            mxDestroyArray(ib);
        }
    }
}

int double_cmp(const void *a, const void *b) {
    double cmp = *((const double *) a) - *((const double *) b);

    if(cmp < 0.0) {
        return -1;
    } else if(cmp > 0.0) {
        return 1;
    } else {
        return 0;
    }
}

