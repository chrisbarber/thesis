function s=check_sigma(s)

if ~isempty(s)
    % fix asymmetric

    if ~all(s(:)==ravel(s'))
        s=(s+s')/2;
    end

    % fix non-invertible / non positive definite by ridge adjustment

    fudge=0.001;
    while ~invertible(s)
        s=s+fudge*eye(size(s,1));
        fudge=fudge*10;
        if fudge > 1000
            break;
        end
    end
end

end
