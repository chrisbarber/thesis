function [folds testSetIndices trainingSetIndices] = CVfolds(num_seq, k, useRandom)

%
%         CVfolds - Generate Cross-validation folds
%         -~=====================================~-
%
% Inputs
%
%   num_seq is the total number of sequences to divide into folds
%
%   k is the number of folds into which the sequences should be separated.
%     If k>num_seq k defaults to num_seq.
%
%   useRandom specifies whether the folds should be "in place," or if
%     the sequences should be randomly permuted into the folds.  If
%     omitted this defaults to false.
%

if 2==nargin
    useRandom=false;
end

% folds are distributed as evenly as possible.  the larger test-sets
% are placed in earlier folds.
smallFoldSize=floor(num_seq/k);
largeFoldSize=ceil(num_seq/k);
numLargeFolds=num_seq-(k*smallFoldSize);

largeFolds=1:numLargeFolds;
largeFolds=repmat(largeFolds, [largeFoldSize 1]);
largeFolds=largeFolds(:)';

smallFolds=numLargeFolds+1:k;
smallFolds=repmat(smallFolds, [smallFoldSize 1]);
smallFolds=smallFolds(:)';

folds=[largeFolds smallFolds];

if useRandom
    folds=folds(randperm(num_seq));
end

for fold=1:k
    testSetIndices{fold}=find(folds==fold);
    trainingSetIndices{fold}=my_setdiff(1:num_seq,testSetIndices{fold});
end