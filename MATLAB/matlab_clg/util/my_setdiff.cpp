/* implements the [c,ia]=my_setdiff(a,b) case of the matlab setdiff function
this function however, will output duplicate values appearing in a */

#include "mex.h"
#include <stdlib.h>

int double_cmp(const void *a, const void *b);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

    int i, j;

    /* check # arguments */

    if(2 != nrhs) {
        mexErrMsgTxt("Wrong number of input arguments");
    }
    if(2 < nlhs) {
        mexErrMsgTxt("Wrong number of output arguments");
    }

    /* get input sizes (both should be vectors) */

    int ma, na, mb, nb, la, lb;

    ma = mxGetM(prhs[0]);
    na = mxGetN(prhs[0]);
    mb = mxGetM(prhs[1]);
    nb = mxGetN(prhs[1]);

    if((ma > 1 && na > 1) || (mb > 1 && nb > 1)) {
        mexErrMsgTxt("Input arguments must be vectors");
    }

    la = mxGetNumberOfElements(prhs[0]);
    lb = mxGetNumberOfElements(prhs[1]);

    if(0==la) {
        plhs[0] = mxCreateDoubleMatrix(0, 0, mxREAL);

        if(nlhs >= 2) {
            plhs[1] = mxCreateDoubleMatrix(0, 0, mxREAL);
        }
    } else if(0==lb) {
        plhs[0] = mxDuplicateArray(prhs[0]);

        if(nlhs >= 2) {
            plhs[1] = mxCreateDoubleMatrix(la, 1, mxREAL);
            double *pdata = mxGetPr(plhs[1]);
            for(i = 0; i < la; ++i) {
                pdata[i] = (double)(i + 1);
            }
        }
    } else {
        /* allocate arrays for output */

        mxArray *c = mxCreateDoubleMatrix(la, 1, mxREAL);
        mxArray *ia = mxCreateDoubleMatrix(la, 1, mxREAL);

        /* copy a and b into new vectors and sort them */

        mxArray *sorted_a = mxDuplicateArray(prhs[0]);
        mxArray *sorted_b = mxDuplicateArray(prhs[1]);
        double *psorted_a = mxGetPr(sorted_a);
        double *psorted_b = mxGetPr(sorted_b);

        qsort(psorted_a, la, sizeof(double), double_cmp);
        qsort(psorted_b, lb, sizeof(double), double_cmp);

        /* search through to compute my_setdiff, c, as well as ia */

        int k = 0;

        double *pc, *pia;
        pc = mxGetPr(c);
        pia = mxGetPr(ia);

        i = 0;
        j = 0;
        while(i < la) {
            if(j >= lb || psorted_a[i] < psorted_b[j]) {
                pc[k] = psorted_a[i];
                pia[k] = (double)(i + 1);
                ++k;
                ++i;
            } else if(psorted_a[i] > psorted_b[j]) {
                ++j;
            } else {
                /* psorted_a[i] == psorted_b[j] */
                ++i;
            }
        }

        mxDestroyArray(sorted_a);
        mxDestroyArray(sorted_b);

        /* shrink outputs to appropriate size */

        mxSetM(c, k);
        mxSetM(ia, k);

        /* allocate and populate output arguments */

        plhs[0] = c;

        if(nlhs >= 2) {
            plhs[1] = ia;
        } else {
            mxDestroyArray(ia);
        }
    }
}

int double_cmp(const void *a, const void *b) {
    double cmp = *((const double *) a) - *((const double *) b);

    if(cmp < 0.0) {
        return -1;
    } else if(cmp > 0.0) {
        return 1;
    } else {
        return 0;
    }
}

