function logsumM = logsum(M, dim)

if 1 == nargin
    if ~isvector(M)
        error('M must be a vector or a dimension must be specified');
    end
    
    logsumM = logsumvec(M);
else
    % sum a matrix along dimension 'dim'

    if dim < 1
        error('dim must be >= 1');
    elseif dim > ndims(M)
        logsumM=M;
    else
        log_alpha=max(M(:));
        logsumM=log(sum(exp(M-log_alpha),dim))+log_alpha;
    end
end