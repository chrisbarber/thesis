function ret_vals=run_parallel(function_name,invariant_args,input_args,root_dir,queue,varargin)

if isempty(varargin)
    collect_results=1;
else
    collect_results=varargin{1};

    if ~collect_results
        ret_vals=[];
    end
end

scratch_dir=[root_dir 'tmp/'];
if ~exist(scratch_dir,'dir') mkdir(scratch_dir); end
% system(['rm ' scratch_dir '*']); % clear scratch dir to make sure old files aren't accidentally used

% broadcast invariant args

broadcast_file=[scratch_dir 'broadcast.mat'];
save(broadcast_file,'invariant_args');

% write input files

already_run=zeros([length(input_args) 1]);

for i=1:length(input_args)
    input_file{i}=[scratch_dir 'input_' num2str(i) '.mat'];
    output_file{i}=[scratch_dir 'output_' num2str(i) '.mat'];

    if ~exist(output_file{i})
        temp_args=input_args{i};
        save(input_file{i},'temp_args');
    else
        already_run(i)=1;
    end
end

% write commands to slave queue and write input files

lock_file(queue);
fid=fopen(queue,'a');
for i=1:length(input_args)
    if ~already_run(i)
        fprintf(fid,'load(''%s''); load(''%s''); ret_val=%s(invariant_args{:},temp_args{:}); lock_file(''%s''); save(''%s'',''ret_val''); unlock_file(''%s'');\n',broadcast_file,input_file{i},function_name,output_file{i},output_file{i},output_file{i});
    end
end
fclose(fid);
unlock_file(queue);

% run a slave on this node that will resume execution when the queue is empty

slave(queue,1);

% collect results from slaves

for i=1:length(input_args)
    while ~exist(output_file{i},'file')
        disp(['waiting for parallel job ' num2str(i) ' to finish ...']);
        pause(10);
    end

    if collect_results
        lock_file(output_file{i});
        load(output_file{i},'ret_val');
        unlock_file(output_file{i});
        ret_vals{i}=ret_val;
    end

    disp(['collected results from parallel job ' num2str(i)]);
end

end

