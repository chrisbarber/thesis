% works like mvnrnd except ignores nan's

function D=nanmvnrnd(mu,sigma)

obs=~isnan(mu);

D=mvnrnd(mu(obs),sigma(obs,obs));

end
