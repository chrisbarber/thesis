function log_pr=gmm_density(gmm,x)

%include('util/');

log_pr=0;
obs=~isnan(x);
if any(obs)
    x=x(obs);
    log_pr=-inf;
    for r=1:gmm.num_regimes
        log_pr=logadd(log_pr,gmm.regime_posterior(r)+logmvnpdf(x,gmm.mvn{r}.b0(obs),gmm.mvn{r}.s(obs,obs)));
    end
end

end

