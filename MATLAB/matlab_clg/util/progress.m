function [experiment,txt]=progress(experiment,cmd,varargin)

if ~isfield(experiment,'progress')
    experiment.progress={};
end

if strcmp(cmd,'clear')
    experiment.progress={};
elseif strcmp(cmd,'push')
    experiment.progress{end+1,1}=varargin{1};
    experiment.progress{end,2}='';
elseif strcmp(cmd,'pop')
    experiment.progress=experiment.progress(1:end-1,:);
elseif strcmp(cmd,'update')
    experiment.progress{end,2}=varargin{1};
else
    error(['invalid progress command ' cmd]);
end


if ~isempty(experiment.progress)
    txt=[experiment.progress{1,1} ' ' experiment.progress{1,2}];
    for i=2:size(experiment.progress,1)
        txt=[txt ', ' experiment.progress{i,1} ' ' experiment.progress{i,2}];
    end
else
    txt='';
end

fprintf(1,'%s\n',txt);

end
