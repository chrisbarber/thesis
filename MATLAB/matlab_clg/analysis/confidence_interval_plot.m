function confidence_interval_plot(results)

include('data/');
include('util/');

all_err=[];
all_pred=[];
for i=1:results.experiment.num_folds
    load(results.fold{i}.evaluation);
    all_err=[all_err; abs(fold.baseline.evaluation.err(:))];
    spd_pred=get_speed(fold.predictions,results.experiment);
    spd_pred=spd_pred.d;
    spd_pred=spd_pred(ravel(repmat(1:size(spd_pred,1),[2 1])),:);
    all_pred=[all_pred; spd_pred(:)];
end


XSERIES=0:1:14;
CONFIDENCES=[.99 .5];

y1=get_y(XSERIES,CONFIDENCES,all_err,all_pred);

all_err=[];
for i=1:results.experiment.num_folds
    load(results.fold{i}.evaluation);
    all_err=[all_err; abs(fold.evaluation.err(:))];
end

y2=get_y(XSERIES,CONFIDENCES,all_err,all_pred);

figure;
hold on;
for i=1:length(CONFIDENCES)
    if .5==CONFIDENCES(i)
        plot(XSERIES(1:size(y1,2)),y1(i,:),'r-+');
        plot(XSERIES(1:size(y2,2)),y2(i,:),'b-*');
    else
        plot(XSERIES(1:size(y1,2)),y1(i,:),'r:+');
        plot(XSERIES(1:size(y2,2)),y2(i,:),'b:*');
    end
end

end

function y=get_y(XSERIES,CONFIDENCES,all_err,all_pred)

CDF_X=0:.001:10;

for x_idx=1:(length(XSERIES)-1)
    x1=XSERIES(x_idx);
    x2=XSERIES(x_idx+1);
    
    this_err=all_err(and(all_pred>=x1,all_pred<x2));
    
    if numel(this_err)>100
        err_cdf=cumsum(histc(this_err,CDF_X));
        err_cdf=err_cdf/max(err_cdf);
    
        for i=1:length(CONFIDENCES)
            idx=find(err_cdf<=CONFIDENCES(i),1,'last');
            if isempty(idx)
                idx=1;
            end
            y(i,x_idx)=CDF_X(idx);
        end
    end
end

end
