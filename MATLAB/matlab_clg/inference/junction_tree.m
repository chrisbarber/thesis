% calib=junction_tree(factors,ar_order,obs_vars,obs_vals)
%
% Performs clique-tree calibration on a Vector Auto-Regressive model.  Input
% factors must be in order of increasing timesteps.  Returns a 1-D structure-
% array of calibrated cluster factors.
%
% factors - factors of VAR model in 1-D structure array
% ar_order - markov order of the model, needed to construct cliques
% obs_vars - observed variable indexes
% obs_vals - observed values at those indexes
%
% calib - calibrated clique factors in 1-D structure array

function calib=junction_tree(factors,ar_order,obs_vars,obs_vals)

% reduce factors according to evidence
factors=factor_reduce_multiple(factors,obs_vars,obs_vals);

% build initial cluster graph and multiply factors in same clique
cliques=[factor_prod_multiple(factors(1:min(length(factors),ar_order+1))) factors(ar_order+2:end)];

% perform forward pass
calib=cliques;
for i=1:length(cliques)-1
    non_sepset_vars=my_setdiff(cliques(i).v,cliques(i+1).v);
    forward_msg=factor_marginalize(calib(i),non_sepset_vars);
    calib(i+1)=factor_product(calib(i+1),forward_msg);
end

% perform backward pass
belief=cliques;
for i=length(cliques):-1:2
    non_sepset_vars=my_setdiff(cliques(i).v,cliques(i-1).v);
    backward_msg=factor_marginalize(belief(i),non_sepset_vars);
    belief(i-1)=factor_product(belief(i-1),backward_msg);
    calib(i-1)=factor_product(calib(i-1),backward_msg);
end

end

