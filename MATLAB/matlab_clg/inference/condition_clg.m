% clg2=condition_clg(clg,obs_vars,obs_vals)
%
% This function simply plugs in values for a subset of the parents in a CLG,
% removing them and updating the means accordingly.

function clg2=condition_clg(clg,obs_vars,obs_vals)

if isempty(obs_vars)
    clg2=clg;
else
    [junk,junk,obs_pa_idx]=my_intersect(obs_vars,clg.pa_vars);
    assert(numel(obs_pa_idx)==numel(obs_vars)); % all obs_vars must be parents
    leftover_pa_idx=(1:clg.pa_sz)'; leftover_pa_idx(obs_pa_idx)=[];

    clg2=clg;
    clg2.pa_vars=clg2.pa_vars(leftover_pa_idx);
    clg2.pa_joint=clg2.pa_joint(leftover_pa_idx);
    clg2.pa_nodes=my_unique(clg2.pa_joint);
    clg2.pa_n=numel(clg2.pa_nodes);
    clg2.pa_sz=numel(clg2.pa_vars);

    clg2.b0=clg2.b0+clg2.b(:,obs_pa_idx)*obs_vals(:);
    clg2.b=clg2.b(:,leftover_pa_idx);
end

end

