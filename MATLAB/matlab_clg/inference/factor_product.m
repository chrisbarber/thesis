function f3=factor_product(f1,f2)

if isempty(f1.v)
    f3=f2;
elseif isempty(f2.v)
    f3=f1;
else
    f3.v=union(f1.v,f2.v);
    f3.n=length(f3.v);
    f3.note='';

    [junk,junk,f1idx]=my_intersect(f1.v,f3.v);
    [junk,junk,f2idx]=my_intersect(f2.v,f3.v);

    f3.K=zeros(f3.n);
    f3.K(f1idx,f1idx)=f1.K;
    f3.K(f2idx,f2idx)=f3.K(f2idx,f2idx)+f2.K;

    f3.h=zeros([f3.n 1]);
    f3.h(f1idx)=f1.h;
    f3.h(f2idx)=f3.h(f2idx)+f2.h;

end

f3.g=f1.g+f2.g;

end
