% f=clg_to_factor(clg)
%
% Converts an MVN or CLG  to create a Gaussian factor in canonical form
% (see Koller & Friedman sec. 14.2).  If input is MVN it should be in
% clg format, with an empty regression matrix (no parents) i.e.
%
% clg.b0=mvn.m;
% clg.s=mvn.s;
%
% If input is CLG, the parents are put before the children.
%
% See Koller & Friedman Exercise 14.1 for details

function f=clg_to_factor(clg)

%include('util/');

f.v=[clg.pa_vars; clg.vars];
f.n=numel(f.v);
f.note=clg.note;

f.K=nan(f.n);
C_inv=my_inv(clg.s);
B_T_C_inv=clg.b'*C_inv;
f.K(1:clg.pa_sz,1:clg.pa_sz)=B_T_C_inv*clg.b;
f.K(1:clg.pa_sz,clg.pa_sz+1:end)=-B_T_C_inv;
f.K(clg.pa_sz+1:end,1:clg.pa_sz)=-B_T_C_inv';
f.K(clg.pa_sz+1:end,clg.pa_sz+1:end)=C_inv;

f.h=-B_T_C_inv*clg.b0;
f.h=[f.h; C_inv*clg.b0];

f.g=-0.5*(clg.b0'*C_inv*clg.b0+log((2*pi)^clg.sz*det(clg.s)));

end

