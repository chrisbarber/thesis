% f=var_elim(factors,ar_order,elim_vars,obs_vars,obs_vals)
%
% Perform variable elimination on a Vector Auto-regressive model.  Factors must
% be specified in order from left to right, in canonical Gaussian form.
% Implicitly the elimination ordering is left to right.  ar_order specifies the
% order of the model, in order to avoid searching through factors to find those
% with an elimination variable in their scope.


function f=var_elim(factors,ar_order,elim_vars,obs_vars,obs_vals)

f=factor_reduce_multiple(factors,obs_vars,obs_vals);

iter=length(f)-ar_order;
for i=1:iter+1
    factors_in_scope=1:min(length(f),ar_order+1);
    temp_f=factor_prod_multiple(f(factors_in_scope));
    [temp_elim,junk,junk]=my_intersect(elim_vars,f(1).v);
    temp_f=factor_marginalize(temp_f,temp_elim);
    f(factors_in_scope)=[];
    f=[temp_f f];
end

assert(1==length(f));

end

