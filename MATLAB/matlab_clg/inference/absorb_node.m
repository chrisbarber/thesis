% mvn2=absorb_node(mvn,clg)
%
% This function directly applies Thm. 4.5.8 in Koller & Friedman.  Using this
% we can take a CLG node and an MVN over its parents and output the joint MVN.
% The MVN can contain nodes which are not parents in the CLG.

function mvn2=absorb_node(mvn,clg)

assert(0==mvn.pa_n);

[junk,junk,pa_idx]=my_intersect(clg.pa_vars,mvn.vars);

mvn2=new_clg([mvn.nodes; clg.nodes],[mvn.vars; clg.vars],[mvn.joint; clg.joint],[],[],[]);
mvn2.b0=[mvn.b0; clg.b0+clg.b*mvn.b0(pa_idx)];
mvn2.s(1:mvn.sz,1:mvn.sz)=mvn.s;
mvn2.s(mvn.sz+1:end,1:mvn.sz)=clg.b*mvn.s(pa_idx,:);
mvn2.s(1:mvn.sz,mvn.sz+1:end)=mvn2.s(mvn.sz+1:end,1:mvn.sz)';
mvn2.s(mvn.sz+1:end,mvn.sz+1:end)=clg.s+clg.b*mvn.s(pa_idx,pa_idx)*clg.b';

end

