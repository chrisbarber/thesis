function f=factor_reduce(factor,v,y)

f=factor;

if ~isempty(v)
    [junk,junk,idx]=my_intersect(v,f.v); % find vars to reduce
    leftover_idx=1:f.n'; leftover_idx(idx)=[]; % find remaining idx
    f.v=f.v(leftover_idx);
    f.n=numel(f.v);
    f.note='';

    Kxx=f.K(leftover_idx,leftover_idx);
    Kxy=f.K(leftover_idx,idx);
    Kyy=f.K(idx,idx);

    hx=f.h(leftover_idx);
    hy=f.h(idx);

    y=y(:);
    f.K=Kxx;
    f.h=hx-Kxy*y;
    f.g=f.g+hy'*y-0.5*y'*Kyy*y;
end

end

