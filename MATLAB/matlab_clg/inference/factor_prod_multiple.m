
function f=factor_prod_multiple(factors)

f=factors(1);

for i=2:length(factors)
  f=factor_product(f,factors(i));
end

end

