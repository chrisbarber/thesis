function slave(file,varargin)

include_all();

if ~isempty(varargin)
    break_when_empty=varargin{1};
else
    break_when_empty=0;
end

while 1
    if ~exist(file,'file')
        disp('waiting on nonexistent queue ...');
        pause(15);
    else
        lock_file(file);
        [junk,result]=system(['bash -c "cat ' file ' | wc -l"']);
        unlock_file(file);
        nlines=str2num(result);

        if 0==nlines
            if break_when_empty
                break;
            else
                disp('waiting on empty queue ...');
                pause(15);
            end
        else
            lock_file(file);
            lock_file([file '.bak']);
            [junk,result]=system(['bash -c "tail -n 1 ' file ' && sed ''$d'' < ' file ' > ' file '.bak && mv ' file '.bak ' file '"']);
            unlock_file(file);
            unlock_file([file '.bak']);

            eval(result);
        end
    end
end



end

