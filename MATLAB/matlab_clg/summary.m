function summary(results,varargin)

if isempty(varargin)
    open_browser=1;
else
    open_browser=varargin{1};
end

include('util/');
include('data/');

load(results.experiment.data_src,results.experiment.variable_name);
eval(['data=' results.experiment.variable_name ';']);

file=[results.experiment.root '/summary.html'];
fid=fopen(file,'w');

img_dir=[results.experiment.root '/img/'];
if ~exist(img_dir,'dir')
    mkdir(img_dir);
end

% TODO: add date run

% EXPERIMENT OVERVIEW



% GLOBAL STATS

fprintf(fid,'<h3>Improvement versus baseline model (<i>%s</i>)</h3>',results.experiment.baseline.model);

MAE_improvement=cell2mat(cellfun(@(x) getfield(getfield(x,'comparison'),'MAE_improvement'),results.fold,'UniformOutput',0));
RMSE_improvement=cell2mat(cellfun(@(x) getfield(getfield(x,'comparison'),'RMSE_improvement'),results.fold,'UniformOutput',0));
fprintf(fid,'<ul>');

for i=1:results.experiment.num_station_variables
    fprintf(fid,'<li><i>%s</i><ul>',results.experiment.station_variables{i});

    unit_idx=get_var_idx(results.fold{1}.comparison,results.experiment,[],results.experiment.station_variables{i},[],[]);
    this_MAE_imp=MAE_improvement(unit_idx,:);
    this_RMSE_imp=RMSE_improvement(unit_idx,:);

    fprintf(fid,'<li>Average MAE improvement: %2.1f%% (<i>stddev</i>=%2.1f%% over %d folds)</li>',mean(this_MAE_imp(:)),std(mean(this_MAE_imp,1)),results.experiment.num_folds);
    fprintf(fid,'<li>Average RMSE improvement: %2.1f%% (<i>stddev</i>=%2.1f%% over %d folds)</li>',mean(this_RMSE_imp(:)),std(mean(this_RMSE_imp,1)),results.experiment.num_folds);

    fprintf(fid,'</ul></li>');
end

fprintf(fid,'<li><i>ALL (%s)</i><ul>',sdisp(results.experiment.station_variables));
fprintf(fid,'<li>Average MAE improvement: %2.1f%% (<i>stddev</i>=%2.1f%% over %d folds)</li>',mean(MAE_improvement(:)),std(mean(MAE_improvement,1)),results.experiment.num_folds);
fprintf(fid,'<li>Average RMSE improvement: %2.1f%% (<i>stddev</i>=%2.1f%% over %d folds)</li>',mean(RMSE_improvement(:)),std(mean(RMSE_improvement,1)),results.experiment.num_folds);
fprintf(fid,'</ul></li>');

fprintf(fid,'<li><i>spd</i> (computed from u,v)<ul>');
if isempty(strmatch('spd',results.experiment.station_variables))
    spd_MAE_improvement=cell2mat(cellfun(@(x) getfield(getfield(x,'comparison'),'spd_MAE_improvement'),results.fold,'UniformOutput',0));
    spd_RMSE_improvement=cell2mat(cellfun(@(x) getfield(getfield(x,'comparison'),'spd_RMSE_improvement'),results.fold,'UniformOutput',0));
    fprintf(fid,'<li>Average MAE improvement (speed): %2.1f%% (<i>stddev</i>=%2.1f%% over %d folds)</li>',mean(spd_MAE_improvement(:)),std(mean(spd_MAE_improvement,1)),results.experiment.num_folds);
    fprintf(fid,'<li>Average RMSE improvement (speed): %2.1f%% (<i>stddev</i>=%2.1f%% over %d folds)</li>',mean(spd_RMSE_improvement(:)),std(mean(spd_RMSE_improvement,1)),results.experiment.num_folds);
end
fprintf(fid,'</ul></li>');

fprintf(fid,'</ul>');

% MISSING DATA SUMMARY

fprintf(fid,'<h3>Missing data summary (percent missing)</h3>');
fprintf(fid,'<table border="1">');
fprintf(fid,'<tr>');
fprintf(fid,'<td><b>Site index</b></td><td><b>Station</b></td>');

for fold_idx=1:results.experiment.num_folds
    fprintf(fid,'<td><b>Fold %d</b></td>',fold_idx);
end

fprintf(fid,'</tr>');

for site_idx=1:length(results.experiment.stations)
    fprintf(fid,'<tr><td>%d</td><td>%s</td>',site_idx,results.experiment.station_names{site_idx});

    for fold_idx=1:results.experiment.num_folds
        fold_data=get_fold_data(results.experiment,fold_idx);
        site_data_idx=find(site_idx==fold_data.site);
        total_points=numel(fold_data.d(site_data_idx,:));
        observed_points=sum(ravel(~isnan(fold_data.d(site_data_idx,:))));
        percent_missing=[num2str(round(100*(1-observed_points/total_points))) '%'];
        rgb_value=floor(255*observed_points/total_points);
        fprintf(fid,'<td style="background-color: rgb(255,%d,%d);">%s</td>',rgb_value,rgb_value,percent_missing);
    end

    fprintf(fid,'</tr>');
end

fprintf(fid,'</table>');

% FOLD INFO

fprintf(fid,'<h3>Fold info</h3><table><tr>');

f=figure('visible','off');

for i=1:results.experiment.num_folds
    fprintf(fid,'<td><h5>Fold %d</h5><ul>',i);

    %fprintf(fid,'<li>Date: %s</li>',datestr(results.fold{i}.date_run));
    %fprintf(fid,'<li>Host: %s</li>',results.fold{i}.hostname);
    fprintf(fid,'<li>Halt: %s</li>',results.fold{i}.em_info.em_halt);
    fprintf(fid,'<li>%d iterations</li>',results.fold{i}.em_info.iter);
    fprintf(fid,'<li>Training time: %02d:%02d</li>',floor(results.fold{i}.em_info.training_time/3600),round(mod(results.fold{i}.em_info.training_time,3600)/60));
    fprintf(fid,'<li>Log-like: %d</li>',results.fold{i}.em_info.training_set_ll(end));

    fprintf(fid,'</ul></td>');
end

fprintf(fid,'</tr></table>');

% STATS BY SITE

% TODO: sort sites by baseline "difficulty"?

MAE=cell2mat(cellfun(@(x) getfield(getfield(x,'evaluation'),'MAE'),results.fold,'UniformOutput',0));
RMSE=cell2mat(cellfun(@(x) getfield(getfield(x,'evaluation'),'RMSE'),results.fold,'UniformOutput',0));

fprintf(fid,'<h3>Statistics by-site</h3><ul>');

f=figure('visible','off');

for i=1:results.experiment.num_station_variables
    fprintf(fid,'<li><i>%s</i><ul>',results.experiment.station_variables{i});

    unit_idx=get_var_idx(results.fold{1}.comparison,results.experiment,[],results.experiment.station_variables{i},[],[]);
    this_MAE_imp=MAE_improvement(unit_idx,:);
    this_RMSE_imp=RMSE_improvement(unit_idx,:);

    errorbar(mean(this_MAE_imp,2),std(this_MAE_imp,0,2),'x');
    MAE_imp_img=[img_dir 'MAE_improvement_by_site_' results.experiment.station_variables{i} '.png'];
    title([results.experiment.station_variables{i} ' MAE improvement by site']);
    xlabel('Site index');
    ylabel(['percent improvement versus ' results.experiment.baseline.model],'Interpreter','none');
    set(get(f,'CurrentAxes'),'XTick',1:results.experiment.num_stations);
    print(f,'-r80','-dpng',MAE_imp_img);

    errorbar(mean(this_RMSE_imp,2),std(this_RMSE_imp,0,2),'x');
    RMSE_imp_img=[img_dir 'RMSE_improvement_by_site_' results.experiment.station_variables{i} '.png'];
    title([results.experiment.station_variables{i} ' RMSE improvement by site']);
    xlabel('Site index');
    ylabel(['percent improvement versus ' results.experiment.baseline.model],'Interpreter','none');
    set(get(f,'CurrentAxes'),'XTick',1:results.experiment.num_stations);
    print(f,'-r80','-dpng',RMSE_imp_img);

    unit_idx=get_var_idx(results.fold{1}.evaluation,results.experiment,[],results.experiment.station_variables{i},[],[]);
    this_MAE=MAE(unit_idx,:);
    this_RMSE=RMSE(unit_idx,:);

    errorbar(mean(this_MAE,2),std(this_MAE,0,2),'x');
    MAE_img=[img_dir 'MAE_by_site_' results.experiment.station_variables{i} '.png'];
    title([results.experiment.station_variables{i} ' MAE by site']);
    xlabel('Site index');
    ylabel(['MAE (' results.experiment.units.(results.experiment.station_variables{i}) ')'],'Interpreter','none');
    set(get(f,'CurrentAxes'),'XTick',1:results.experiment.num_stations);
    print(f,'-r80','-dpng',MAE_img);

    errorbar(mean(this_RMSE,2),std(this_RMSE,0,2),'x');
    RMSE_img=[img_dir 'RMSE_by_site_' results.experiment.station_variables{i} '.png'];
    title([results.experiment.station_variables{i} ' RMSE by site']);
    xlabel('Site index');
    ylabel(['RMSE (' results.experiment.units.(results.experiment.station_variables{i}) ')'],'Interpreter','none');
    set(get(f,'CurrentAxes'),'XTick',1:results.experiment.num_stations);
    print(f,'-r80','-dpng',RMSE_img);

   fprintf(fid,'<li>MAE<br /><img src="%s" /><img src="%s" /></li>',MAE_imp_img,MAE_img);
    fprintf(fid,'<li>RMSE<br /><img src="%s" /><img src="%s" /></li>',RMSE_imp_img,RMSE_img);

    fprintf(fid,'</ul></li>');
end

fprintf(fid,'</ul>');

% OPEN IN BROWSER

if open_browser
    system(['firefox ' file ' &']);
end

end

