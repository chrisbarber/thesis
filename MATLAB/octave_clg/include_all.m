function include_all()

include('baseline/');
include('data/');
include('em/');
include('experiment/');
include('inference/');
include('model/');
include('util/');

end

