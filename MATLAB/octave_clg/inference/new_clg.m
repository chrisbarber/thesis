% clg=new_clg(nodes,vars,joint,pa_nodes,pa_vars,pa_joint)
%
% construct clg distribution
%   nodes - node indexes e.g. [1 2]
%   vars - variable indexes, e.g. [1 2 3 4]
%   joint - groupings of joint vars e.g. [1 1 2 2]
%   pa_ ... - same as other params but for parents

function clg=new_clg(nodes,vars,joint,pa_nodes,pa_vars,pa_joint)

clg.nodes=nodes(:);
clg.vars=vars(:);
clg.joint=joint(:);
clg.pa_nodes=pa_nodes(:);
clg.pa_vars=pa_vars(:);
clg.pa_joint=pa_joint(:);

clg.n=numel(nodes);
clg.sz=numel(vars);
clg.pa_n=numel(pa_nodes);
clg.pa_sz=numel(pa_vars);

clg.b0=zeros([clg.sz 1]);
clg.b=zeros([clg.sz clg.pa_sz]);
clg.s=eye(clg.sz);

clg.note='';


end
