% clg=factor_to_mvn(factor)
%
% Convert a Gaussian factor in canonical form back into an MVN.  Care should be
% taken only to call this function if the factor represents an MVN.  If it is
% equivalent to a CLG, K will not be invertible, which will cause a warning. In
% conversion to factor, only information about variables (not nodes) is kept,
% so node information here is set to NaN.

function clg=factor_to_mvn(factor)

clg.nodes=nan;
clg.vars=factor.v;
clg.joint=nan;
clg.pa_nodes=[];
clg.pa_vars=[];
clg.pa_joint=[];

clg.n=nan;
clg.sz=factor.n;
clg.pa_n=0;
clg.pa_sz=0;

factor.K=check_sigma(factor.K);
clg.s=inv(factor.K);
clg.b0=clg.s*factor.h;
clg.b=zeros([clg.sz 0]);

clg.note=factor.note;

end

