function f2=factor_reduce_multiple(f,obs_vars,obs_vals)

f2=f;

for i=1:length(f2)
    [reduce_vars,junk,reduce_idx]=my_intersect(f2(i).v,obs_vars);
    reduce_vals=obs_vals(reduce_idx);
    f2(i)=factor_reduce(f2(i),reduce_vars,reduce_vals);
end

end

