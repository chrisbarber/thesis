% clg2=condition_mvn(clg,obs_vars,obs_vals)
%
% see http://en.wikipedia.org/wiki/Multivariate_normal#Conditional_distributions
%

function clg2=condition_mvn(clg,obs_vars,obs_vals)

if isempty(obs_vars)
    clg2=clg;
else
    assert(0==clg.pa_n); % ensure input is mvn

    [cond_vars,junk,idx]=my_intersect(obs_vars,clg.vars);
    leftover_idx=(1:clg.sz)'; leftover_idx(idx)=[];

    assert(numel(cond_vars)==numel(obs_vars));

    clg2.vars=clg.vars(leftover_idx);
    clg2.joint=clg.joint(leftover_idx);
    clg2.nodes=my_unique(clg.joint);
    clg2.pa_nodes=[];
    clg2.pa_vars=[];
    clg2.pa_joint=[];
    clg2.n=numel(clg2.nodes);
    clg2.sz=numel(clg2.vars);
    clg2.pa_n=0;
    clg2.pa_sz=0;

    m1=clg.b0(leftover_idx);
    m2=clg.b0(idx);
    s11=clg.s(leftover_idx,leftover_idx);
    s21=clg.s(idx,leftover_idx);
    s12=clg.s(leftover_idx,idx);
    s22=clg.s(idx,idx);

    s22=check_sigma(s22);
    s22_inv=inv(s22);
    clg2.b0=m1+s12*s22_inv*(obs_vals-m2);
    clg2.b=zeros([clg2.sz 0]);
    clg2.s=s11-s12*s22_inv*s21;
end

end

