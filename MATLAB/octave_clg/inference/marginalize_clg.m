function clg2=marginalize_clg(clg,vars_to_keep)

clg2=clg;

if ~isempty(vars_to_keep)
    assert(isempty(my_intersect(vars_to_keep,clg.pa_vars)));

    [junk,junk,leftover_idx]=my_intersect(vars_to_keep,clg.vars);

    clg2.vars=clg2.vars(leftover_idx);
    if ~(1==numel(clg2.joint) && isnan(clg2.joint))
        clg2.joint=clg2.joint(leftover_idx);
    end
    clg2.nodes=my_unique(clg2.joint);
    clg2.n=numel(clg2.nodes);
    clg2.sz=numel(clg2.vars);

    clg2.b0=clg2.b0(leftover_idx);
    clg2.b=clg2.b(leftover_idx,:);
    clg2.s=clg2.s(leftover_idx,leftover_idx);
end

end

