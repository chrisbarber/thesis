function f=factor_marginalize(factor,v)

f=factor;

if ~isempty(v)
    [junk,junk,idx]=my_intersect(v,f.v); % find vars to marginalize over
    leftover_idx=(1:f.n)'; leftover_idx(idx)=[]; % find remaining idx
    f.v=f.v(leftover_idx);
    f.n=numel(f.v);
    f.note='';

    Kxx=f.K(leftover_idx,leftover_idx);
    Kxy=f.K(leftover_idx,idx);
    Kyx=f.K(idx,leftover_idx);
    Kyy=f.K(idx,idx);

    Kyy=check_sigma(Kyy);

    hx=f.h(leftover_idx);
    if isempty(hx) && size(hx,1)==1; hx=hx'; end;
    hy=f.h(idx);

    Kyy_inv=inv(Kyy);
    Kxy_Kyy_inv=Kxy*Kyy_inv;
    f.K=Kxx-Kxy_Kyy_inv*Kyx;
    if isempty(hx)
        f.h=hx;
    else
        f.h=hx-Kxy_Kyy_inv*hy;
    end
    f.g=f.g+0.5*(numel(idx)*log(2*pi)-log(det(Kyy))+hy'*Kyy_inv*hy);
end

end

