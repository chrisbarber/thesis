% [posterior prior]=naive(clgs,obs_vars,obs_vals)
%
% Performs inference on a Vector Auto-Regressive model by the naive method of
% constructing the full MVN joint prior, and conditioning on evidence.

function [posterior prior]=naive(clgs,obs_vars,obs_vals)

prior=clgs(1);
for i=2:length(clgs)
    % enter evidence for deterministic parents if any
    deterministic_pa_vars=my_setdiff(clgs(i).pa_vars,prior.vars);
    [junk,junk,deterministic_pa_idx]=my_intersect(deterministic_pa_vars,obs_vars);
    deterministic_pa_vals=obs_vals(deterministic_pa_idx);
    clgs(i)=condition_clg(clgs(i),deterministic_pa_vars,deterministic_pa_vals);

    % absorb timestep i into the prior
    prior=absorb_node(prior,clgs(i));
end

posterior=condition_mvn(prior,obs_vars,obs_vals);

end

