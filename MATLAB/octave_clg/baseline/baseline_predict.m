function predictions=baseline_predict(model,test_set,experiment)

%include('experiment/');
%include('util/');

predictions.data_idx=find(~test_set.is_deterministic);
predictions.N=numel(predictions.data_idx);
predictions.T=test_set.T;
predictions.horizon=experiment.model_config.horizon;
predictions.timesteps=test_set.timesteps;
predictions.point_prediction_made=zeros([predictions.N predictions.T]);
predictions.site=test_set.site(predictions.data_idx);
predictions.units=test_set.units(predictions.data_idx);

predictions.d=nan([predictions.N predictions.T]);

if strcmp(experiment.baseline.model,'persistent_gaussian')
    predictions.num_regimes=1;
    predictions.regime_posterior=zeros([1 predictions.T]);
    predictions.ll=0;

    for t=1:predictions.T
        output_idx=find((predictions.timesteps(t)+predictions.horizon)==predictions.timesteps);

        if ~isempty(output_idx)
            x=test_set.d(predictions.data_idx,t);
            gmm.num_regimes=1;
            gmm.regime_posterior=0;
            gmm.mvn{1}.b0=model.sample_mean;
            obs=~isnan(x);
            gmm.mvn{1}.b0(obs)=x(obs);
            gmm.mvn{1}.s=model.sample_cov;

            % make point point prediction

            slice=point_predict(gmm,'expected_value'); % method is irrelevant
            predictions.d(obs,output_idx)=slice(obs);
            predictions.point_prediction_made(obs,output_idx)=1; % mark as predicted

            % multiply in log-like for this step

            predictions.ll=predictions.ll+gmm_density(gmm,x);
        end
    end
else
    error(['unsupported baseline model ' experiment.baseline.model]);
end

end

