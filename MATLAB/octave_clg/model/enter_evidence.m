% posterior=enter_evidence(model,window,algorithm)
%
% Given a matrix of observations over a window, and missing values indicated by
% NaN's, compute the posterior distribution over missing values using the
% specified inference algorithm.  Algorithm may be 'naive,' 'var_elim,' or
% 'junction_tree.'  Note that Naive and VE output the joint MVN over missing
% values, whereas Junction Tree outputs a clique tree.  Optional argument is
% for VE queries only.  See model/query.m.

function posterior=enter_evidence(model,window,algorithm,varargin)

%include('inference/');
%include('util/');

posterior.num_regimes=model.config.num_regimes;
posterior.algorithm=algorithm;
posterior.markov_order=model.config.markov_order;

% set up VE query if requested

if ~isempty(varargin)
    assert(strcmp(algorithm,'var_elim'));
    query_mask=varargin{1};
else
    query_mask=ones([window.N window.T]);
end

% set up inputs

obs=~isnan(window.d(:));
obs_vals=window.d(obs);

% save obs_vals into posteriors structure
posterior.obs_vals=obs_vals;

% compute posteriors in each regime

posterior.obs_pr=nan([model.config.num_regimes 1]);
for r=1:model.config.num_regimes

    % set up inputs for this regime

    clgs=model.params.clg(r,:);
    factors=model.params.factors{r};
    ar_order=model.config.markov_order;
    window_vars=get_window_vars(model,window,r);
    obs_vars=window_vars(obs);
    posterior.obs_vars{r}=obs_vars; % save obs_vars in the posterior

    % perform inference

    if strcmp(algorithm,'naive')
        [posterior.mvn{r} prior]=naive(clgs,obs_vars,obs_vals);
        [junk,junk,prior_obs_idx]=my_intersect(obs_vars,prior.vars);
        posterior.obs_pr(r)=logmvnpdf(obs_vals,prior.b0(prior_obs_idx),prior.s(prior_obs_idx,prior_obs_idx));
    elseif strcmp(algorithm,'var_elim')
        elim_vars=window_vars(~query_mask);
        temp_f=var_elim(factors,ar_order,elim_vars,obs_vars,obs_vals);
        posterior.mvn{r}=factor_to_mvn(temp_f);
        temp_f=factor_marginalize(temp_f,temp_f.v);
        % this is another way to compute the obs_pr but is more time consuming:
        % temp_f=var_elim(factors,ar_order,my_setdiff(all_vars,obs_vars),obs_vars,obs_vals);
        posterior.obs_pr(r)=temp_f.g;
    elseif strcmp(algorithm,'junction_tree')
        posterior.cliques{r}=junction_tree(factors,ar_order,obs_vars,obs_vals);
        temp_f=factor_marginalize(posterior.cliques{r}(1),posterior.cliques{r}(1).v);
        posterior.obs_pr(r)=temp_f.g;
    else
        error(['Unsupported inference algorithm: ' algorithm]);
    end
end

% multiply by regime priors
posterior.regime_posterior=posterior.obs_pr+log(model.params.regime_pr);
% normalize regime posterior
posterior.regime_posterior=lognorm(posterior.regime_posterior,1);

% calculate log-likelihood of entire window
posterior.ll=logsum(model.params.regime_pr+posterior.obs_pr,1);

end

