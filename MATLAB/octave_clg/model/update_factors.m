function model=update_factors(model)

%include('model/');

for r=1:model.config.num_regimes
    model.params.factors{r}=make_factors(model,r);
end

end
