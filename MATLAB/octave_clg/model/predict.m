% outputs point & probabilistic predictions

function predictions=predict(model,test_set,experiment)

%include('data/');
%include('model/');
%include('inference/');
%include('util/');
%include('experiment/');

% copy in relevant meta-information

predictions.data_idx=find(~test_set.is_deterministic);
predictions.N=numel(predictions.data_idx);
predictions.T=test_set.T;
predictions.horizon=model.config.horizon;
predictions.timesteps=test_set.timesteps;
predictions.num_regimes=model.config.num_regimes;
predictions.site=test_set.site(predictions.data_idx);
predictions.units=test_set.units(predictions.data_idx);
predictions.point_prediction_method=experiment.predict.point_prediction_method;

% initialize fields for probabilistic prediction results

point_prediction_made=zeros([predictions.N predictions.T]);
regime_posterior=nan([model.config.num_regimes predictions.T]);
d=nan([predictions.N predictions.T]);

% do probabilistic predictions

predictions.ll=0;
experiment=progress(experiment,'push','prediction');
for window_serial_idx=1:test_set.num_windows
    experiment=progress(experiment,'update',[num2str(round(100*window_serial_idx/test_set.num_windows)) '%']);
    window=get_window(test_set,model.config.window_length,model.config.horizon,window_serial_idx);

    if ismember(window.timesteps(1),test_set.interval_starts)
        % if window is at the beginning of a contiguous interval, we have to predict all steps
        intra_window=1:window.T;
    else
        % otherwise the last step of the window is the only step not covered by the last window
        intra_window=window.T;
    end

    for t=intra_window

        % calculate posterior over prediction variables

        conditioning_window=window; conditioning_window.d(predictions.data_idx,t:window.T)=nan;
        query_mask=zeros([window.N window.T]); query_mask(predictions.data_idx,t)=1;
        posterior=query(model,conditioning_window,query_mask);

        % save regime posterior for this slice, and do point prediction
        
        output_idx=find(window.timesteps(t)==predictions.timesteps); % time being predicted
        regime_posterior(:,output_idx)=posterior.regime_posterior;
        this_d=point_predict(posterior,experiment.predict.point_prediction_method);
        this_d=this_d(predictions.data_idx);
        d(:,output_idx)=this_d;
        point_prediction_made(:,output_idx)=1; % mark prediction as having been made

        % get mvn's for each regime from posterior in order to compute unrolled log-like
        
        x=window.d(predictions.data_idx,t);
        gmm.num_regimes=posterior.num_regimes;
        gmm.regime_posterior=posterior.regime_posterior;

        for r=1:model.config.num_regimes
            % extract posterior mvn over predict vars
            window_vars=get_window_vars(model,window,r);
            predict_vars=window_vars(predictions.data_idx,t);
            gmm.mvn{r}=marginalize_clg(posterior.mvn{r},predict_vars);
        end

        % multiply into total ll
        this_ll=gmm_density(gmm,x);
%        if ~isnan(this_ll)
            predictions.ll=predictions.ll+this_ll;
%        end
    end
end
predictions.point_prediction_made=point_prediction_made;
predictions.regime_posterior=regime_posterior;
predictions.d=d;

experiment=progress(experiment,'pop');

end

