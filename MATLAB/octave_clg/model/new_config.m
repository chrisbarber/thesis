function config=new_config(num_regimes,window_length,markov_order,horizon)

config.num_regimes=num_regimes;
config.window_length=window_length;
config.markov_order=markov_order;
config.horizon=horizon;

end

