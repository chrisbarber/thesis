% f=make_factors(model,regime)
%
% Make canonical gaussian factors for purposes of inference, within a regime,
% by converting CLGs to canonical form.

function f=make_factors(model,regime)

%include('inference/');

f=[];
for t=1:model.config.window_length+1
    f=[f clg_to_factor(model.params.clg(regime,t))];
end

end

