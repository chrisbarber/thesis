function clg=make_cpds(model)

%include('inference/');


for r=1:model.config.num_regimes
    for t=1:model.config.window_length+1

        % assemble a clg from all (non-deterministic) nodes in this timeslice

        nodes=[];
        vars=[];
        joint=[];
        pa_nodes=[];
        pa_vars=[];
        pa_joint=[];

        for i=1:length(model.graph.by_slice{r,t})
            node=model.graph.nodes(model.graph.by_slice{r,t}(i));

            if ~node.is_deterministic
                nodes=[nodes; node.node];
                vars=[vars; node.vars];
                joint=[joint; node.joint];

                pa_nodes=[pa_nodes; node.pa_nodes];
            end
        end

        % collapse same parents
        pa_nodes=my_unique(pa_nodes);

        for i=1:length(pa_nodes)
            node=model.graph.nodes(pa_nodes(i));
            pa_vars=[pa_vars; node.vars];
            pa_joint=[pa_joint; node.joint];
        end 

        clg(r,t)=new_clg(nodes,vars,joint,pa_nodes,pa_vars,pa_joint);
    end
end

end

