function results=run_experiment(experiment)

rand('state',experiment.rand_seed);
randn('state',experiment.rand_seed);

include('.');
include('model/');
include('util/');
include('em/');
include('experiment/');
include('inference/');
include('data/');
include('baseline/');

model=new_model(experiment);

results=cv(model,experiment);

end

