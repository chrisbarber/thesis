function [bits nats hartleys] = entropy(log_dist)

dist=exp(log_dist);
bits=0;
nats=0;
hartleys=0;
for i=1:length(dist)
    bits=bits-dist(i)*log(dist(i))/log(2);
    nats=nats-dist(i)*log(dist(i));
    hartleys=hartleys-dist(i)*log(dist(i))/log(10);
end

