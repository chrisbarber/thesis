% return the string that disp() would print

function s=sdisp(var)

s=evalc('disp(var)');

end

