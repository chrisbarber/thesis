function Y = improvement(Eref,Emodel)

Y=100*(Eref-Emodel)./Eref;

end