function zlog_X = zlog(X)

% this replacement to log() behaves as follows:
% 1. positive values are the same as when using log()
% 2. zero values produce log(realmin) instead of -Inf.
% 3. negative values cause an error instead of returning a complex #.

if any(X<0)
    error('negative value supplied to zlog');
else
    zlog_X=log(X+realmin*(0==X));
end