function log_a_plus_c = logaddalpha(log_a, log_c, log_alpha)

a=exp(log_a-log_alpha);
c=exp(log_c-log_alpha);
log_a_plus_c=log(a+c)+log_alpha;
