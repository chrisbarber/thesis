function output = CV(data, folds, foo, varargin)

[num_seq seq_len]=size(data);

% reproduce folds from 'folds' parameter
k=max(folds(:));
for fold=1:k
    testSetIndices{fold}=find(folds==fold);
    trainingSetIndices{fold}=my_setdiff(1:num_seq,testSetIndices{fold});
end

if k<=1 || k>num_seq
    error('k must be between 1 and the number of sequences');
end

output=cell([1 k]);
for fold=1:k
    output{fold}=foo(fold,data(trainingSetIndices{fold},:),data(testSetIndices{fold},:),varargin{:});
end

