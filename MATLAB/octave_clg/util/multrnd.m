% returns 1 sample from a multinomial specified by the vector p, which is expected to be normalized.

function x=multrnd(p)

x=find(mnrnd(1,p));

end
