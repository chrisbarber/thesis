function log_a_plus_c = logadd(log_a, log_c)

if isinf(log_c) && log_c<0
    log_a_plus_c=log_a;
else
    log_alpha=max(max(log_a(:)),max(log_c(:)));
    a=exp(log_a-log_alpha);
    c=exp(log_c-log_alpha);
    log_a_plus_c=log(a+c)+log_alpha;
end

end

