function fold=run_fold(model,experiment,fold_idx)

fold.date_run=now();
[junk,ret]=system('hostname');
fold.hostname=ret;
[junk,ret]=system('echo $LSB_JOBINDEX');
if ~isempty(ret)
    fold.jobindex=str2num(ret);
else
    fold.jobindex=[];
end
experiment=progress(experiment,'update',num2str(fold_idx));

% load data for this fold
fold_data=get_training_set(experiment,fold_idx);
test_set=get_test_set(experiment,fold_idx);

% now do parameter learning
[learned_model em_info]=em(model,fold_data,experiment);

% get maximum-likelihood prediction
predictions=predict(learned_model,test_set,experiment);

% do error evaluation on test set
evaluation=evaluate(predictions,test_set,experiment);

% do baseline model
baseline.model=baseline_learn(fold_data,experiment);

baseline.predictions=baseline_predict(baseline.model,test_set,experiment);
baseline.evaluation=evaluate(baseline.predictions,test_set,experiment);

% compare model to baseline
comparison=compare(baseline.evaluation,evaluation);

% store everything into results structure
fold.learned_model=learned_model;
fold.em_info=em_info;
fold.predictions=predictions;
fold.evaluation=evaluation;
fold.baseline=baseline;
fold.comparison=comparison;

% TODO: provide option specifying whether to return predictions & evaluation, versus saving to files

file=[experiment.root 'results_' num2str(fold_idx) '.mat'];
save(file,'fold');

fold.predictions=file;
fold.evaluation=file;
fold.baseline.predictions=file;
fold.baseline.evaluation=file;
fold.comparison.both_predicted=file;
fold.comparison.both_spd_predicted=file;

end
