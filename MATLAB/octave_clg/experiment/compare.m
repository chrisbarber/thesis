function comparison=compare(baseline_evaluation,evaluation)

%include('util/');

comparison.N=evaluation.N;
comparison.site=evaluation.site;
comparison.units=evaluation.units;
comparison.rel_ll=evaluation.ll-baseline_evaluation.ll;
comparison.both_predicted=and(evaluation.point_prediction_made,baseline_evaluation.point_prediction_made);

MAE=nanmean(abs(evaluation.err.*comparison.both_predicted),2);
RMSE=nanmean((evaluation.err.*comparison.both_predicted).^2,2).^0.5;
baseline_MAE=nanmean(abs(baseline_evaluation.err.*comparison.both_predicted),2);
baseline_RMSE=nanmean((baseline_evaluation.err.*comparison.both_predicted).^2,2).^0.5;

comparison.MAE_improvement=improvement(baseline_MAE,MAE);
comparison.RMSE_improvement=improvement(baseline_RMSE,RMSE);

comparison.spd_site=evaluation.spd_site;
comparison.both_spd_predicted=and(evaluation.spd_prediction_made,baseline_evaluation.spd_prediction_made);

spd_MAE=nanmean(abs(evaluation.spd_err.*comparison.both_spd_predicted),2);
spd_RMSE=nanmean((evaluation.spd_err.*comparison.both_spd_predicted).^2,2).^0.5;
baseline_spd_MAE=nanmean(abs(baseline_evaluation.spd_err.*comparison.both_spd_predicted),2);
baseline_spd_RMSE=nanmean((baseline_evaluation.spd_err.*comparison.both_spd_predicted).^2,2).^0.5;

comparison.spd_MAE_improvement=improvement(spd_MAE,baseline_spd_MAE);
comparison.spd_RMSE_improvement=improvement(spd_RMSE,baseline_spd_RMSE);

end

