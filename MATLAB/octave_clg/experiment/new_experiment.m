function experiment=new_experiment(root,data_src,variable_name,model_config,rand_seed,num_stations,num_deterministic,stations,num_station_variables,station_variables,station_joint,global_joint,deterministic_idx,startdate,enddate,num_folds,folds,em_config,predict,baseline)

load(data_src);
eval(['data=' variable_name ';']);

experiment.root=root;
experiment.data_src=data_src;
experiment.variable_name=variable_name;
experiment.timestep=data.timestep;
experiment.model_config=model_config;
experiment.rand_seed=rand_seed;
experiment.num_stations=num_stations;
experiment.num_deterministic=num_deterministic;
experiment.stations=stations;
experiment.num_station_variables=num_station_variables;
experiment.station_variables=station_variables;
experiment.station_joint=station_joint;
experiment.global_joint=global_joint;
experiment.deterministic_idx=deterministic_idx;
experiment.station_names=data.station_names(stations);
experiment.startdate=startdate;
experiment.enddate=enddate;
experiment.num_folds=num_folds;
experiment.folds=folds;
experiment.em=em_config;
experiment.predict=predict;
experiment.baseline=baseline;
experiment.units=data.units;

end

