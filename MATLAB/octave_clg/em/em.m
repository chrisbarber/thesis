function [best_model em_info]=em(model,data,experiment)

%include('em/');
%include('model/');

tic;

if any(all(isnan(data.d),2))
    error(['training set for fold ' num2str(data.fold_idx) ' has some sites with no observations']);
end

best_ll=-inf;
experiment=progress(experiment,'push','em random-restart');
for restart=1:experiment.em.random_restarts
    experiment=progress(experiment,'update',num2str(restart));

    temp_model=randomize_model(model,experiment);
    best_model=temp_model;

    iter=0;
    delta_ll=nan;
    last_ll=-inf;
    temp_em_info.em_halt='reached_max_iter';
    temp_em_info.iter=experiment.em.max_iterations;
    temp_em_info.training_set_ll=[];
    experiment=progress(experiment,'push','iteration');
    while iter<experiment.em.max_iterations
        iter=iter+1;
        experiment=progress(experiment,'update',[num2str(iter) ' (delta log-like=' num2str(delta_ll) ')']);
                
        [ess ll]=e_step(temp_model,data,experiment);

        temp_model=m_step(model,ess,experiment);

        delta_ll=(ll-last_ll)/abs(last_ll);
        %assert(delta_ll>=0);
        if delta_ll<=experiment.em.convergence
            temp_em_info.em_halt='converged';
            temp_em_info.iter=iter;
            break;
        end
        temp_em_info.training_set_ll(end+1)=ll;
        last_ll=ll;
    end
    experiment=progress(experiment,'pop');

    if ll>best_ll
        best_ll=ll;
        best_model=temp_model;
        em_info=temp_em_info;
    end
end
experiment=progress(experiment,'pop');

em_info.training_time=toc;

end

