function model=m_step(model,ess,experiment)

%include('util/');
%include('model/');

experiment=progress(experiment,'push','m-step');
for i=1:length(model.graph.nodes)
    % experiment=progress(experiment,'update',[num2str(round(100*i/length(model.graph.nodes))) '%']);
    node=model.graph.nodes(i);

    if ~node.is_deterministic
        r=node.regime;
        t=node.tstep;

        [junk,junk,ess_idx]=my_intersect(node.source_nodes,model.graph.ess_nodes);
        assert(~isempty(ess_idx));
        node_ess=sum_ess(ess.nodes(ess_idx));

        model.params.clg(r,t)=mle_clg(model.params.clg(r,t),node,node_ess,ess.w(r));
    end
end
experiment=progress(experiment,'pop');

model.params.regime_pr=normalize(ess.w,1);

model=update_factors(model);

end


function ess2=sum_ess(ess)

ess2=ess(1);
for i=2:length(ess)
    ess2.sx=ess2.sx+ess(i).sx;
    ess2.sy=ess2.sy+ess(i).sy;
    ess2.sxx=ess2.sxx+ess(i).sxx;
    ess2.syy=ess2.syy+ess(i).syy;
    ess2.sxy=ess2.sxy+ess(i).sxy;
end

end

