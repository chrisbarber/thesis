% clg2=mle_clg(clg,node,ss,w)
%
% Update clg parameters to their Maximum Likelihood Estimate given sufficient
% statistics, for the specified node within the clg.

function clg2=mle_clg(clg,node,ss,w)

[junk,junk,idx]=my_intersect(node.vars,clg.vars);
[junk,junk,pa_idx]=my_intersect(node.pa_vars,clg.pa_vars);
assert(numel(idx)==node.sz && numel(pa_idx)==node.pa_sz);

clg2=clg;

szz=[ss.sxx ss.sx; ss.sx' w];
szy=[ss.sxy; ss.sy'];

szz=check_sigma(szz);
a=szy'*inv(szz);
clg2.b0(idx)=a(:,end);
clg2.b(idx,pa_idx)=a(:,1:end-1);
clg2.s(idx,idx)=(ss.syy-a*szy)/w;

end

