function [sx sy sxx syy sxy]=get_ess(node,posterior)

%include('inference/');

x_vars=node.pa_vars;
y_vars=node.vars;

% useful my_intersections & indices

[x_obs_vars,x_obs_idx,x_obs_val_idx]=my_intersect(x_vars,posterior.obs_vars{node.regime});
[y_obs_vars,y_obs_idx,y_obs_val_idx]=my_intersect(y_vars,posterior.obs_vars{node.regime});
[junk,x_mis_idx]=my_setdiff(x_vars,x_obs_vars);
[junk,y_mis_idx]=my_setdiff(y_vars,y_obs_vars);

% intitialize sufficient statistics matrices

sx=nan([node.pa_sz 1]);
sy=nan([node.sz 1]);
% covariance will be zero where at least 1 RV is observed
sxx=zeros([node.pa_sz node.pa_sz]);
syy=zeros([node.sz node.sz]);
sxy=zeros([node.pa_sz node.sz]);

% expected means for observed variables are just the observations
sx(x_obs_idx)=posterior.obs_vals(x_obs_val_idx);
sy(y_obs_idx)=posterior.obs_vals(y_obs_val_idx);

% fill in the remainder of the sufficient statistics using the posterior

if strcmp(posterior.algorithm,'naive') || strcmp(posterior.algorithm,'var_elim')
    mvn=posterior.mvn{node.regime};
elseif strcmp(posterior.algorithm,'junction_tree')
    % retreive the relevant clique
    clique_idx=max([1 node.tstep-posterior.markov_order]);
    clique=posterior.cliques{node.regime}(clique_idx);

    % convert to mvn
    mvn=factor_to_mvn(clique);    
else
    error(['Unsupported inference algorithm: ' posterior.algorithm]);
end

[junk,junk,x_mis_mvn_idx]=my_intersect(x_vars,mvn.vars);
[junk,junk,y_mis_mvn_idx]=my_intersect(y_vars,mvn.vars);

% these are just the mus from the posterior
sx(x_mis_idx)=mvn.b0(x_mis_mvn_idx);
sy(y_mis_idx)=mvn.b0(y_mis_idx);

% find the covariances (if either was observed it is 0 from initialization)
sxx(x_mis_idx,x_mis_idx)=mvn.s(x_mis_mvn_idx,x_mis_mvn_idx);
syy(y_mis_idx,y_mis_idx)=mvn.s(y_mis_mvn_idx,y_mis_mvn_idx);
sxy(x_mis_idx,y_mis_idx)=mvn.s(x_mis_mvn_idx,y_mis_mvn_idx);

% add E[X_i]*E[X_j] to get the desired statistics E[X_i * X_j]
sxx=sxx+sx*sx';
syy=syy+sy*sy';
sxy=sxy+sx*sy';

end

