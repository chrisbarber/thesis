function fold_data=get_fold_data(experiment,fold_idx)

% load data if it isn't already in persistent storage

persistent persistent_data data_src variable_name
if (strcmp(data_src,experiment.data_src) && strcmp(variable_name,experiment.variable_name)) ...
    || isempty(data_src) || isempty(variable_name)

    data_src=experiment.data_src;
    variable_name=experiment.variable_name;

    %assert(isempty(strmatch(experiment.variable_name,{'experiment','fold_idx','data_src','variable_name'},'exact'))); % make sure we don't clobber a local variable
    %load(experiment.data_src,experiment.variable_name);
    %eval(['persistent_data=' experiment.variable_name ';']);
    temp=load(experiment.data_src,experiment.variable_name);
    persistent_data=temp.(experiment.variable_name);
end

% initialize fold data to empty

fold_data.fold_idx=fold_idx;
fold_data.timesteps=experiment.folds{fold_idx};
fold_data.d=[];
fold_data.is_deterministic=[];
fold_data.site=[];
fold_data.units=[];
fold_data.deterministic_idx=[];

% put deterministic data

for i=1:experiment.num_deterministic
    fold_data.is_deterministic=[fold_data.is_deterministic; 1];
    fold_data.site=[fold_data.site; nan];
    fold_data.units=[fold_data.units; nan];
    fold_data.deterministic_idx=[fold_data.deterministic_idx; i];

    fold_data.d=[fold_data.d; persistent_data.deterministic(experiment.deterministic_idx(i),fold_data.timesteps)];
end

% now append station variables

for i=1:experiment.num_stations
    for j=1:experiment.num_station_variables
        fold_data.is_deterministic=[fold_data.is_deterministic; 0];
        fold_data.site=[fold_data.site; i];
        fold_data.units=[fold_data.units; j];
        fold_data.deterministic_idx=[fold_data.deterministic_idx; nan];

        variable_data=persistent_data.(experiment.station_variables{j});
        fold_data.d=[fold_data.d; variable_data(experiment.stations(i),fold_data.timesteps)];
    end
end

fold_data.N=size(fold_data.d,1);
fold_data.T=size(fold_data.d,2);

% compute and store window positions (dependent on window length), which is important for non-contiguous fold_data.timesteps

fold_data.window_duration=experiment.model_config.window_length+experiment.model_config.horizon;
interval_boundaries=ravel(find(diff(fold_data.timesteps)>1)); % boundaries between contiguous intervals
fold_data.interval_starts=fold_data.timesteps([1; interval_boundaries+1]);
fold_data.interval_ends=fold_data.timesteps([interval_boundaries; end]);
fold_data.interval_lengths=fold_data.interval_ends-fold_data.interval_starts+1;
fold_data.interval_nwindows=max(0,fold_data.interval_lengths-fold_data.window_duration+1);
fold_data.interval_serial_idx=cumsum(fold_data.interval_nwindows)-fold_data.interval_nwindows+1;
fold_data.num_windows=sum(fold_data.interval_nwindows);

end

