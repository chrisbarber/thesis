function r=include(s)

PROJECT_ROOT='/home/cabarber_a/octave_clg/';

s=[PROJECT_ROOT s];
prev_state=warning('off','MATLAB:rmpath:DirNotFound');
rmpath(s);
warning(prev_state.state,'MATLAB:rmpath:DirNotFound');
addpath(s);

end

