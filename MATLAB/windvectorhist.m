function windvectorhist(ew_component, ns_component, max_speed, numbuckets)

assert(isvector(ew_component) && isvector(ns_component));
assert(length(ew_component)==length(ns_component));

ew_component(abs(ew_component)>max_speed)=NaN;
ns_component(abs(ns_component)>max_speed)=NaN;

wrap=2*max_speed*ns_component+ew_component;
n=hist(wrap, numbuckets^2);

n=reshape(n,[numbuckets numbuckets]);

limits=[-max_speed max_speed];
imagesc(limits,limits,n);
axis xy;