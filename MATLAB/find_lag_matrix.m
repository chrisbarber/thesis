% for each pair of stations, find the lag at which the cross-correlogram
% peaks.  then lag_matrix(s1,s2) is that value for stations s1, s2
function lag_matrix=find_lag_matrix(D, minlag, maxlag)

numstations=length(D);

lag_matrix=zeros(numstations);

for s1=1:numstations
    for s2=1:numstations
        [xcorr,lags]=myxcorr(D{s1}.S,D{s2}.S,minlag,maxlag);
        idx_with_max_xcorr=find(xcorr==max(xcorr));
        if isempty(idx_with_max_xcorr)
            lag_with_max_xcorr=NaN;
        else
            lag_with_max_xcorr=lags(idx_with_max_xcorr);
        end
        lag_matrix(s1,s2)=lag_with_max_xcorr;
        
        %%%%%%%%%%%%%%%%%%%%%%%%
        if lag_with_max_xcorr~=0
            fprintf(1,'(%s,%s)=%d\n',D{s1}.name,D{s2}.name,lag_with_max_xcorr);
        end
        %%%%%%%%%%%%%%%%%%%%%
    end
end

end