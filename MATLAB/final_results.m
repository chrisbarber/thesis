function final_results(wi,pnw)

addpath('matlab_clg\');
include_all;

disp(' ');
disp('Off-site');

r1=[ravel(wi.voo.evaluation.RMSE); ravel(pnw.voo.evaluation.RMSE)];
r2=[ravel(wi.evaluation.RMSE(:,:,:,1,:)); ravel(pnw.evaluation.RMSE(:,:,:,1,:))];
compare_results(r1,r2);

disp('Window length');

r1=[ravel(wi.evaluation.RMSE(:,1,:,:,:)); ravel(pnw.evaluation.RMSE(:,1,:,:,:))];
r2=[ravel(wi.evaluation.RMSE(:,3,:,:,:)); ravel(pnw.evaluation.RMSE(:,3,:,:,:))];
compare_results(r1,r2);

disp('Regimes');

r1=[ravel(wi.evaluation.RMSE(1,3,:,:,:)); ravel(pnw.evaluation.RMSE(1,3,:,:,:))];
r2=[ravel(wi.evaluation.RMSE(4,3,:,:,:)); ravel(pnw.evaluation.RMSE(4,3,:,:,:))];
compare_results(r1,r2);

disp('Persistence');

r1=[ravel(wi.baseline.RMSE(4,3,:,:,:)); ravel(pnw.baseline.RMSE(4,3,:,:,:))];
r2=[ravel(wi.evaluation.RMSE(4,3,:,:,:)); ravel(pnw.evaluation.RMSE(4,3,:,:,:))];
compare_results(r1,r2);

disp('ARMA');

r1=[ravel(wi.arma.evaluation.RMSE); ravel(pnw.arma.evaluation.RMSE)];
r2=[ravel(wi.evaluation.RMSE(4,3,:,:,:)); ravel(pnw.evaluation.RMSE(4,3,:,:,:))];
compare_results(r1,r2);

disp('Air temperature');

r1=ravel(wi.airtmp.evaluation.RMSE);
r2=ravel(wi.evaluation.RMSE(4,3,:,1,:));
compare_results(r1,r2);

figure;
errorbar([ravel(mean(wi.comparison.RMSE_imp(4,3,:,:,:),5)) ravel(mean(wi.arma.comparison.RMSE_improvement(1,1,:,:,:),5))],[ravel(std(wi.comparison.RMSE_imp(4,3,:,:,:),0,5)) ravel(std(wi.arma.comparison.RMSE_improvement(1,1,:,:,:),0,5))])
title('WI');

figure;
errorbar([ravel(mean(pnw.comparison.RMSE_imp(4,3,:,:,:),5)) ravel(mean(pnw.arma.comparison.RMSE_improvement(1,1,:,:,:),5))],[ravel(std(pnw.comparison.RMSE_imp(4,3,:,:,:),0,5)) ravel(std(pnw.arma.comparison.RMSE_improvement(1,1,:,:,:),0,5))])
title('PNW');

figure;
wi_regime_imp=100*bsxfun(@rdivide,bsxfun(@plus,wi.evaluation.RMSE(1,3,:,1,:),-wi.evaluation.RMSE(:,3,:,1,:)),wi.evaluation.RMSE(1,3,:,1,:));
pnw_regime_imp=100*bsxfun(@rdivide,bsxfun(@plus,pnw.evaluation.RMSE(1,3,:,1,:),-pnw.evaluation.RMSE(:,3,:,1,:)),pnw.evaluation.RMSE(1,3,:,1,:));
errorbar([ravel(mean(wi_regime_imp,5)) ravel(mean(pnw_regime_imp,5))],[ravel(std(wi_regime_imp,0,5)) ravel(std(pnw_regime_imp,0,5))]);
title('Effect of Regimes (h=1)');

end

function compare_results(r1,r2)

disp(' ');
better=r1>r2;
disp(['#: ' num2str(sum(better)) '/' num2str(numel(better))]);
disp(['imp: ' num2str(mean(improvement(r1,r2))) '%']);
[junk,p]=ttest(r1,r2);
disp(['p: ' num2str(p)]);
disp(' ');

end