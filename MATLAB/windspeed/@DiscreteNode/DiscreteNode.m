classdef DiscreteNode < Node
    %DISCRETENODE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetAccess = private)
        size % node takes on values from 1:size
        obs % current setting to this node (can be empty)
        samples % samples for node (only if unobserved), cell array
        % TODO: allow discrete nodes to have continuous parents,
        %   using probit distribution
        CPD % struct specifying multinomial distribution
    end
    
    methods
        function me = DiscreteNode(name, size)
            me = me@Node(name);
            me.size = size;
            resizeCPD(me);
        end
        
        function enterEvidence(me, evidence)
            assert(isscalar(evidence));
            me.obs=evidence;
        end
        
        function sampleNode(me)
            me.samples = cell([me.nsamples 1]);
            for s=1:me.nsamples
                dist = condition(me, s);
                me.samples{s} = Utils.multrnd(dist, 1);
            end
        end
        
        function p = posterior(me, sampleIndex)
            if isempty(me.obs)
                p = Utils.PROBABILITY_ONE;
            else
                dist = condition(me, sampleIndex);
                p = dist(me.obs);
            end
        end
    end

    methods (Access = protected)
        function resizeCPD(me)
            DiscreteDims = getParentSizes(me, 'DiscreteNode')';
            if hasParentOfType(me, 'LCGNode')
                error('Unsupported or unrecognized parent node type (Discrete nodes currently can only have discrete parents.');
            end

            % TODO: choose a way to randomize.  Should probably be
            % dirichlet.
            % This just sets to uniform (since normalizing zeros ends up
            % uniform):
            % dist = zeros([me.size DiscreteDims]);
            % This normalizes a vector of independent samples from unit
            % uniform distribution.  This might actually boil down to
            % dirichlet but i'm not sure:
            dist = rand([me.size DiscreteDims]);
            
            me.CPD.CPT = Utils.multnorm(dist);
        end
        
        function multinomial = condition(me, sampleIndex)
            dps = getParentSettings(me, 'DiscreteNode');
            if ~isempty(dps{sampleIndex})
                dpindex = mat2cell(dps{sampleIndex},ones([length(dps{sampleIndex}) 1]),1);
            else
                dpindex = {};
            end
            
            multinomial = me.CPD.CPT(:,dpindex{:});
        end
    end
end

