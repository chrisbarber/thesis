classdef DBN < handle
    %DBN Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetAccess = private)
        nodes = {} % all nodes are kept in topological order
        ss1 % number of variables in the first time-slice
        ss % slice-size for t>1
        adjmat % adjacency matrix (sparse)
    end
    
    methods
        % nodes{1} is a cell array of nodes in timeslice 1
        % nodes{2} is a cell array of nodes in slices t>1
        function me = DBN(nodes)
            me.ss1 = length(nodes{1});
            me.ss = length(nodes{2});
            me.nodes = horzcat(nodes{1},nodes{2});
            me.adjmat = adjacencyMatrix(me);
            timeslice1 = graphtopoorder(me.adjmat(1:length(nodes{1}),1:length(nodes{1})));
            timeslice2 = graphtopoorder(me.adjmat(length(nodes{1})+1:end,length(nodes{1})+1:end));
            reorder = [timeslice1 timeslice2+length(nodes{1})];
            me.nodes = me.nodes(reorder);
            me.adjmat = me.adjmat(reorder,reorder);
        end
        
        function names = listNodes(me)
            names = {};
            for n=1:numNodes(me);
                names{end+1} = me.nodes{n}.name;
            end
        end
        
        % compute Pr of evidence vars in current time slice, given evidence
        % from previous slice, and given values from a specific sample for
        % unobserved nodes in current & prev slice
        function p = posterior(me, sampleIndex)
            p = Utils.PROBABILITY_ONE;
            for n=1:numNodes(me)
                p = mulpr(p, posterior(me.nodes{n}, sampleIndex));
            end
        end
        
        function viewDBN(me)
            view(biograph(me.adjmat, listNodes(me)));
        end
    end
    
    methods (Access = private)
        function N = numNodes(me)
            N = me.ss1 + me.ss;
        end
        
        function M = adjacencyMatrix(me)
            N = numNodes(me);
            M = sparse(numNodes(me), numNodes(me));
            for na=1:N
                for nb=1:N
                    nbtype = class(me.nodes{nb});
                    if isfield(me.nodes{na}.children, nbtype)
                        if any(eq(me.nodes{nb}, me.nodes{na}.children.(nbtype)))
                            M(na,nb) = 1;
                        end
                    end
                end
            end
        end
        
        function setNumSamples(me, nsamples)
            for n=1:numNodes(me)
                nodes{n}.nsamples = nsamples;
            end
        end
        
        % see Russel & Norvig Figure 15.15
        function particleFiltering(me, evidence, nsamples)
            % Propagate -- move 1 time-step forward
            for n=1:numNodes(me)
                stepForward(me.nodes{n});
            end
            
            % Propagate -- enter new evidence
            for n=1:numNodes(me)
                if ~isempty(evidence{n})
                    enterEvidence(me.nodes{n}, evidence{n});
                end
            end
            
            % Propagate -- sample
            for n=1:numNodes(me)
                sampleNode(me.nodes{n});
            end
            
            % Weight
            weights = zeros([nsamples 1]);
            for s=1:nsamples
                weights(s) = posterior(me, s);
            end
            
            % Resample
            for n=1:numNodes(me)
                resample(me.nodes{n}, weights);
            end
        end
    end                        
    
end

