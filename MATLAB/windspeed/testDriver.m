function res = testDriver()
    dbstop if error
    res{1} = test_small_model();
    test_my_model();
    test_CPD_dimensions();
    
end

function test_CPD_dimensions()
    a = DiscreteNode('a',2);
    b = DiscreteNode('b',3);
    assert(2==numel(a.CPD.CPT));
    assert(3==numel(b.CPD.CPT));
    
    addChild(a,b);
    assert(2==numel(a.CPD.CPT));
    assert(2*3==numel(b.CPD.CPT));
    
    a = DiscreteNode('a',2);
    b = DiscreteNode('b',3);
    c = LCGNode('c',5);
    d = LCGNode('d',7);
    e = DiscreteNode('e',11);
    addChild(a,c); addChild(b,c); addChild(b,d); addChild(d,c); addChild(a,e); addChild(b,e);
    
    assert(2==numel(a.CPD.CPT));
    assert(3==numel(b.CPD.CPT));    
    assert(11*2*3==numel(e.CPD.CPT));
    
    assert(5*7*2*3==numel(c.CPD.a));
    assert(5*2*3==numel(c.CPD.b));
    assert(5*5*2*3==numel(c.CPD.C));
    
    assert(isempty(d.CPD.a));
    assert(7*3==numel(d.CPD.b));
    assert(7*7*3==numel(d.CPD.C));
    
end

function res = test_small_model()

    x=LCGNode('x',2);
    y=DiscreteNode('y',3);
    z=LCGNode('z',4);
    addChild(x,z);
    addChild(y,z);
    tiePrevious(z,x,1);

    dbn = DBN({{x},{y z}});
    % setNumSamples(dbn, nsamples);
    sampleNode(x);
    enterEvidence(y,3);
    sampleNode(z);
    res = z.samples;
    viewDBN(dbn);
    
end

function test_my_model()
    x1p=LCGNode('x1p',2);
    x2p=LCGNode('x2p',2);
    x3p=LCGNode('x3p',2);
    x4p=LCGNode('x4p',2);
    dp=DiscreteNode('dp',2);

    x1=LCGNode('x1',2);
    x2=LCGNode('x2',2);
    x3=LCGNode('x3',2);
    x4=LCGNode('x4',2);
    d=DiscreteNode('d',2);

    addChild(dp,x1p); addChild(dp,x2p); addChild(dp,x3p); addChild(dp,x4p);
    addChild(d,x1); addChild(d,x2); addChild(d,x3); addChild(d,x4);
    addParent(x1,x1p); addParent(x1,x2p);
    addParent(x2,x1p); addParent(x2,x2p); addParent(x2,x3p);
    addParent(x3,x2p); addParent(x3,x3p); addParent(x3,x4p);
    addParent(x4,x3p); addParent(x4,x4p);
    addChild(dp,d);
    tiePrevious(d,dp,1);
    tiePrevious(x1,x1p,1);
    tiePrevious(x2,x2p,1);
    tiePrevious(x3,x3p,1);
    tiePrevious(x4,x4p,1);

    dbn2 = DBN({{dp x1p x2p x3p x4p},{d x1 x2 x3 x4}});
    viewDBN(dbn2);

    %res{2} = sampleDBN(dbn2,25,5);
end