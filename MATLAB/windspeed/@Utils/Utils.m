classdef Utils
    %UTILS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Constant)
        PROBABILITY_ONE = 0;
    end
    
    methods (Static)
        % take union of vectors using eq etc methods, which works with
        % vectors of handle classes
        function L = union(a,b)
            a = sort(a);
            b = sort(b);
            L = [];
            while ~isempty(a) || ~isempty(b)
                if ~isempty(a) && ~isempty(b)
                    if eq(a(1),b(1))
                        if isempty(L) || ne(L(end),a(1))
                            L = [L a(1)];
                        end
                        a = a(2:end);
                        b = b(2:end);
                    elseif lt(a(1),b(1))
                        if isempty(L) || ne(L(end),a(1))
                            L = [L a(1)];
                        end
                        a = a(2:end);
                    else % lt(b(1),a(1))
                        if isempty(L) || ne(L(end),b(1))
                            L = [L b(1)];
                        end
                        b = b(2:end);
                    end
                elseif isempty(a)
                    if isempty(L) || ne(L(end),b(1))
                        L = [L b(1)];
                    end
                    b = b(2:end);
                else % isempty(b)
                    if isempty(L) || ne(L(end),a(1))
                        L = [L a(1)];
                    end
                    a = a(2:end);
                end
            end
        end
        
        % normalize a multinomial distribution
        function Pn = multnorm(P)
            P=exp(P-max(P(:)));
            
            % find normalizing constants
            norm=sum(P);

            % if NC is zero set to a uniform distribution
            if any(0==norm)
                zero_idx=find(0==norm);
                norm(zero_idx)=1;
                new_dist=zeros(size(norm));
                new_dist(zero_idx)=1/size(P,1);
                P=bsxfun(@plus,P,new_dist);
            end

            % normalize
            P=bsxfun(@rdivide,P,norm);
            Pn=log(P);
        end

        % sample a multinomial distribution
        function samples = multrnd(P, nsamples)
            assert(isvector(P));
            samples = zeros([nsamples 1]);
            for i=1:nsamples
                samples(i) = find(cumsum(exp(P)) >= rand,1,'first');
            end
        end
        
        % multiply probabilities
        function z = mulpr(x,y)
            z = x + y;
        end
    end
    
end

