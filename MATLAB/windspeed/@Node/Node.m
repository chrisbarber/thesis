classdef Node < handle
    %NODE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetAccess = private)
        name
        parents = struct();
        children = struct();
        self % self(n) is this node n time steps back
    end
    
    properties
        nsamples = 50; % number of samples kept when sampling
    end
    
    properties (Abstract, SetAccess = private)
        size
        obs
        samples
        CPD
    end
    
    methods
        function me = Node(name)
            me.name = name;
        end
        
        function addChild(me, newChild)
            if isfield(me.children, class(newChild))
                me.children.(class(newChild)) = sort(Utils.union(me.children.(class(newChild)), newChild));
            else
                me.children.(class(newChild)) = newChild;
            end
            
            if isfield(newChild.parents, class(me))
                newChild.parents.(class(me)) = sort(Utils.union(newChild.parents.(class(me)), me));
            else
                newChild.parents.(class(me)) = me;
            end
            
            resizeCPD(me);
            resizeCPD(newChild);
        end
        
        function addParent(me, newParent)
            if isfield(me.parents, class(newParent))
                me.parents.(class(newParent)) = sort(Utils.union(me.parents.(class(newParent)), newParent));
            else
                me.parents.(class(newParent)) = newParent;
            end
            
            if isfield(newParent.children, class(me))
                newParent.children.(class(me)) = sort(Utils.union(newParent.children.(class(me)), me));
            else
                newParent.children.(class(me)) = me;
            end
            
            resizeCPD(me);
        end
        
        function tiePrevious(me, oldSelf, howOld)
            me.self{howOld} = oldSelf;
        end
        
        function stepForward(me, nsteps)
            assert(1==nsteps); % only support single timestep currently
            if ~isempty(me.self)
                me.self(1).obs = me.obs;
                me.self(1).samples = me.samples;
            end
            me.obs = [];
            me.samples = {};
        end
        
        function resample(me, weights)
            newSampleIndices = multrnd(weights, me.nsamples);
            newSamples = me.samples(newSampleIndices);
            me.samples = newSamples;
        end
    end
    
    methods (Access = protected)
        function hasParent = hasParentOfType(me, type)
            hasParent = isfield(me.parents, type);
        end
        
        function parentSizes = getParentSizes(me, type)
            if hasParentOfType(me, type)
                parents = me.parents.(type);
                parentSizes = zeros([length(parents) 1]);
                for i = 1:length(parents)
                    parentSizes(i) = parents(i).size;
                end
            else
                parentSizes = 1;
            end
        end
        
        function values = getParentSettings(me, nodetype)
            values = cell([me.nsamples 1]);
            if hasParentOfType(me, nodetype)
                parents = me.parents.(nodetype);
                for s=1:me.nsamples
                    for i=1:length(parents)
                        if ~isempty(parents(i).obs)
                            values{s} = [values{s}; parents(i).obs];
                        elseif ~isempty(parents(i).samples)
                            values{s} = [values{s}; parents(i).samples{s}];
                        else
                            error('Parent nodes must be observed or have samples.');
                        end
                    end
                end
            end
        end
    end
    
    methods (Abstract)
        enterEvidence(me, evidence);
        sampleNode(me);
        p = posterior(me, sampleIndex);
    end
    
    methods (Abstract, Access = protected)
        resizeCPD(me)
    end
    
end