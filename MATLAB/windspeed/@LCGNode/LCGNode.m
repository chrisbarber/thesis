classdef LCGNode < Node
    %LCGNODE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetAccess = private)
        size % length of this vector-valued node
        obs % current setting to this node column vector (can be empty)
        samples % samples for node (only if unobserved), cell array
        CPD % struct specifying multivariate conditional linear gaussian
    end
    
    methods
        function me = LCGNode(name, size)
            me = me@Node(name);
            me.size = size;
        end
        
        function enterEvidence(me, evidence)
            assert(isvector(evidence) && length(evidence)==me.size);
            me.obs=evidence(:);
        end
        
        function sampleNode(me)
            me.samples = cell([me.nsamples 1]);
            for s=1:me.nsamples
                [mu, sigma] = condition(me, s);
                me.samples{s} = mvnrnd(mu, sigma)';
            end
        end
        
        function p = posterior(me, sampleIndex)
            if isempty(me.obs)
                p = Utils.PROBABILITY_ONE;
            else
                [mu, sigma] = condition(me, sampleIndex);
                p = mvnpdf(me.obs, mu, sigma);
            end
        end
    end

    methods (Access = protected)
        function resizeCPD(me)
            % TODO: this should probably always randomize the parameters
            %   (how to create random covariance matrix?)
            LCGDim = sum(getParentSizes(me, 'LCGNode'));
            DiscreteDims = getParentSizes(me, 'DiscreteNode')';

            if hasParentOfType(me, 'LCGNode')
                % TODO: choose a way to randomize weights
                % This just sets to zero:
                % me.CPD.a = zeros([me.size LCGDim DiscreteDims]);
                % This draws weights independently from a standard normal
                me.CPD.a = normrnd(0,1,[me.size LCGDim DiscreteDims]);
            else
                me.CPD.a = [];
            end
            % TODO: choose a way to randomize offsets & covariances, for
            % now just leaving at zeros.
            me.CPD.b = zeros([me.size DiscreteDims]);
            me.CPD.C = zeros([me.size me.size DiscreteDims]);
        end
        
        function [mu, sigma] = condition(me, sampleIndex)
            dps = getParentSettings(me, 'DiscreteNode');
            cps = getParentSettings(me, 'LCGNode');
            if ~isempty(dps{sampleIndex})
                dpindex = mat2cell(dps{sampleIndex},ones([length(dps{sampleIndex}) 1]), 1);
            else
                dpindex = {};
            end

            if hasParentOfType(me, 'LCGNode')
                mu = me.CPD.a(:,:,dpindex{:}) * cps{sampleIndex} + me.CPD.b(:,dpindex{:});
            else
                mu = me.CPD.b(:,dpindex{:});
            end
            sigma = me.CPD.C(:,:,dpindex{:});
        end
    end

end

