function [xcorrelogram, lags] = myxcorr(x,y,minlag,maxlag)

assert(isvector(x) && isvector(y));
assert(length(x)==length(y));

N=length(x);
mx=nanmean(x);
my=nanmean(y);

lags=minlag:maxlag;
xcorrelogram=zeros([1 maxlag-minlag]);

for d=lags
    cov=0;
    varx=0;
    vary=0;
    for i=1:N
        if i-d>=1 && i-d<=N && ~isnan(x(i)) && ~isnan(y(i-d))
            cov=cov+(x(i)-mx)*(y(i-d)-my);
            varx=varx+(x(i)-mx)^2;
            vary=vary+(y(i-d)-my)^2;
        end
    end

    xcorr=cov/(sqrt(varx)*sqrt(vary));
    xcorrelogram(d-minlag+1)=xcorr;
end

end