make_data;

minlag=-10;
maxlag=10;

for s=1:12
    % windowSize=3;
    % series1=diff(filter(ones(1,windowSize)/windowSize,1,D{1}.S));
    % series2=diff(filter(ones(1,windowSize)/windowSize,1,D{s}.S));
    
    series1=D{1}.S;
    series2=D{s}.S;
    
    [xcorr,lags]=myxcorr(series1,series2,minlag,maxlag);
    subplot(3,4,s);
    plot(lags,xcorr);
    axis([minlag maxlag 0 1])
    xlabel('Time lag (hours)');
    ylabel('Correlation');
    title(sprintf('%s (peaks at %d)',D{s}.name,lags(find(xcorr==max(xcorr)))));
end
    
    