function img = makeheatmap(H,varargin)

if 1==length(varargin)
    maxval=varargin{1};
else
    maxval=max(H(:));
end
    
img=uint8(repmat(floor(255*H/maxval),[1 1 3]));

end