temp = importdata('C:\to_matlab\station_0.csv');
D{1}.usaf_id = NaN;
D{1}.name = 'Lakeshore Technical College';
D{1}.latitude = 43.92317;
D{1}.longitude = -87.72995;
D{1}.N = size(temp.data, 1);
D{1}.V = temp.data(:,1:2);
D{1}.T = temp.data(:,3);
P=diff(temp.data(:,4));
badvalues=find(or(P<0,P>1000));
P(badvalues)=0;
D{1}.P = [0; P];
clear P badvalues
D{1}.S = sqrt(temp.data(:,1).^2+temp.data(:,2).^2);

% this fixes the fact that i didnt realize Lakeshore was using central time
D{1}.V = [nan([6 2]); D{1}.V(1:end-6,:)];
D{1}.T = [nan([6 1]); D{1}.T(1:end-6)];
D{1}.P = [nan([6 1]); D{1}.P(1:end-6)];
D{1}.S = [nan([6 1]); D{1}.S(1:end-6)];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

temp = importdata('C:\to_matlab\station_726410.csv');
D{2}.usaf_id = 726410;
D{2}.name = 'MADISON/DANE COUNTY';
D{2}.latitude = 43.141;
D{2}.longitude = -89.345;
D{2}.N = size(temp.data, 1);
D{2}.V = temp.data(:,1:2);
D{2}.T = temp.data(:,3);
D{2}.P = nan([D{2}.N 1]);
D{2}.S = sqrt(temp.data(:,1).^2+temp.data(:,2).^2);

temp = importdata('C:\to_matlab\station_726413.csv');
D{3}.usaf_id = 726413;
D{3}.name = 'WEST BEND MUNI';
D{3}.latitude = 43.422;
D{3}.longitude = -88.118;
D{3}.N = size(temp.data, 1);
D{3}.V = temp.data(:,1:2);
D{3}.T = temp.data(:,3);
D{3}.P = nan([D{3}.N 1]);
D{3}.S = sqrt(temp.data(:,1).^2+temp.data(:,2).^2);

temp = importdata('C:\to_matlab\station_726425.csv');
D{4}.usaf_id = 726425;
D{4}.name = 'SHEBOYGAN';
D{4}.latitude = 43.769;
D{4}.longitude = -87.851;
D{4}.N = size(temp.data, 1);
D{4}.V = temp.data(:,1:2);
D{4}.T = temp.data(:,3);
D{4}.P = nan([D{4}.N 1]);
D{4}.S = sqrt(temp.data(:,1).^2+temp.data(:,2).^2);

temp = importdata('C:\to_matlab\station_726450.csv');
D{5}.usaf_id = 726450;
D{5}.name = 'GREEN BAY/A.-STRAUB';
D{5}.latitude = 44.513;
D{5}.longitude = -88.120;
D{5}.N = size(temp.data, 1);
D{5}.V = temp.data(:,1:2);
D{5}.T = temp.data(:,3);
D{5}.P = nan([D{5}.N 1]);
D{5}.S = sqrt(temp.data(:,1).^2+temp.data(:,2).^2);

temp = importdata('C:\to_matlab\station_726455.csv');
D{6}.usaf_id = 726455;
D{6}.name = 'MANITOWAC MUNI AWOS';
D{6}.latitude = 44.129;
D{6}.longitude = -87.668;
D{6}.N = size(temp.data, 1);
D{6}.V = temp.data(:,1:2);
D{6}.T = temp.data(:,3);
D{6}.P = nan([D{6}.N 1]);
D{6}.S = sqrt(temp.data(:,1).^2+temp.data(:,2).^2);

temp = importdata('C:\to_matlab\station_726456.csv');
D{7}.usaf_id = 726456;
D{7}.name = 'WITTMAN RGNL';
D{7}.latitude = 43.984;
D{7}.longitude = -88.557;
D{7}.N = size(temp.data, 1);
D{7}.V = temp.data(:,1:2);
D{7}.T = temp.data(:,3);
D{7}.P = nan([D{7}.N 1]);
D{7}.S = sqrt(temp.data(:,1).^2+temp.data(:,2).^2);

temp = importdata('C:\to_matlab\station_726457.csv');
D{8}.usaf_id = 726457;
D{8}.name = 'APPLETON/OUTAGAMIE';
D{8}.latitude = 44.257;
D{8}.longitude = -88.517;
D{8}.N = size(temp.data, 1);
D{8}.V = temp.data(:,1:2);
D{8}.T = temp.data(:,3);
D{8}.P = nan([D{8}.N 1]);
D{8}.S = sqrt(temp.data(:,1).^2+temp.data(:,2).^2);

temp = importdata('C:\to_matlab\station_726506.csv');
D{9}.usaf_id = 726506;
D{9}.name = 'FOND DU LAC CO.';
D{9}.latitude = 43.770;
D{9}.longitude = -88.486;
D{9}.N = size(temp.data, 1);
D{9}.V = temp.data(:,1:2);
D{9}.T = temp.data(:,3);
D{9}.P = nan([D{9}.N 1]);
D{9}.S = sqrt(temp.data(:,1).^2+temp.data(:,2).^2);

temp = importdata('C:\to_matlab\station_726509.csv');
D{10}.usaf_id = 726509;
D{10}.name = 'JUNEAU\DODGE';
D{10}.latitude = 43.426;
D{10}.longitude = -88.700;
D{10}.N = size(temp.data, 1);
D{10}.V = temp.data(:,1:2);
D{10}.T = temp.data(:,3);
D{10}.P = nan([D{10}.N 1]);
D{10}.S = sqrt(temp.data(:,1).^2+temp.data(:,2).^2);

temp = importdata('C:\to_matlab\station_994330.csv');
D{11}.usaf_id = 994330;
D{11}.name = 'SHEBOYGAN';
D{11}.latitude = 43.750;
D{11}.longitude = -87.683;
D{11}.N = size(temp.data, 1);
D{11}.V = temp.data(:,1:2);
D{11}.T = temp.data(:,3);
D{11}.P = nan([D{11}.N 1]);
D{11}.S = sqrt(temp.data(:,1).^2+temp.data(:,2).^2);

temp = importdata('C:\to_matlab\station_997736.csv');
D{12}.usaf_id = 997736;
D{12}.name = 'PORT WASHINGTON';
D{12}.latitude = 43.383;
D{12}.longitude = -87.867;
D{12}.N = size(temp.data, 1);
D{12}.V = temp.data(:,1:2);
D{12}.T = temp.data(:,3);
D{12}.P = nan([D{12}.N 1]);
D{12}.S = sqrt(temp.data(:,1).^2+temp.data(:,2).^2);

clear temp
