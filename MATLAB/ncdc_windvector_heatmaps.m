make_data;

maxspeed=15;
nbuckets=100;
maxcolor=D{1}.N/nbuckets/10;

for s=1:12
    
    ew=D{s}.V(:,1);
    ns=D{s}.V(:,2);
    ew(abs(ew)>maxspeed)=NaN;
    ns(abs(ns)>maxspeed)=NaN;
    if 0==1 && 1==s
        for t=1:length(ew)
            if D{s}.S(t)<.3
                ew(t)=NaN;
                ns(t)=NaN;
            end
        end
    end
    
    B=-maxspeed:(2*maxspeed/nbuckets):maxspeed;
    [n,x,y]=hist2d(ew,ns,B);
    subplot(3,4,s);
    imagesc(x(1,:),y(:,1),n);
    caxis([0 maxcolor]);
    axis xy;
    xlabel('East-west windspeed (m/s)');
    ylabel('North-sorth windspeed (m/s)');
    title(D{s}.name);
end
    