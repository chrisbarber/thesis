%% LyX 1.4.4 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english]{article}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
\usepackage{graphicx}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LyX specific LaTeX commands.
%% Bold symbol macro for standard LaTeX users
\providecommand{\boldsymbol}[1]{\mbox{\boldmath $#1$}}


\usepackage{babel}
\makeatother
\begin{document}

\title{Spring 2007 Independent Study: Structure Learning for Probabilistic
Sequence Models}


\author{Chris Barber}

\maketitle
\newpage{}


\section{\label{sec:Overview}Overview}

This paper describes the various methods developed and experiments
performed during my independent study with Professor Bockhorst in
the Spring semester of 2007\@. The goal has been to extend the methods
established in \cite[Bockhorst et al. 2007 UAI paper]{Joe UAI} to
the task of learning an identical model structure or \emph{segmentation},
except that hidden variables may not be independent\@. Specifically,
according to its \emph{Bayesian network} representation, a segmentation
might include directed edges such as $(H_{3},H_{4})$ and $(H_{4},H_{5})$
found in Figure \ref{fig:Connected-Model}\@.%
\begin{figure}
\begin{centering}
\includegraphics[scale=0.5]{connected_model_example}
\par\end{centering}


\caption{\label{fig:Connected-Model}Example of a Connected Model Structure}
\end{figure}
 A model having a structure such as this will henceforth be referred
to as a {}``connected model,'' whereas a model of the variety studied
in \cite{Joe UAI} will be referred to as a {}``disconnected model.''


\section{\label{sec:Methods}Methods}

An algorithm is developed to learn a connected model structure for
a set of sequences\@. A recurrence similar to that in \cite{Joe UAI}
is formulated, the main difference being that \emph{subproblem optimality}
is merely an assumption\@. Additionally, scoring connected models
is much more time consuming, and the connected structure prevents
us from exploiting \emph{memoization} for the most part\@. To address
these efficiency concerns a heuristic is incorporated into the algorithm,
which is described in Section \ref{sub:Heuristic}\@.


\subsection{\label{sub:Notation}Notation}

The segmentation denoted by\[
S=((s_{1},l_{1},c_{1},d_{1}),\ldots,(s_{a},l_{a},c_{a},d_{a}),\ldots,(s_{M},l_{M},c_{M},d_{M}))\]
contains $M$ segments, each a four-tuple $S_{a}=(s_{a},l_{a},c_{a},d_{a})$
where 

\begin{itemize}
\item $s_{a}$ is the start position of segment $S_{a}$,
\item $l_{a}$ is its length,
\item $c_{a}\geq1$ is the cardinality of $H_{a}$, the hidden variable
associated with $S_{a}$,
\item and for $1<a\leq M$, $d_{a}\in\{ T,F\}$ is $T$ iff the Bayes net
representation contains the directed edge $(H_{a-1},H_{a})$\@.
\end{itemize}
We require that a segmentation $S$ be complete and non-overlapping
i.e. 

\begin{itemize}
\item $s_{1}=1$, 
\item $s_{M}+l_{M}-1=L$, where $L$ is the length of the sequences being
considered,
\item and $s_{a}+l_{a}=s_{a+1}$ for all $1\leq a<L$\@.
\end{itemize}

\subsection{\label{sub:Scoring}Scoring}

To score a possible segmentation we use the CV method described in
\cite{Joe UAI}\@. The main difference here is a significant increase
in the computational cost of scoring\@. Let $\texttt{score}(S)$
denote the function for CV scoring in the following sections, where
$S$ is any segmentation, and the score is computed over the training
data.

In addition, a minor modification to $\texttt{score}$ gives a heuristic
scoring technique, $\texttt{hscore}$, which is introduced in Section
\ref{sub:Heuristic}\@.


\subsection{\label{sub:Algorithm}Algorithm}

As in \cite{Joe UAI} we design a dynamic program which computes segmentations
over positions 1 through $j$, for $j=1\ldots L$\@. Let $S^{j}$
denote the segmentation obtained when considering positions 1 through
$j$\@. Likewise $S_{a}^{j}$ denotes the $a$th segment in $S^{j}$,
and $M^{j}$ is the index of the last segment in $S^{j}$\@. As in
\cite{Joe UAI} we must choose a maximum cardinality $C$\@.

The dynamic program is based on the recurrence\begin{equation}
S^{j}=\arg\max_{S}\{\texttt{score}(S)\mid S\in\mathcal{S^{\mathnormal{j}}\}}\label{eq:DP}\end{equation}
where $\mathcal{S^{\mathnormal{j}}}$ is the set of segmentations\begin{equation}
\{(S_{1}^{j-l},\ldots,S_{M^{j-l}}^{j-l},(j-l+1,l,c,d))\mid1\leq l\leq j\textnormal{, }d\in\{ T,F\}\textnormal{, and }c\leq C\}\label{eq:segmentations}\end{equation}
Using this dynamic program we can compute $S^{L}$, which is the segmentation
over the whole data set\@.


\subsection{\label{sub:Heuristic}Heuristics}

The first heuristic is based on the intuition that we may obtain a
reasonable approximation for $\texttt{score}(S)$ by omitting an edge
whose presence or absence in the segmentation has already been determined\@.

The heuristic modifies (\ref{eq:DP}) as follows:

\begin{equation}
S^{j}=\arg\max_{S}\{\texttt{score}(f(S))\mid S\in\mathcal{S^{\mathnormal{j}}\}}\label{eq:heuristic}\end{equation}
where for any segmentation $S=(S_{1},\ldots,S_{M})$,\begin{equation}
f(S)=(S_{1},\ldots,S_{M-2},(s_{M-1},l_{M-1},c_{M-1},F),S_{M})\label{eq:heur_f}\end{equation}
The absence of directed edge $(H_{M-2},H_{M-1})$ allows $\texttt{score}(f(S))$
to be computed as\begin{equation}
\texttt{score}(f(S))=\texttt{score}(S_{1},\ldots,S_{M-2})+\texttt{score}((s_{M-1},l_{M-1},c_{M-1},F),S_{M})\label{eq:heur_score}\end{equation}
The value of the first term, $\texttt{score}(S_{1},\ldots,S_{M-2})$,
can be saved and reused so that, for each segmentation $S$ considered
in (\ref{eq:heuristic}), $\texttt{score}$ only needs to be evaluated
for the last two segments in $S$\@.

A second heuristic is a scoring method, which we will call $\texttt{hscore}$,
that modifies the parameter-learning portion of the $\texttt{score}$
method\@. In $\texttt{hscore}$, the \emph{MAP} estimates are computed
for each segment considered independently\@. Emission parameters
are then fixed, while \emph{MAP} estimates are computed for the hidden
variables in the full segmentation\@. This is the technique described
in \cite{greenspan}\@. The $\texttt{hscore}$ method is used only
in certain instances, namely when scoring the first term in (\ref{eq:heur_score}),
so that (\ref{eq:heur_score}) becomes\[
\texttt{score}(f(S))=\texttt{hscore}(S_{1},\ldots,S_{M-2})+\texttt{score}((s_{M-1},l_{M-1},c_{M-1},F),S_{M})\]
 


\section{\label{sec:Results}Results}

The algorithm described in Section \ref{sub:Algorithm}, using the
heuristic formulation of Section \ref{sub:Heuristic}, was evaluated
on SNP data from \cite{Patil} (the same SNP data set used in \cite{Joe UAI})\@.
To evaluate our algorithm, the 10-fold cross-validation experiment
of \cite{Joe UAI} on this data set was replicated\@. Models were
learned for each fold using the following parameters:

\begin{itemize}
\item EM convergence ratio = 1.5
\item Number of CV folds = 5
\item Maximum cardinality $C=5$
\item Number of random-restarts = 1
\item HV pseudocounts = 0
\item Emission pseudocounts = 0.01
\item Maximum segment length = 50
\end{itemize}
Many of these parameters are far from ideal simply because running-time
was problematic\@.

Figure \ref{fig:relLL}%
\begin{figure}

\caption{\label{fig:relLL}Relative Log-Likelihood of Connected vs. Disconnected
Models}

\begin{centering}
\includegraphics[scale=0.75]{C:/MATLAB7/work/cs699/results/no_shortcut}
\par\end{centering}
\end{figure}
 shows log-likelihoods of connected models given each sequence, relative
to \emph{}log-likelihoods of disconnected models obtained in \emph{}\cite{Joe UAI}\@.


\section{\label{sec:Conclusion}Conclusion}

This paper demonstrates a significant step forward in understanding
the problem of learning a {}``connected model'' over a sequential
data set such as the SNPs studied here\@. Much work remains however,
including

\begin{itemize}
\item a more rigorous evaluation of the methods presented here
\item possible improvement upon or elimination of heuristic elements of
the algorithm
\item possible justification for subproblem optimality\newpage{}
\end{itemize}
\begin{thebibliography}{1}
\bibitem{Joe UAI}Bockhorst, J. \& Jojic, N. (2007). Discovering Patterns
in Biological Sequences by Optimal Segmentation. (Submitted).

\bibitem{greenspan}Greenspan, G. \& Geiger, D. (2004). Model-based
inference of haplotype block variation. \emph{Journal of Computational
Biology}, (pp. 493-504).

\bibitem{Patil}Patil, N., Berno, A., Hinds, D., Barrett, W., Doshi,
J., Hacker, C., Kautzer, C., Lee, D., Marjoribanks, C., McDonough,
D., Nguyen, B., Norris, M., Sheehan, J., Shen, N., Stern, D., Stokowski,
R., Thomas, D., Trulson, M., Vyas, K., Frazer, K., Fodor, S., \& Cox,
D. (2001). Blocks of limited haplotype diversity revealed by high
resolution scanning of human chromosome 21. \emph{science}, 294(5547):1719-1723.
\end{thebibliography}

\end{document}
