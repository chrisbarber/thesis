function chris_FOMM = joe2chris(joe_FOMM)

chris_FOMM=joe_FOMM;
for i=1:length(chris_FOMM)
    % column vector instead of row vector for component_probs
    chris_FOMM(i).component_probs=transpose(chris_FOMM(i).component_probs);
    chris_FOMM(i).component_probs=zlog(chris_FOMM(i).component_probs);
    % E(pos,sym,comp) instead of E(comp,pos,sym)
    chris_FOMM(i).E=permute(chris_FOMM(i).E,[2 3 1]);
    chris_FOMM(i).E=zlog(chris_FOMM(i).E);
end