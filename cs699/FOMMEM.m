function [new_FOMM, Stats] = FOMMEM(D, FOMM, c, varargin)

%         FOMMEM - Expectation Maximization for a 
%                Family Of Mixture Models
%         -~===================================~-
%
% Inputs
%
%   D is an array of integer sequences of the same length e.g.
%       D=[1 2 3 2 3 4; 3 2 2 4 3 1; 1 1 1 1 0 0]
%     Make sure that each sequence extends along a row (dimension 2),
%   thus the columns represent an aligned position across all sequences.
%     The alphabet is assumed to be every integer from 1 to the maximum.
%   Zeros represent missing data.
%
%   FOMM is the model for which the parameters will be learned.  At this
%     point the parameters must contain the desired pseudocounts for the
%     corresponding parameters.  These will be saved and used in each
%     iteration as the initial counts.
%
%   c is a convergence ratio, i.e. the algorithm will halt once the
%     ratio of the current likelihood over the previous iteration is less
%     than this parameter.
% Optional Inputs
%
%   varargin is passed along to MMEM.  See MMEM.m function header for
%     details.
%
% Outputs
%
%   new_FOMM is the trained FOMM.
%
%   Stats is a structure containing some statistics gathered during EM
%

num_seg=length(FOMM);

Stats=cell([1 num_seg]);
new_FOMM=FOMM;
for i=1:num_seg
    [new_mm indStats]=MMEM(D(:,FOMM(i).positions),FOMM(i),c,varargin{:});
    new_FOMM(i).component_probs=new_mm.component_probs;
    new_FOMM(i).E=new_mm.E;
    Stats{i}=indStats;
end
    