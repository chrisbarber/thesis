function [new_COMM, pos, inseg] = splitCOMM(COMM, pos)

% Split COMM at random pos.
% -~=====================~-

num_seg=length(COMM);
seq_len=COMM(end).endpos;

% choose random position for split if unspecified
if 1==nargin
    pos=ceil(rand*(seq_len-1)); % endpos of first resulting mm
end

% find segment containing split position
inseg=1;
while COMM(inseg).endpos<pos
    inseg=inseg+1;
end

% setup split

mm1=COMM(inseg);
mm1.endpos=pos;
mm1.L=mm1.endpos-mm1.startpos+1;
mm1.positions=mm1.startpos:mm1.endpos;

mm2=COMM(inseg);
mm2.startpos=pos+1;
mm2.L=mm2.endpos-mm2.startpos+1;
mm2.positions=mm2.startpos:mm2.endpos;

% choose new cardinalities

mm1.num_components=ceil(sqrt(COMM(inseg).num_components));
mm2.num_components=ceil(sqrt(COMM(inseg).num_components));

% update component prob. dimensions

if 1==inseg
    mm1.component_probs=zeros([mm1.num_components 1]);
else
    mm1.component_probs=zeros([mm1.num_components COMM(inseg-1).num_components]);
end
mm2.component_probs=zeros([mm2.num_components mm1.num_components]);
if inseg~=num_seg
    if 1~=size(COMM(inseg+1).component_probs,2)
        COMM(inseg+1).component_probs=zeros([COMM(inseg+1).num_components mm2.num_components]);
    end
end

% update emission param. dimensions

mm1.E=zeros([mm1.L size(mm1.E,2) mm1.num_components]);
mm2.E=zeros([mm2.L size(mm2.E,2) mm2.num_components]);

% merge mm1 and mm2 into COMM

COMM(inseg)=mm1;
if inseg~=num_seg
    COMM=[COMM(1:inseg) mm2 COMM(inseg+1:end)];
else
    COMM=[COMM(1:inseg) mm2];
end

new_COMM=COMM;