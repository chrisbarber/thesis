function [LL testSet_loglike new_COMMs Stats] = COMMEMCV(D, COMM, c, folds, varargin)

%
%         COMMEMCV - Cross-Validated EM for a 
%               Chain Of Mixture Models
%         -~===============================~-
%
% Inputs
%
%   D, COMM, c, & varargin are simply passed on to COMMEM().  See COMMEM.m
%     function header descriptions of these parameters.
%
%   folds(j) specifies the fold in which sequence j should belong
%     to the test-set.  'folds' must be specified if 'k' is not.
%
% Outputs
%
%   testSet_loglike(j) is the log-likelihood of the sequence j, given the
%     model generated when sequence j was in the test-set
%
%   new_COMMs{i} is the COMM generated during the ith fold.
%
%   Stats{i} is the 'Stats' structure returned by COMMEM() during the ith
%     fold.  See COMMEM.m function header for a description.
%

[num_seq seq_len]=size(D);

[useFixedE fixedE_LPind otherargs]=getarg('fixedE_LPind',varargin);

% reproduce folds from 'folds' parameter
k=max(folds(:));
for fold=1:k
    testSetIndices{fold}=find(folds==fold);
    trainingSetIndices{fold}=setdiff(1:num_seq,testSetIndices{fold});
end

if k<=1 || k>num_seq
    error('k must be between 1 and the number of sequences');
end

testSet_loglike=zeros([1 num_seq]);
new_COMMs=cell([1 k]);
Stats=cell([1 k]);
for fold=1:k
    % train the model on the training-set
    if iscell(COMM)
        temp_COMM=COMM{fold};
    else
        temp_COMM=COMM;
    end
    if useFixedE
        tempargs={otherargs, 'fixedE_LPind', fixedE_LPind{fold}};
    else
        tempargs=varargin;
    end
    [new_COMMs{fold} Stats{fold}]=COMMEM(D(trainingSetIndices{fold},:),temp_COMM,c,tempargs{:});
    % evaluate log-likelihood of the test-set
    LPind=LPCOMMind(D(testSetIndices{fold},:),new_COMMs{fold});
    [junk testSet_loglike(testSetIndices{fold}) junk2]=LPCOMM(D(testSetIndices{fold},:),LPind,new_COMMs{fold},varargin{:});
end

LL=sum(testSet_loglike);