function data=repeatMMEM(D, mm, c, n)
tic
% repeat em(D, k, c, r_A, r_P) n times and plot a histogram of the
% log-likelihoods of resulting models

% also returns the model with greatest likelihood

bestLL=-realmax;
percent=0;
LL=zeros([1 n]);
iter=zeros([1 n]);
for i=1:n
    [M, S]=MMEM(D, mm, c);
    LL(i)=S.LL;
    iter(i)=S.iter;
    
    if S.LL>bestLL
        bestLL=S.LL;
        bestModel=M;
        bestModel.stat=S;
    end
    
    % print status
    if percent < round(100*i/n)
        clc
        disp(sprintf('%d%%',round(100*i/n)));
    end
    percent=round(100*i/n);
end

h1=figure('Name','log-likelihood');
hist(LL, 50);
h2=figure('Name','iterations');
hist(iter, 50);

data=struct('bestModel', bestModel, 'LL', LL, 'iter', iter);
toc