function [new_mm, Stats] = MMEM(D, mm, c, varargin)

%
%         MMEM - Mixture Model Expectation Maximization
%         -~=========================================~-
%
% Inputs
%
%   D is an array of integer sequences of the same length e.g.
%       D=[1 2 3 2 3 4; 3 2 2 4 3 1; 1 1 1 1 0 0]
%     Make sure that each sequence extends along a row (dimension 2),
%   thus the columns represent an aligned position across all sequences.
%     Zeros represent missing data.  All values >= 1 represent observed
%   data.  Note that the alphabet is assumed to be every integer from
%   the minimum value in D to the maximum.  If values in this interval
%   are not present in D, they are treated as unobserved symbols.
%
%   mm is the the mixture model for which parameters should be learned.
%     Initially, mm should contain the desired pseudocounts in the
%     positions for their corresponding parameters.
%
%   c is a convergence ratio, i.e. the algorithm will halt once the
%     ratio of the current likelihood over the previous iteration is less
%     than this parameter.
%
% Optional Inputs
%
%   'numRestarts' is the number if restarts to perform.  If ommitted this
%     defaults to 1.
%
% Outputs
%
%   M is the resulting model parameters.  It is a structure including A,
%     the component probabilities, and P(a,b,c), the distribution
%     Pr(X_a=S(b) | Z=c).
%
%   S is a structure containing some statistics gathered during execution
%

% trim down sequence data to what is needed if there is extra
D=D(:,mm.positions);

% find # of sequences & their length
if isnumeric(D) && (2==ndims(D))
    [num_seq, seq_len]=size(D);
else
    error('D must be a 2-dimensional integer array');
end

% find # of symbols
num_sym=max(size(mm.E,2));

% handle optional parameters
[useRestarts numRestarts]=getarg('numRestarts',varargin);
if ~useRestarts
    numRestarts=1;
end

% extract information from blank model
k=mm.num_components;

for restartIndex=1:numRestarts
    % set all parameters to pseudocount values
    pseudocounts=mm;
    
    % initialize model with random parameters

    %    [  the model has the format that entry P(a,b,c) equals
    %    Pr(X_a=S(b) | Z=c) where X_a is the ath position of a
    %    sequence, S(b) is the bth symbol in symbol set S, and c is
    %    the mixture component.  furthermore, A(i) is the weight or
    %    probability of component i (A is a row vector).  ]

    % generate random parameters
    P=rand([seq_len num_sym k]);
    % normalize
    P=normalize(P,2);
    P=zlog(P);

    % generate random weights
    A=rand([k 1]);
    % normalize
    A=normalize(A,1);
    A=zlog(A);

    % initializations for EM-loop
    cont=true;
    iter=0; % iteration counter

    while cont
        iter=iter+1;

        % E-STEP
        Y=zeros([seq_len num_seq k]); % Y(i,j,z)=log(1)
        for i=1:seq_len
            obs_seq=find(D(:,i)); % indices of seq. observed at pos i
            Y(i,obs_seq,:)=P(i,D(obs_seq,i),:); % Y(i,j,z)=Pr(seq_j(i)|Z=z)
        end
        Y=reshape(sum(Y,1),[num_seq k]); % Y(j,z)=log(Pr(seq_j|Z=z))
        Y=Y+repmat(A',[num_seq 1]); % Y(j,z)=log(A(z)*Pr(seq_j|Z=z))

        % normalize (2nd dim is mixture component)
        Y=lognorm(Y,2);

        % M-STEP

        % calculate expected component counts
        A=logadd(logsum(Y,1)',mm.component_probs); % add pseudocounts
        A=lognorm(A,1);

        % calculate expected parameter counts
        P=mm.E; % initialize to pseudocount values
        for z=1:k
            for j=1:num_seq
                for i=find(D(j,:))
                    P(i,D(j,i),z)=logadd(P(i,D(j,i),z),Y(j,z));
                end
            end
        end
        % normalize to get next round's parameters
        P=lognorm(P,2);

        % save previous log-likelihood of model params.
        if 1<iter
            prevLL=LL;
        end

        % calculate log-likelihood of new model parameters:

        current_mm=struct('component_probs', A, 'E', P, 'num_components', k, 'positions', 1:seq_len, 'startpos', 1, 'endpos', seq_len, 'L', seq_len);
        [LL, LLseq]=LPMM(D, current_mm); % by EM Pr(D|P) is used for Pr(P|D)
    
        % modify LLseq(j,z) to contain the log of probabilities
        % Pr(seq_j|Z=z,A,P) relative to null model (UAR).
        LLseq=LLseq+log(num_sym^seq_len);

        % convergence test

        if 1<iter && log(c)>LL-prevLL
            cont=false;
        elseif 1<iter && LL<prevLL
            warning('LL decreased! from %d to %d',prevLL,LL);
        end
    end

    if 1==restartIndex
        new_mm=current_mm;
        Stats=struct('LL', LL, 'LLseq', LLseq, 'iter', iter);
    else
        if LL>Stats.LL
            new_mm=current_mm;
            Stats=struct('LL', LL, 'LLseq', LLseq, 'iter', iter);
        end
    end
end

new_mm.startpos=mm.startpos;
new_mm.endpos=mm.endpos;
new_mm.positions=mm.positions;
new_mm.L=mm.L;