function LPind = LPCOMMind(D, COMM)

% LPind{j,i}(k) is the probability of the ith segment of sequence j
% given that the component for the ith segment is k.  Use LPind(j,:)
% to extract the LPind for sequence j.

[num_seq seq_len]=size(D);

num_seg=length(COMM);

LPind=cell([num_seq num_seg]);
for i=1:num_seg
    temp_mm=COMM(i);
    % component_probs can be junk of the correct dimension
    temp_mm.component_probs=zeros([temp_mm.num_components 1]);
    [junk tempLPind]=LPMM(D, temp_mm);
    LPind(:,i)=num2cell(tempLPind,2);
end
