function [new_COMM seg] = connectCOMM(COMM, idx)

% Connect COMM between 2 random segments
% -~==================================~-
%
%  idx=i specifies that an arc should be added from the h.v.
%    of the (i-1)th connected component of the model to the h.v.
%    of the ith connected component.
%

num_seg=length(COMM);
chains=findFOCOMMchains(COMM);
numchains=length(chains);

% choose a random pair of mm's to connect
if 1==nargin
    connect=ceil(rand*(numchains-1))+1; % index of descendant chain
else
    connect=idx;
end

% make connection

seg=chains{connect}(1);
COMM(seg).component_probs=repmat(COMM(seg).component_probs, [1 COMM(seg-1).num_components]);

new_COMM=COMM;
