function new_FoCOMM = initFoCOMM(FoCOMM, r_component, r_E)

% set parameters in a Family or Chain of Mixture Models (FoCOMM) to
% pseudocounts in preparation for EM

new_FoCOMM=FoCOMM;
for i=1:length(new_FoCOMM)
    new_FoCOMM(i).component_probs(:)=r_component;
    new_FoCOMM(i).E(:)=r_E;
end