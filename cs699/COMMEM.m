function [new_COMM, Stats] = COMMEM(D, COMM, c, varargin)

%
%         COMMEM - Expectation Maximization for a 
%                 Chain Of Mixture Models
%         -~===================================~-
%
% Inputs
%
%   D is an array of integer sequences of the same length e.g.
%       D=[1 2 3 2 3 4; 3 2 2 4 3 1; 1 1 1 1 0 0]
%     Make sure that each sequence extends along a row (dimension 2),
%   thus the columns represent an aligned position across all sequences.
%     The alphabet is assumed to be every integer from 1 to the maximum.
%   Zeros represent missing data.
%
%   COMM is the model for which the parameters will be learned.  At this
%     point the parameters must contain the desired pseudocounts for the
%     corresponding parameters.  These will be saved and used in each
%     iteration as the initial counts.  If 'fixedE_LPind' is used, the
%     E distributions must contain the desired probabilities, which will
%     be fixed throughout training of the component probabilities.
%
%   c is a convergence ratio, i.e. the algorithm will halt once the
%     ratio of the current likelihood over the previous iteration is less
%     than this parameter.
%
% Optional Inputs
%
%   'numRestarts' is the number if restarts to perform.  If ommitted this
%     defaults to 1.
%
%   'initCOMM' is the set of initial parameters for each restart.  If
%     numRestarts is 1, initCOMM has the format of a COMM populated with
%     the desired initial parameters.  For numRestarts>1, initCOMM is a
%     cell-array of such COMMs where initCOMM{r} are the initial
%     parameters to be used in the rth restart.  This is useful for testing
%     purposes.
%
%   'fixedE_LPind' is an optional parameter allowing for precomputed
%     emission distributions obtained by some other means.
%     fixedE_LPind{j,i}(k) contains log(Pr(seq_j(i)|Z_i=k)), or the
%     probability that segment i of sequence j is observed given the
%     mixture-component k.
%
% Outputs
%
%   new_COMM is the trained COMM.
%
%   Stats is a structure containing some statistics gathered during EM
%

% find # of sequences & their length
if isnumeric(D) && (2==ndims(D))
    [num_seq, seq_len]=size(D);
else
    error('D must be a 2-dimensional integer array');
end

% handle optional parameters

[useRestarts numRestarts]=getarg('numRestarts',varargin);
if ~useRestarts
    numRestarts=1;
end
[useInitCOMM initCOMM]=getarg('initCOMM',varargin);
[useFixedE fixedE_LPind]=getarg('fixedE_LPind',varargin);

% find # of symbols
num_sym=max(D(:));

% find # of segments
num_seg=length(COMM);

% save pseudocount values
Pseudocounts=COMM;

% perform numRestarts iterations of EM

for restartIndex=1:numRestarts
    if useInitCOMM
        if ~useFixedE
            if useRestarts
                COMM=initCOMM{restartIndex};
            else
                COMM=initCOMM;
            end
        else
            % don't assume initCOMM contains proper fixedE values
            if useRestarts
                for i=1:num_seg
                    COMM(i).component_probs=initCOMM{restartIndex}(i).component_probs;
                end
            else
                for i=1:num_seg
                    COMM(i).component_probs=initCOMM(i).component_probs;
                end
            end
        end
    else
        % initialize model with random parameters
        for i=1:num_seg
            mm=COMM(i);
            mm.component_probs=rand(size(mm.component_probs));
            mm.component_probs=normalize(mm.component_probs, 1);
            mm.component_probs=log(mm.component_probs);
            if ~useFixedE
                mm.E=rand(size(mm.E));
                mm.E=normalize(mm.E, 2);
                mm.E=log(mm.E);
            end
            COMM(i)=mm;
        end
    end
        
    % precompute LPind for new COMM
    if ~useFixedE
        LPind=LPCOMMind(D,COMM);
    else
        if 1==restartIndex
            LPind=fixedE_LPind;
        end
    end
    
    % other initializations
    cont=true;
    iter=0; % iteration counter

    % recurrence
    while cont
        iter=iter+1;

        % set all parameters to their pseudocount values (recall that the
        %   variable 'Pseudocounts' is a COMM with identical structure to the
        %   model we wish to learn.  Thus 'Counts' will be the appropriate
        %   structure to be populated with expected counts.
        Counts=Pseudocounts;
        
        % for each sequence, add expected counts
        for j=1:num_seq
            % calculate forward & backward values at all positions & states
            if 1==iter
                f=fwdCOMM(D(j,:), LPind(j,:), COMM);
            else
                f=savedFwdVals{j};
            end
            b=bwdCOMM(D(j,:), LPind(j,:), COMM);

            % calculate 1/Pr(seq_j) factor
            LPseq=logsum(f{num_seg});

            % calculate component_probs counts; 1st segment is a special case
            temp=COMM(1).component_probs+LPind{j,1}'+b{1}'-LPseq;
            Counts(1).component_probs=logadd(temp, Counts(1).component_probs);
            for i=2:num_seg
                k=COMM(i-1).num_components;
                l=COMM(i).num_components;
                temp=repmat(f{i-1},[l 1]);
                temp=temp+COMM(i).component_probs;
                temp=temp+repmat(LPind{j,i}',[1 k]);
                temp=temp+repmat(b{i}',[1 k]);
                temp=temp-LPseq;
                Counts(i).component_probs=logadd(temp, Counts(i).component_probs);
            end

            if ~useFixedE
                % calculate E counts
                for i=1:num_seg
                    posterior_state_prs=f{i}+b{i}-LPseq;
                    for a=1:num_sym
                        pos_a_obs=find(a==D(j,COMM(i).positions));
                        if 0<length(pos_a_obs)
                            % (only need to add counts where a is observered)
                            temp=permute(posterior_state_prs,[1 3 2]);
                            temp=repmat(temp,[length(pos_a_obs) 1 1]);
                            tempE=Counts(i).E(pos_a_obs,a,:);
                            tempE=logadd(tempE,temp);
                            Counts(i).E(pos_a_obs,a,:)=tempE;
                        end
                    end
                end
            end
        end

        % normalize counts to become new model parameters
        for i=1:num_seg
            COMM(i).component_probs=lognorm(Counts(i).component_probs, 1);
            if ~useFixedE
                COMM(i).E=lognorm(Counts(i).E, 2);
            end
            % note: in the case of fixedE, COMM(i).E's are never modified,
            %   so they contain the desired parameters throughout EM.
        end
        
        if ~useFixedE
            % precompute LPind for new COMM
            LPind=LPCOMMind(D,COMM);
        end

        % save previous log-likelihood of model params.
        if 1<iter
            prevLL=LL;
        end

        % calculate log-likelihood of new model parameters
        [LL LLseq savedFwdVals]=LPCOMM(D,LPind,COMM);

        % convergence test
        if 1<iter && LL<prevLL
%            warning('LL decreased! from %d to %d',prevLL,LL);
            cont=false;
        elseif 1<iter && log(c)>LL-prevLL
            cont=false;
        end
    end

    if 1==restartIndex
        Stats=struct('LL', LL, 'LLseq', LLseq, 'iter', iter);
        new_COMM=COMM;
    else
        if LL>Stats.LL
            Stats=struct('LL', LL, 'LLseq', LLseq, 'iter', iter);
            new_COMM=COMM;
        end
    end
end
