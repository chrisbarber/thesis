function [seg_COMM LL LLhistory] = searchCOMMseg(D, initSeg, config)

%
% config members:
%       .num_folds
%       .rand_folds
%       .iter
%       .r_A
%       .r_P
%       .EMconverge
%       .numRestarts
%

[num_seq seq_len]=size(D);

bestLL=-inf;
COMM=initSeg;
folds=CVfolds(num_seq,config.num_folds,config.rand_folds);
LLhistory=zeros([1 config.iter]);
for iter=1:config.iter
    tic
    op=logical(mod(iter,2));
    new_COMMs=cell(0);
    num_seg=length(COMM);
    numchains=length(findFOCOMMchains(COMM));
    if 0==op
        if num_seg<seq_len
            % perform a split in each segment
            for i=1:num_seg
                % split at midpoint of this segment
                if COMM(i).L>2
                    pos=COMM(i).positions(floor(COMM(i).L/2));
                    new_COMMs{end+1}=splitCOMM(COMM,pos);
                end
            end
        else
            warning('Cannot perform split operation');
            continue;
        end
    else % 1==op
        if numchains>1
            % try each connection independently
            for i=2:numchains
                new_COMMs{end+1}=connectCOMM(COMM,i);
            end
        else
            warning('Cannot perform connect operation.');
            continue;
        end
    end

    bestidx=0;
    for m=1:length(new_COMMs)
        new_COMMs{m}=initFoCOMM(new_COMMs{m},config.r_A,config.r_P);
        LL=FOCOMMEMCV(D,new_COMMs{m},config.EMconverge,folds,'numRestarts',config.numRestarts);

        if LL>bestLL
            bestidx=m;
            bestLL=LL;
        end
    end

    if bestidx>0
        COMM=new_COMMs{bestidx};
        LLhistory(iter)=bestLL;
    end
    
    display(sprintf('Iteration %d, duration: %d',iter, toc));
end

seg_COMM=COMM;