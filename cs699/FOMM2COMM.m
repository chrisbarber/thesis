function COMM = FOMM2COMM(FOMM)

% converts a FOMM to an equivalent COMM

COMM=FOMM;
for i=2:length(COMM)
    newCPD=COMM(i).component_probs;
    newCPD=repmat(newCPD, [1 COMM(i-1).num_components]);
    COMM(i).component_probs=newCPD;
end