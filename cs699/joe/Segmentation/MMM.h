#pragma once

#include <vector>
#include "Alignment.h"
#include "Utils.h"
#include "seg_common.h"


class MultinomialMixtureModel
{
public:

  //
  // ******** MEMBER VARIABLES ***********
  //
  int m_MAX_CARDINALITY, m_MAX_LENGTH, m_MAX_TRAINING_SEQS; // Max values set once in the constructor
  int m_alphabet_size; // number of symbols in the alphabet, also set only in the contsructor
  double m_1over_alphabet_size; // 1.0 / alphabet_size
  
  int m_length; // the length of the mixture model
  int m_cardinality;  // number of mixture components
  int m_startpos; // start position in the alignment of the first position of the mixture model
  int m_endpos; // start position in the alignment of the last position of the mixture model
  Alignment *m_pAlignment;

  /* Probability Distribution Over the Hidden Variable */
  std::vector<double> m_componentProbs;
  std::vector<double> m_componentProbs_cache;
  std::vector< std::vector<double> > m_countsByComponent;
  std::vector<double> m_componentCounts;

  /* Emission Distributions */
  std::vector< std::vector< std::vector<double> > > m_E;
  std::vector< std::vector< std::vector<double> > > m_E_cache;
  std::vector< std::vector< std::vector<double> > > m_E_counts;
  std::vector< std::vector<double> > m_initCounts;

  /* ?? */
  double m_sequenceCounts;

  /* Likelihood Related Variables */
  std::vector< std::vector<double> > m_likelihoodByComponent;
  std::vector<double> m_likelihoodBySeq;

  // if log likelihood of training set increases by less that CONVERGENCE_THRESHOLD stop EM iterations
  static double CONVERGENCE_THRESHOLD;

 private: 

  

  //
  // ******** MEMBER FUNCTIONS ***********
  //
 public:

  MultinomialMixtureModel(int max_cardinality, int alphabet_size, int max_length, int max_training_seqs);
  ~MultinomialMixtureModel(void);

  /* initialize a new model */
  void init(  Alignment *pAlignment, int mixmodLength, int numComponents, int startpos ); 

  double randomRestartTrain( Alignment *pAlignment, int length, int cardinality, int startpos, int maxIterations, int num_restarts );
  double train( int maxIterations );
  double trainOneIteration();
  double computeExpectedCounts();
  double logLikelihoodOfSegment(std::vector<int> full_seq);

  double getLogOfPointLikelihoodOfCompleteData();
  double getLogOfMarginalLikelihoodOfCompleteData();


  void dumpDistForPos(int pos);
  void dumpDists();
  void dumpCounts();
  void dumpDistForCount(int pos);

 private:
  void initializeEmissionDistributions();
  void initializeHiddenVariableDistribution();
  void clearCounts();   /* set the values of the count variables to zero */
  void checkDistributions(); /* check that all probabilitiy distributions are legal */
  void cacheProbabilties(); /* save the current emission and mixture component probabilities to a cache */
  void setProbabiltiesFromCache();

};
