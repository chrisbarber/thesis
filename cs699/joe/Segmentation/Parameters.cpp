#include "seg_common.h"
#include "Parameters.h"

Parameters::Parameters( )  :
  m_rng_seed(DEFAULT_RNG_SEED),
  m_alphabet_size(-1),
  m_pSymbolMap(NULL),
  m_initial_start_idx(-1),
  m_terminal_start_idx(-1),
  m_max_cardinality(DEFAULT_MAX_CARDINALITY),
  m_max_length(-1),
  m_num_random_restarts(DEFAULT_NUM_RANDOM_RESTARTS),
  m_numCVFolds(DEFAULT_NUM_CV_FOLDS),
  m_maxIterations(DEFAULT_MAX_ITERATIONS)
{

}

int Parameters::check(){
  int error = 0;

  if( m_rng_seed < 0 ){
    error = 1;
    fprintf( STDERR, "RNG seed less than zero" );
  }
  else if( m_alphabet_size == -1 || m_pSymbolMap == NULL ){
    error = 1;
    fprintf( STDERR, "ALPHABET not set!\n" );
  }
  

  return error;
}
