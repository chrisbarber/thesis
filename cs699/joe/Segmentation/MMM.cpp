#include "math.h"
#include "MMM.h"
#include <assert.h>

#ifdef LPind
#include <iostream>
#endif

double MultinomialMixtureModel::CONVERGENCE_THRESHOLD = 1e-5;

MultinomialMixtureModel::MultinomialMixtureModel(int max_cardinality, int alphabet_size, int max_length, int max_training_seqs) {

  this->m_MAX_CARDINALITY = max_cardinality;
  this->m_alphabet_size = alphabet_size;
  this->m_1over_alphabet_size = 1.0 / alphabet_size;
  this->m_MAX_LENGTH = max_length;
  this->m_MAX_TRAINING_SEQS = max_training_seqs;

  // set the size of the multidimensional arrays

  this->m_componentProbs.resize( m_MAX_CARDINALITY );
  this->m_componentProbs_cache.resize( m_MAX_CARDINALITY );
  for( int k = 0; k < m_MAX_CARDINALITY; k++ ){
    this->m_componentProbs[k] = 0.0;
    this->m_componentProbs_cache[k] = 0.0;
  }

  this->m_E.resize( m_MAX_CARDINALITY );
  this->m_E_cache.resize( m_MAX_CARDINALITY );
  this->m_E_counts.resize( m_MAX_CARDINALITY );

  for( int k = 0; k < m_MAX_CARDINALITY; k++ ){
    this->m_E[k].resize( m_MAX_LENGTH );
    this->m_E_cache[k].resize( m_MAX_LENGTH );
    this->m_E_counts[k].resize( m_MAX_LENGTH );

    for (int pos=0; pos < m_MAX_LENGTH; pos++){
      this->m_E[k][pos].resize( m_alphabet_size );
      this->m_E_cache[k][pos].resize( m_alphabet_size );
      this->m_E_counts[k][pos].resize( m_alphabet_size );

      for( int symbol = 0; symbol < m_alphabet_size; symbol++ ){
	this->m_E[k][pos][symbol] = -1;
	this->m_E_cache[k][pos][symbol] = -1;
	this->m_E_counts[k][pos][symbol] = -1;
      }
    }
  }

  this->m_countsByComponent.resize( m_MAX_TRAINING_SEQS );
  this->m_likelihoodByComponent.resize( m_MAX_TRAINING_SEQS );

  for (int seq_id=0; seq_id<m_MAX_TRAINING_SEQS; seq_id++ ){
    this->m_countsByComponent[seq_id].resize( m_MAX_CARDINALITY );
    this->m_likelihoodByComponent[seq_id].resize( m_MAX_CARDINALITY );

    for( int k = 0; k < m_MAX_CARDINALITY; k++ ){
      this->m_countsByComponent[seq_id][k] = 0.0;
      this->m_likelihoodByComponent[seq_id][k] = 0.0;
    }
  }

  this->m_componentCounts.resize( m_MAX_CARDINALITY );
  for( int k = 0; k < m_MAX_CARDINALITY; k++ ){
    this->m_componentCounts[k] = 0.0;
  }

  this->m_likelihoodBySeq.resize( m_MAX_TRAINING_SEQS );
  for (int seq_id=0; seq_id<m_MAX_TRAINING_SEQS; seq_id++ ){
    this->m_likelihoodBySeq[seq_id] = 0.0;
  }

  this->m_initCounts.resize( m_MAX_LENGTH );
  for (int pos=0; pos < m_MAX_LENGTH; pos++){
    this->m_initCounts[pos].resize( m_alphabet_size );
    for( int symbol = 0; symbol < m_alphabet_size; symbol++ ){
      this->m_initCounts[pos][symbol] = 0.0;
    }
  }

}

MultinomialMixtureModel::~MultinomialMixtureModel(void)
{
}


/* Initialilze a new mixture model. 
 *
 * alignment - the training data that will be used to learn parameters. Included here to initialize the emission distributions
 * mixmodLength - the number of positions in the mixture model
 * cardinality - the nuber of values that the hidden variable can take
 * startpos - the first position in the alighnment begin modeled 
 */
//void MultinomialMixtureModel::init( Alignment *alignment, int length, int cardinality, int startpos ){
void MultinomialMixtureModel::init(  Alignment *pAlignment, int length, int cardinality, int startpos ){

  this->m_pAlignment = pAlignment;
  this->m_length = length;
  this->m_cardinality = cardinality;
  this->m_startpos = startpos;
  this->m_endpos = startpos + m_length - 1;

  clearCounts(); 

  initializeEmissionDistributions();
  
  initializeHiddenVariableDistribution();

#ifdef _DEBUG
  checkDistributions();
#endif
}

double MultinomialMixtureModel::randomRestartTrain( Alignment *pAlignment, 
						    int length, int cardinality, int startpos, int maxIterations, int num_restarts ){

  double bestScore = -BIG_NUMBER;
  double loglike;

  for( int restart_idx = 0; restart_idx < num_restarts; restart_idx++ ){
	
    this->init( pAlignment, length, cardinality, startpos );

    loglike = this->train(maxIterations );

    // fprintf( STDERR, "log like fold %d, restart %d = %f\n", tuneFold, restart_idx, loglike );

    if( loglike > bestScore ){
      bestScore = loglike;
      this->cacheProbabilties();
    }

  }

  this->setProbabiltiesFromCache();

  return bestScore;
}

/**
 * train the mixture model using EM. Stop training when either the change in the log likelihood of the 
 * training set is less that CONVERGENCE_THRESHOLD or the maximum number of iterations is reached
 */
double MultinomialMixtureModel::train( int maxIterations){

  double last_loglike = -BIG_NUMBER;
  
  for (int iteration=0; iteration<maxIterations; iteration++){
    //printf( "training iteration %d\n", iteration );
    double loglike = trainOneIteration( );

    if( (loglike - last_loglike) < CONVERGENCE_THRESHOLD ){
      //fprintf( STDERR, "converged on iteration %d, %f  %f = %f\n", 
      //       iteration, loglike, last_loglike, (loglike-last_loglike) );
      break;
    }

    last_loglike = loglike;
  }


  return last_loglike;
}

/**
 * perform one iteration of EM
 * 
 * return the log likelihood of the training sequences
 */
double MultinomialMixtureModel::trainOneIteration(){

  // E-step, gather the counts
  double loglike = computeExpectedCounts( );
  
  // M-step
  double totalCompCounts = Utils::sum( m_componentCounts, m_cardinality );
  for (int compIdx=0; compIdx<m_cardinality; compIdx++){
    m_componentProbs[compIdx] = m_componentCounts[compIdx] / totalCompCounts;
  }

  //int firstComponent = m_dist0_isunchangeable ? 0 : 1;

  for (int compIdx=0; compIdx<m_cardinality; compIdx++){
    for( int pos = 0; pos < m_length; pos++ ){

      double the_sum = Utils::sum( m_E_counts[compIdx][pos], m_alphabet_size );

      if( the_sum == 0 ){
	for (int symbol=0; symbol<m_alphabet_size; symbol++){
	  m_E[compIdx][pos][symbol] = m_1over_alphabet_size;
	}
      }
      else{
	for (int symbol=0; symbol<m_alphabet_size; symbol++){
	  m_E[compIdx][pos][symbol] = m_E_counts[compIdx][pos][symbol] / the_sum;
	}
      }

      // smooth so that probabilites do not go to zero
      for (int symbol=0; symbol<m_alphabet_size; symbol++) m_E[compIdx][pos][symbol] += 0.001;

      Utils::normalize( m_E[compIdx][pos], m_alphabet_size );
    }
  }

#ifdef _DEBUG
   checkDistributions();
#endif

  return loglike;
}

/**
 * compute the expected number of times each model parameter is used over the whole training set
 * return the log likelihood of the training set 
 */
double MultinomialMixtureModel::computeExpectedCounts(){

  int pos, rel_pos;
  double loglike = 0.0; // log likelihood of all training sequences

  clearCounts();

  // Initialization
  for (int seqIdx=0; seqIdx<m_pAlignment->m_NROWS; seqIdx++){
    m_likelihoodBySeq[seqIdx] = 0.0;
    for (int compIdx=0; compIdx<m_cardinality; compIdx++){
      m_likelihoodByComponent[seqIdx][compIdx] = m_componentProbs[compIdx]; 
    }
  }
  for (int compIdx=0; compIdx<m_cardinality; compIdx++)  m_componentCounts[compIdx] = 0.0;

  // compute the likelihood for the individual components
  pos = m_startpos; rel_pos = 0;
  while( pos <= m_endpos ){
    for (int seqIdx=0; seqIdx<m_pAlignment->m_NROWS; seqIdx++){
      for (int compIdx=0; compIdx<m_cardinality; compIdx++){

	if( m_pAlignment->m_data[seqIdx][pos] >= 0 ){
	  m_likelihoodByComponent[seqIdx][compIdx] *= m_E[compIdx][rel_pos][ m_pAlignment->m_data[seqIdx][pos] ];
	}
      }
    }
    pos++; rel_pos++;
  }

  // store the likelihood for the sequence
  for (int seqIdx=0; seqIdx<m_pAlignment->m_NROWS; seqIdx++){
    for (int compIdx=0; compIdx<m_cardinality; compIdx++){
      m_likelihoodBySeq[seqIdx] += m_likelihoodByComponent[seqIdx][compIdx];
    }
    loglike += log( m_likelihoodBySeq[seqIdx] );
  }

  // divde to get the component counts
  for (int seqIdx=0; seqIdx<m_pAlignment->m_NROWS; seqIdx++){
    for (int compIdx=0; compIdx<m_cardinality; compIdx++){
      m_countsByComponent[seqIdx][compIdx] = m_likelihoodByComponent[seqIdx][compIdx] / m_likelihoodBySeq[seqIdx];
      m_componentCounts[compIdx] += m_countsByComponent[seqIdx][compIdx];
    }
  }
  
  // the E counts come from the components counts
  pos = m_startpos; rel_pos = 0;
  while( pos <= m_endpos ){
    for (int compIdx=0; compIdx<m_cardinality; compIdx++){
      for (int seqIdx=0; seqIdx<m_pAlignment->m_NROWS; seqIdx++){
	if( m_pAlignment->m_data[seqIdx][pos] >= 0 ){
	  m_E_counts[compIdx][rel_pos][ m_pAlignment->m_data[seqIdx][pos] ] += m_countsByComponent[seqIdx][compIdx];
	}
      }
    }
    pos++; rel_pos++;
  }

  return loglike;
}

/*
 * returns the log likelihood of the segment of the full sequence that this is a mixture model of.
 */
double MultinomialMixtureModel::logLikelihoodOfSegment(std::vector<int> full_seq){
  std::vector<double> componentLikelihood(m_cardinality);

  // init;
  #ifdef LPind
  for (int compIdx=0; compIdx<m_cardinality; compIdx++) componentLikelihood[compIdx] = 1.0; 
  #else
  for (int compIdx=0; compIdx<m_cardinality; compIdx++) componentLikelihood[compIdx] = m_componentProbs[compIdx]; 
  #endif

  int pos = m_startpos;
  int rel_pos = 0;
  while( pos <= m_endpos ){
    for (int compIdx=0; compIdx<m_cardinality; compIdx++){
      if( full_seq[pos] >= 0 ){
		componentLikelihood[compIdx] *= m_E[compIdx][rel_pos][ full_seq[pos] ];
      }
    }
    pos++;
    rel_pos++;
  }

#ifdef LPind
  for(int compIdx=0; compIdx<m_cardinality; compIdx++)
  {
    std::cout << log(componentLikelihood[compIdx]);
	if(m_cardinality-1 != compIdx) std::cout << ",";
  }
#endif  
  
  double totalLikelihood = Utils::sum( componentLikelihood, m_cardinality );

  /*
  for (int compIdx=0; compIdx<m_numComponents; compIdx++){
    m_componentCounts[compIdx] = componentLikelihood[compIdx] / totalLikelihood; 
  }

  if( totalLikelihood < -100.0 ){
    fprintf( STDERR, "%f\n", log( totalLikelihood ) );

    for (int compIdx=0; compIdx<m_numComponents; compIdx++){
      fprintf( STDERR, "likelihood for comp %d = %f", compIdx, componentLikelihood[compIdx] );
      fprintf( STDERR, "a[%d][%d] = %d", seq_idx, m_startpos, a[seq_idx][m_startpos] );
    }

    exit(0);
  }
  */

  //fprintf( STDERR, "total likelihood is %f (%f)\n", totalLikelihood, log(totalLikelihood) );

  return log( totalLikelihood );
}


void MultinomialMixtureModel::initializeEmissionDistributions(){

  //double total_counts = 0.0; // the total number of observed symbols in the alignment for the segment being modeled

  for( int component = 0; component < m_cardinality; component++ ){
    for( int pos = 0; pos < m_length; pos++ ){
      for( int symbol = 0; symbol < m_alphabet_size; symbol++ ){
	Utils::perturb( this->m_E[component][pos], m_alphabet_size );  // perturb normalizes 
      }
    }
  }


  /*
  // count the number of times each symbol is observed at each position
  for( int row = 0; row < alignment->N_ROWS; row++ ){
    for( int rel_pos = 0; rel_pos < this->m_length; rel_pos++ ){
      int pos = startpos + rel_pos;

      //printf( "symbol at (%d,%d) is %d\n", row, pos, a[row][pos] );
      
      if( alignment->data[row][pos] >= 0 ){
	this->m_initCounts[rel_pos][ a[row][pos] ] += 1.0;
	total_counts++;
      }
    }
  }
  for( int rel_pos = 0; rel_pos < this->m_length; rel_pos++ ){
    double the_sum = Utils::sum( this->m_initCounts[rel_pos], m_alphabet_size );
    
    for( int symbol = 0; symbol < m_alphabet_size; symbol++ ){
      double value = this->m_initCounts[rel_pos][symbol] / the_sum;  // 
      //printf( "value = %f\n", value );
      
      for( int component = 0; component < this->m_cardinality; component++ ){
	if( total_counts == 0 ){ this->m_E[component][rel_pos][symbol] = m_1over_alphabet_size; }
	else{
	  this->m_E[component][rel_pos][symbol] = value;
	}
	
	//printf( "initialized E(%d,%d,%d) to %f\n", component, rel_pos, symbol, value );
      }
    }
  }

  for( int component = 0; component < this->m_numComponents; component++ ){
    for( int rel_pos = 0; rel_pos < this->m_length; rel_pos++ ){
      Utils::perturb( this->m_E[component][rel_pos], ALPHABET_SIZE );  // perturb normalizes 
      //this->normalize( this->m_E[component][rel_pos], ALPHABET_SIZE );
    }
  }
  */

}

void MultinomialMixtureModel::initializeHiddenVariableDistribution(){

  for( int component = 0; component < this->m_cardinality; component++ ){
    this->m_componentProbs[component] = 1.0 / this->m_cardinality;
  }
  Utils::normalize( this->m_componentProbs, m_cardinality );
}



/**
 * set all counts to 0
 */
void MultinomialMixtureModel::clearCounts(){

  for (int i=0; i<m_MAX_CARDINALITY; i++){
    for (int j=0; j<m_MAX_LENGTH; j++){
      for (int k=0; k<m_alphabet_size; k++){
	this->m_E_counts[i][j][k] = 0.0;
      }
    }
  }

  for (int k=0; k<m_MAX_CARDINALITY; k++){
    this->m_componentCounts[k] = 0.0;
  }

  for (int j=0; j<m_MAX_LENGTH; j++){
    for (int l=0; l<m_alphabet_size; l++){
      //this->m_initCounts[j][l] = 0 + INIT_PSEUDO_COUNT;
      this->m_initCounts[j][l] = 0;
    }
  }

}

/*
 * return log Pr( D' | theta, model structure ) where D' is the (complete) data given by the sufficient statistics
 *
 * Use the sufficient staistics set from the last time set counts was called
 */
double MultinomialMixtureModel::getLogOfPointLikelihoodOfCompleteData(){

  double loglike = 0;

  for( int component_id = 0; component_id < m_cardinality; component_id++ ){

    if( m_componentCounts[component_id] > 0 ){
      assert( m_componentProbs[component_id] > 0.0 );
      loglike += m_componentCounts[component_id] * log( m_componentProbs[component_id] );
    }
  }

  for( int component_id = 0; component_id < m_cardinality; component_id++ ){
    for( int pos = 0; pos < m_length; pos++ ){
      for (int symbol_id=0; symbol_id<m_alphabet_size; symbol_id++){

	if( m_E_counts[component_id][pos][symbol_id] > 0 ){
	  assert(  m_E[component_id][pos][symbol_id] > 0 );
	  loglike += m_E_counts[component_id][pos][symbol_id] * log( m_E[component_id][pos][symbol_id] );
	}
      }
    }
  }

  return loglike;
}


/**
 * see Equations 13 and  35 in Heckerman's "Tutorial on Learning With Bayesian Networks" 
 */
double MultinomialMixtureModel::getLogOfMarginalLikelihoodOfCompleteData(){

  double loglike = 0.0;

  //double clusterSampleSize = m_numComponents; // Total pseudo counts of mixture model
  double clusterSampleSize = 1; // Total pseudo counts of mixture model
  double clusterUniformPseudoCount = clusterSampleSize / m_cardinality;
  double emissionSampleSize = 1;
  double symbolUniformPseudoCount  = emissionSampleSize / m_alphabet_size;

  // terms from the cluster variable
  loglike += gamma( clusterSampleSize ) - gamma( clusterSampleSize + m_sequenceCounts ); 
  for( int component_id = 0; component_id < m_cardinality; component_id++ ){
    loglike += gamma( m_componentCounts[component_id] + clusterUniformPseudoCount ) - gamma( clusterUniformPseudoCount );
  }

  //fprintf( STDERR, "loglike %d\n", loglike );

  // terms from the child variables
  for( int pos = 0; pos < m_length; pos++ ){
    for( int component_id = 0; component_id < m_cardinality; component_id++ ){
    
      loglike += gamma( emissionSampleSize ) - gamma( m_componentCounts[component_id] + emissionSampleSize );

      for (int symbol_id=0; symbol_id<m_alphabet_size; symbol_id++){
	loglike += gamma( symbolUniformPseudoCount + m_E_counts[component_id][pos][symbol_id] ) - gamma( symbolUniformPseudoCount );
      }

    }
  }
    
  return loglike;
}

void MultinomialMixtureModel::checkDistributions(){

  for (int i=0; i<m_length; i++){
    for (int j=0; j<m_cardinality; j++){
      double sum = Utils::sum( this->m_E[j][i], m_alphabet_size );
      //fprintf( STDERR, "sum is %f\n", sum );

      for (int k=0; k<m_alphabet_size; k++){
	if( m_E[j][i][k] < 0 || m_E[j][i][k] > 1 ){
	  fprintf( STDERR, "bad probability: %d %d %d, %f\n", j,i,k,m_E[j][i][k] );
	}
      }

      double d = 1.0 - sum/1.0;
      
      if( isnan(sum) ){
	fprintf( STDERR, "NaN" );
	exit(1);
      }

      //fprintf( STDERR, "checking distribution E(%d,%d)\n", i, j );
      //fprintf( STDERR, "d = %f\n", d );

      if( d > 1e-5 || d < -1e-5 ){
	fprintf( STDERR, "failed dist check at pos %d, component %d, sum = %f",
		 i, j, sum );
      }

      assert(  d< 1e-5 && d > -1e-5 );
    }
  }

}

void MultinomialMixtureModel::dumpDists(){

  fprintf( STDERR, "Dist for (%d:%d), %d comp\n", m_startpos, m_endpos, m_cardinality );

  for (int comp=0; comp<m_cardinality; comp++){
    fprintf( STDERR, "Pr( COMP = %d ) = %1.3f\n", comp, m_componentProbs[comp] );
  }

  for (int pos=0; pos<m_length; pos++){
    dumpDistForPos(pos);
    fprintf( STDERR, "\n" );
  }

}

void MultinomialMixtureModel::dumpDistForPos(int pos){

  char code[] = "ACDEFGHIKLMNPQRSTVWY-";

  fprintf( STDERR, "POS  %2d  ", pos );
  //for (int i=0; i<ALPHABET_SIZE; i++)  fprintf( STDERR, "%5d ", i ); 
  for (int i=0; i<m_alphabet_size; i++)  fprintf( STDERR, "%5c ", code[i] ); 
  fprintf( STDERR, "\n" );

  for (int j=0; j<m_cardinality; j++){
    fprintf( STDERR, "COMP %2d: ", j );

    for (int i=0; i<m_alphabet_size; i++){
      fprintf( STDERR, "%1.3f ", m_E[j][pos][i] ); 
    }

    fprintf( STDERR, "\n" );
  }

}

void MultinomialMixtureModel::dumpCounts(){

  fprintf( STDERR, "Counts for (%d:%d), %d comp\n", m_startpos, m_endpos, m_cardinality );

  for (int comp=0; comp<m_cardinality; comp++){
    fprintf( STDERR, "Cnt( COMP = %d ) = %1.3f\n", comp, m_componentCounts[comp] );
  }

  for (int pos=0; pos<m_length; pos++){
    dumpDistForCount(pos);
    fprintf( STDERR, "\n" );
  }

}

void MultinomialMixtureModel::dumpDistForCount(int pos){

  char code[] = "ACDEFGHIKLMNPQRSTVWY-";

  fprintf( STDERR, "POS  %2d  ", pos );
  //for (int i=0; i<ALPHABET_SIZE; i++)  fprintf( STDERR, "%5d ", i ); 
  for (int i=0; i<m_alphabet_size; i++)  fprintf( STDERR, "%5c ", code[i] ); 
  fprintf( STDERR, "\n" );

  for (int j=0; j<m_cardinality; j++){
    fprintf( STDERR, "COMP %2d: ", j );

    for (int i=0; i<m_alphabet_size; i++){
      fprintf( STDERR, "%1.3f ", m_E_counts[j][pos][i] ); 
    }

    fprintf( STDERR, "\n" );

  }
}

void MultinomialMixtureModel::cacheProbabilties(){

  for (int i=0; i<this->m_cardinality; i++){
    for (int j=0; j<this->m_length; j++){
      for (int k=0; k<m_alphabet_size; k++){
	this->m_E_cache[i][j][k] = this->m_E[i][j][k];
      }
    }
  }
  
  for (int j=0; j<this->m_cardinality; j++){
    this->m_componentProbs_cache[j] = this->m_componentProbs[j];
  }

}

void MultinomialMixtureModel::setProbabiltiesFromCache(){

  for (int i=0; i<this->m_cardinality; i++){
    for (int j=0; j<this->m_length; j++){
      for (int k=0; k<m_alphabet_size; k++){
	this->m_E[i][j][k] = this->m_E_cache[i][j][k];
      }
    }
  }
  
  for (int j=0; j<this->m_cardinality; j++){
    this->m_componentProbs[j] = this->m_componentProbs_cache[j];
  }

}

