#include "TuneSets.h"
#include "Alphabets.h"
#include "Alignment.h"
#include "MMM.h"
#include "SegmentScorer.h"

int main (int argc, char* argv[]){
  int alphabet_size = 5;

  int *pSymbolMap = Alphabets::getDNASymbolMapWithGapCharacter();

  Alignment *pAlignment = Alignment::loadAlignmentFromFASTAFile( argv[1], alphabet_size, pSymbolMap );

  printf( "Loaded alignment with %d rows and %d columns\n", pAlignment->m_NROWS, pAlignment->m_NCOLS );

  MultinomialMixtureModel *pMMM = new MultinomialMixtureModel( 5, 5, 10, pAlignment->m_NROWS );

  printf( "created MMM\n" );

  
  pMMM->init( pAlignment, 10, 2, 1 );
  pMMM->dumpDists();

  printf( "training\n" );
  pMMM->train(5);

  pMMM->dumpDists();

  TuneSets *pTuneSets = new TuneSets( 103, 10 );

  Alignment *pSubAlignment = pAlignment->getSubAlignment( pTuneSets->m_testSeqs[0] );

  printf( "num rows in sub alignment is %d\n", pSubAlignment->m_NROWS );

  int range_min_startpos = 1;
  int range_max_startpos = 100;
  int maxCardinality = 5;
  int maxLength = 20;
  int numCVFolds = 10;
  int maxIterations = 10;
  int numRestarts = 10;

  printf( "creating a segment scorer\n" );

  SegmentScorer *pScorer = new SegmentScorer( pAlignment, 
					      range_min_startpos,
					      range_max_startpos,
					      maxLength,
					      maxCardinality,
					      numCVFolds,
					      maxIterations,
					      numRestarts );

  printf( "created a segment scorer\n" );

  pScorer->score_segments_in_range();
}

