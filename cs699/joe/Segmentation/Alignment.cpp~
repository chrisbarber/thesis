#include <iostream>
#include "seg_common.h"
#include "Utils.h"
#include "Alignment.h"

Alignment::~Alignment(){
}

Alignment::Alignment(int alphabet_size, int num_rows, int num_cols){
  this->m_alphabet_size = alphabet_size;
  this->m_NROWS = num_rows;
  this->m_NCOLS = num_cols;

  // reserve space for the alignment data 
  this->m_data.resize(m_NROWS);

  for( int row = 0; row < m_NROWS; row++ ){
    m_data[row].resize( m_NCOLS+1 ); //

    m_data[row][0] = -2; // we start counting sequene positions from 1

    for (int col=0; col<=m_NCOLS; col++){
      m_data[row][col] = -1;
    }
  }
}

Alignment* Alignment::getSubAlignment( std::vector<int> rowsToKeep ){

  Alignment *pSubAlignment = new Alignment( this->m_alphabet_size, rowsToKeep.size(), this->m_NCOLS );

  for( uint row = 0; row < rowsToKeep.size(); row++ ){
    pSubAlignment->m_data[row] = this->m_data[ rowsToKeep[row] ]; // this does a deep copy, right?
  }

  /** check to verify that the assignment is not just a referene copy
     
      int i = 1, j = 1;
      printf( "In original (%d,%d) = %d\n", rowsToKeep[i], j, m_data[rowsToKeep[i]][j] );
      printf( "In copy (%d,%d) = %d\n", i, j, pSubAlignment->m_data[i][j] );

      pSubAlignment->m_data[i][j]++;

      printf( "Change madeee\n" );

      printf( "In original (%d,%d) = %d\n", rowsToKeep[i], j, m_data[rowsToKeep[i]][j] );
      printf( "In copy (%d,%d) = %d\n", i, j, pSubAlignment->m_data[i][j] );
  */
  
  
  return pSubAlignment;
}


/**
 * read an alignment from a file
 */
Alignment* Alignment::loadAlignmentFromFile( char* filename, int alphabet_size, int *pSymbolMap ){
  FILE *F;
  
  if( VERBOSE ){ 
    printf( "Loading alignment from '%s'\n", filename );
  }

  F = fopen( filename, "r" );
  
  if( F == NULL ){
    Utils::error( "cannot open alignment file" );
  }
 
  int num_rows, num_cols;

  fscanf( F, "%d %d", &num_rows, &num_cols );

  Alignment *alignment = new Alignment( alphabet_size, num_rows, num_cols );

  char c;

  for( int row = 1; row <= num_rows; row++ ){
    for( int col = 1; col <= num_cols; col++ ){
      //fscanf( F, "%d", &(alignment->m_data[row][col]) );
      fscanf( F, "%c", &c );

      int symbol = pSymbolMap[c];

      if( symbol < 0 ){
	char *pBuffer = new char[80];
	sprintf( pBuffer, "unknown symbol '%c' in column %d of row %d\n", c, col, row );
	Utils::error( pBuffer );
      }

      alignment->m_data[row][col] = symbol;
    }
  }

  fclose( F );

  if( VERBOSE ){ 
    printf( "Alignment loaded \n" );
  }


  return alignment;
}

Alignment* Alignment::loadAlignmentFromFASTAFile( char *filename, int alphabet_size, int *pSymbolMap ){

  FILE *F;
  int bufferSize = 1024;
  char *pBuffer = new char[bufferSize];
  char *pReturnVal;
  int n_rows = -1; // the number of sequences in the alignment
  int n_cols = -1; // the number of symbols for each sequence in the alignment
  Alignment *pAlignment = NULL;
  size_t len;
  bool length_found = false;
  int col_idx, row_idx;
  int symbol;
  
  if( VERBOSE ){ 
    printf( "Loading alignment from '%s'\n", filename );
  }

  F = fopen( filename, "r" );
  
  if( F == NULL ){
    Utils::error( "cannot open alignment file" );
  }

  //
  // determine the number and length of the sequences
  //

  // read the first line
  pReturnVal = fgets( pBuffer, bufferSize, F );
  
  if( pReturnVal == 0 ){
    Utils::error( "error reading first line of alignment file" );
  }
  else if( pBuffer[0] != '>' ){
    Utils::error( "expected '>' not found on first line of alignment file\n" );
  }
  else{
    len = strlen( pBuffer );

    if( pBuffer[len-1] != '\n' ){
      Utils::error( "First line of alignment file too long\n" );
    }
  }

  n_cols = 0;
  
  do{
    
    pReturnVal = fgets( pBuffer, bufferSize, F );

    if( pReturnVal == 0 ){ Utils::error( "error reading alignment file\n" ); }

    if( pBuffer[0] == '>' ){
      length_found = true;
    }
    else{
      len = strlen( pBuffer );
      n_cols += len;
    
      if( pBuffer[len-1] == '\n' ){
	n_cols--; // don't count the \n in the length of the file
      }
    }

  }while( !length_found );

  n_rows = 1; // we've alredy read the first sequence

  while( pReturnVal != 0 ){

    if( pReturnVal[0] == '>' ){
      n_rows++;
    }

    pReturnVal = fgets( pBuffer, bufferSize, F );

  }while( pReturnVal > 0 );

  //printf( "The length of sequenes is %d\n", n_cols );
  //printf( "The number of sequenes is %d\n", n_rows );

  //
  // create the alignment object and re-read the file filling in the proper symbols
  //

  pAlignment = new Alignment( alphabet_size, n_rows, n_cols );

  rewind( F );

  row_idx = 0; 

  while(row_idx < pAlignment->m_NROWS ){
    pReturnVal = fgets( pBuffer, bufferSize, F );
    assert( pReturnVal != 0 );
    assert( pReturnVal[0] == '>' );
    len = strlen( pBuffer );
    if( pReturnVal[len-1] != '\n' ){
      Utils::error( "Sequence name line of FASTA file too long" );
    }
    // printf( "\tReading row %d, %s", row_idx, pBuffer );

    // count sequence positions in the alignment starting from 1
    col_idx = 1;

    while( col_idx <= pAlignment->m_NCOLS ){
      pReturnVal = fgets( pBuffer, bufferSize, F );
      
      assert( pReturnVal != 0 );
      assert( pBuffer[0] != '>' );

      len = strlen( pBuffer );

      // printf( "%s", pBuffer );

      if( pBuffer[len-1] == '\n' ) len--;

      for (uint i=0; i<len; i++){
	symbol = pSymbolMap[ pBuffer[i] ];
	assert( symbol >= -1 );

	pAlignment->m_data[row_idx][col_idx] = symbol;
	col_idx++;
      }

      // printf( "%d of %d columns read\n", col_idx, pAlignment->m_NCOLS );
    }

    assert( col_idx == pAlignment->m_NCOLS+1 ); // make certain the line did not have more symbols than expected
    
    row_idx++;
  }

  delete pBuffer;

  return pAlignment;
}


