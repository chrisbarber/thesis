#pragma once

#include <vector>

class SegmentScores{

 public:
  int m_numPositions;
  int m_maxLength;
  int m_maxCardinality;

  std::vector< std::vector< std::vector<bool> > > m_segmentAlreadyScored;
 private:

  std::vector< std::vector< std::vector<double> > > m_segmentScores;

  // a flag that indicates whether or not a segment has been scored. Useful when
  // filling in a partially scored 


 public:
  SegmentScores( int numPositions, int maxLength, int maxCardinality );

  void setScore( int startpos, int length, int cardinality, double score );

  int readFromFile( char *filename );

  int writeToFile( char *filename );

 private: 

  void init_scores();

};
