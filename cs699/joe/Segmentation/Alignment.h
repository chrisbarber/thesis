#pragma once

#include <vector>

class Alignment{

 public:
 
  /* the number of symbols in the alphabet */
  int m_alphabet_size;

  /* the width of the alignment. That is, the number of rows */
  int m_NROWS;

  /* the length of the alignment. That is, the number of columns */
  int m_NCOLS;

  /* the data of the alignment. -1 means missing data */
  //int **m_data;
  std::vector< std::vector<int> > m_data;
  std::vector< char * > m_seq_name;

  /* constructor */
  Alignment(int alphabet_size, int num_rows, int num_cols);
  ~Alignment();

  /*  */
  static Alignment* loadAlignmentFromFile( char* filename, int alphabet_size, int *pSymbolMap );

  static Alignment* loadAlignmentFromFASTAFile( char *filename, int alphabet_size, int *pSymbolMap );

  /* return an alignment that contains a subset of the rows of the orignal alignment. A copy is made */
  Alignment *getSubAlignment( std::vector<int> rowsToKeep );

  void dumpSequenceSegment( int seq_idx, int start_pos, int end_pos );

};
