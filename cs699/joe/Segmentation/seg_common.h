#pragma once

#define VERBOSE 1
//#define MAX_SEQ_LENGTH 4000
//#define MAX_MIX_MODEL_LENGTH 15
//#define MAX_CARDINALITY 3

//#define MAX_SEQS 500
//#define ALPHABET_SIZE 21
#define NUM_RESTARTS  10
//#define INIT_PSEUDO_COUNT 0.05
#define ROOT_DIR "/data/projects/fomm/data/alignments/"
#define STDERR stderr
#define STDOUT stdout
//#define ALIGNMENT_FILE_NAME "alignment.txt"
//#define FOLDS_FILE_NAME "folds.txt"
//#define MAX_EM_ITERATIONS 15
#define BIG_NUMBER 1e307
#define LOG2(x) (log(x) * 1.442695040888963)
#define MIN(x,y) ( ((x) < (y)) ? (x) : (y) )
#define MAX(x,y) ( ((x) > (y)) ? (x) : (y) )

