#include "Alphabets.h"

int *Alphabets::getAminoAcidSymbolMap(){

  int *pAASymbolMap = new int[256];

  for (int i=0; i<256; i++){
    pAASymbolMap[i] = -2;
  }

  pAASymbolMap['A'] = pAASymbolMap['a'] = 0;
  pAASymbolMap['C'] = pAASymbolMap['c'] = 1;
  pAASymbolMap['D'] = pAASymbolMap['d'] = 2;
  pAASymbolMap['E'] = pAASymbolMap['e'] = 3;
  pAASymbolMap['F'] = pAASymbolMap['f'] = 4;
  pAASymbolMap['G'] = pAASymbolMap['g'] = 5;
  pAASymbolMap['H'] = pAASymbolMap['h'] = 6;
  pAASymbolMap['I'] = pAASymbolMap['i'] = 7;
  pAASymbolMap['K'] = pAASymbolMap['k'] = 8;
  pAASymbolMap['L'] = pAASymbolMap['l'] = 9;
  pAASymbolMap['M'] = pAASymbolMap['m'] = 10;
  pAASymbolMap['N'] = pAASymbolMap['n'] = 11;
  pAASymbolMap['P'] = pAASymbolMap['p'] = 12;
  pAASymbolMap['Q'] = pAASymbolMap['q'] = 13;
  pAASymbolMap['R'] = pAASymbolMap['r'] = 14;
  pAASymbolMap['S'] = pAASymbolMap['s'] = 15;
  pAASymbolMap['T'] = pAASymbolMap['t'] = 16;
  pAASymbolMap['V'] = pAASymbolMap['v'] = 17;
  pAASymbolMap['W'] = pAASymbolMap['w'] = 18;
  pAASymbolMap['Y'] = pAASymbolMap['Y'] = 19;
  pAASymbolMap['?'] = -1;
  

  return pAASymbolMap;
}

int *Alphabets::getAminoAcidSymbolMapWithGapCharacter(){

  int *pAASymbolMap = new int[256];

  for (int i=0; i<256; i++){
    pAASymbolMap[i] = -2;
  }

  pAASymbolMap['A'] = pAASymbolMap['a'] = 0;
  pAASymbolMap['C'] = pAASymbolMap['c'] = 1;
  pAASymbolMap['D'] = pAASymbolMap['d'] = 2;
  pAASymbolMap['E'] = pAASymbolMap['e'] = 3;
  pAASymbolMap['F'] = pAASymbolMap['f'] = 4;
  pAASymbolMap['G'] = pAASymbolMap['g'] = 5;
  pAASymbolMap['H'] = pAASymbolMap['h'] = 6;
  pAASymbolMap['I'] = pAASymbolMap['i'] = 7;
  pAASymbolMap['K'] = pAASymbolMap['k'] = 8;
  pAASymbolMap['L'] = pAASymbolMap['l'] = 9;
  pAASymbolMap['M'] = pAASymbolMap['m'] = 10;
  pAASymbolMap['N'] = pAASymbolMap['n'] = 11;
  pAASymbolMap['P'] = pAASymbolMap['p'] = 12;
  pAASymbolMap['Q'] = pAASymbolMap['q'] = 13;
  pAASymbolMap['R'] = pAASymbolMap['r'] = 14;
  pAASymbolMap['S'] = pAASymbolMap['s'] = 15;
  pAASymbolMap['T'] = pAASymbolMap['t'] = 16;
  pAASymbolMap['V'] = pAASymbolMap['v'] = 17;
  pAASymbolMap['W'] = pAASymbolMap['w'] = 18;
  pAASymbolMap['Y'] = pAASymbolMap['Y'] = 19;
  pAASymbolMap['-'] = 20;
  pAASymbolMap['?'] = -1;
  

  return pAASymbolMap;
}


int *Alphabets::getDNASymbolMap(){

  int *pDNASymbolMap = new int[256];

  for (int i=0; i<256; i++){
    pDNASymbolMap[i] = -2;
  }

  pDNASymbolMap['A'] = pDNASymbolMap['a'] = 0;
  pDNASymbolMap['C'] = pDNASymbolMap['c'] = 1;
  pDNASymbolMap['G'] = pDNASymbolMap['g'] = 2;
  pDNASymbolMap['T'] = pDNASymbolMap['t'] = 3;
  pDNASymbolMap['?'] = -1;

  return pDNASymbolMap;
}

int *Alphabets::getDNASymbolMapWithGapCharacter(){

  int *pDNASymbolMap = new int[256];

  for (int i=0; i<256; i++){
    pDNASymbolMap[i] = -2;
  }

  pDNASymbolMap['A'] = pDNASymbolMap['a'] = 0;
  pDNASymbolMap['C'] = pDNASymbolMap['c'] = 1;
  pDNASymbolMap['G'] = pDNASymbolMap['g'] = 2;
  pDNASymbolMap['T'] = pDNASymbolMap['t'] = 3;
  pDNASymbolMap['-'] = 4;
  pDNASymbolMap['?'] = -1;

  return pDNASymbolMap;
}

int *Alphabets::getBinarySymbolMap(){

  int *pBinarySymbolMap = new int[256];

  for (int i=0; i<256; i++){
    pBinarySymbolMap[i] = -2;
  }

  pBinarySymbolMap['0'] = 0;
  pBinarySymbolMap['1'] = 1;
  pBinarySymbolMap['?'] = -1;

  return pBinarySymbolMap;
}

int *Alphabets::getMatlabBinarySymbolMap(){

  int *pMatlabBinarySymbolMap = new int[256];

  for (int i=0; i<256; i++){
    pMatlabBinarySymbolMap[i] = -2;
  }

  pMatlabBinarySymbolMap['1'] = 0;
  pMatlabBinarySymbolMap['2'] = 1;
  pMatlabBinarySymbolMap['?'] = -1;
  pMatlabBinarySymbolMap['0'] = -1;

  return pMatlabBinarySymbolMap;
}

