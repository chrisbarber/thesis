#include <assert.h>
#include "seg_common.h"
#include "Utils.h"
#include "TuneSets.h"

TuneSets::TuneSets(int num_training_seqs, int numFolds)
{

  this->m_numFolds = numFolds;

  this->m_numTestSeqsInFold.resize(m_numFolds);
  this->m_numTrainingSeqsInFold.resize(m_numFolds); 
  this->m_trainingSeqs.resize(m_numFolds);
  this->m_testSeqs.resize(m_numFolds);


  int baseIdx = 0;

  int *permutation = Utils::get_random_permutation_of_integers( num_training_seqs );

  for( int fold = 0; fold < m_numFolds; fold++ ){
    int testSetSize     = (num_training_seqs / m_numFolds) + ( num_training_seqs % m_numFolds > fold ? 1 : 0 );
    int trainingSetSize = num_training_seqs - testSetSize;

    this->m_numTestSeqsInFold[fold] = testSetSize;
    this->m_numTrainingSeqsInFold[fold] = trainingSetSize;

    this->m_testSeqs[fold].resize(testSetSize);
    this->m_trainingSeqs[fold].resize(trainingSetSize);

    int testIdx = 0, trainIdx = 0;

    for (int seqIdx=0; seqIdx<num_training_seqs; seqIdx++){
      int permutedIdx = permutation[seqIdx];
      bool isTestSeq = permutedIdx >= baseIdx && permutedIdx < baseIdx+testSetSize;

      //bool isTestSeq = seqIdx >= baseIdx && seqIdx < baseIdx+testSetSize;

      if( isTestSeq ) m_testSeqs[fold][testIdx++] = seqIdx;
      else m_trainingSeqs[fold][trainIdx++] = seqIdx;
    }

    assert( testIdx == testSetSize );
    assert( trainIdx == trainingSetSize );

    baseIdx += testSetSize;

    fprintf( STDERR, "fold = %d\n", fold );
    fprintf( STDERR, "TEST SEQS: " );
    for (int i=0; i<testSetSize; i++)  fprintf( STDERR, "%d ", m_testSeqs[fold][i] );
    fprintf( STDERR, "\nTRAIN SEQS: " );
    for (int i=0; i<trainingSetSize; i++)  fprintf( STDERR, "%d ", m_trainingSeqs[fold][i] );
    fprintf( STDERR, "\n\n" );

  }

  assert( baseIdx == num_training_seqs );
}



TuneSets::~TuneSets(void)
{

}
