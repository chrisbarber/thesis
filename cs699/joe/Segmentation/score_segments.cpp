#include "Parameters.h"
#include "Alignment.h"
#include "Alphabets.h"
#include "MMM.h"
#include "SegmentScorer.h"
#include "seg_common.h"

Parameters* read_input_parameters(int argc, char **argv);
void usage();

int main (int argc, char* argv[]){
  Parameters *pParameters = NULL;
  Alignment *pAlignment = NULL;
  SegmentScorer *pScorer = NULL;

  //
  // read the parameters
  //
  pParameters = read_input_parameters(argc, argv);

  //
  // initialize the random number generator
  //
  printf( "initializing RNG to %d\n", pParameters->m_rng_seed );
  srand( pParameters->m_rng_seed );

  //
  // read the alignment file
  //
  pAlignment = Alignment::loadAlignmentFromFASTAFile( pParameters->m_alignment_filename, 
						      pParameters->m_alphabet_size, 
						      pParameters->m_pSymbolMap );

  printf( "Loaded alignment with %d rows and %d columns\n", pAlignment->m_NROWS, pAlignment->m_NCOLS );

  //
  // verify and set initial and terminal start indicies
  //
  if( pParameters->m_initial_start_idx == -1 ) pParameters->m_initial_start_idx = 1;
  else{
    if( pParameters->m_initial_start_idx > pAlignment->m_NCOLS ){
      fprintf( STDERR, "start idx is too big\n" );
      usage();
    }
  }
  if( pParameters->m_terminal_start_idx == -1 ) pParameters->m_terminal_start_idx = pAlignment->m_NCOLS;
  else{
    if( pParameters->m_terminal_start_idx > pAlignment->m_NCOLS ){
      fprintf( STDERR, "end idx is too big\n" );
      usage();
    }
  }

  pScorer = new SegmentScorer( pAlignment, 
			       pParameters->m_initial_start_idx,
			       pParameters->m_terminal_start_idx,
			       pParameters->m_max_length,
			       pParameters->m_max_cardinality,
			       pParameters->m_numCVFolds,
			       pParameters->m_maxIterations,
			       pParameters->m_num_random_restarts );


  // score the segments...
  pScorer->score_segments_in_range();


}

Parameters* read_input_parameters(int argc, char **argv){
  Parameters *pParameters = new Parameters();

  int i = 0;
  printf( "argc is %d\n", argc );

  for(i=1;(i<argc) && ((argv[i])[0] == '-');i++) {
    printf( "%d\n", i );
    printf( "%s\n", argv[i] );

    switch ((argv[i])[1]) 
    { 
    case 'h': usage(); exit(0);
    case 'r': i++; pParameters->m_rng_seed=atoi(argv[i]); break;
    case 'i': i++; pParameters->m_initial_start_idx =atoi(argv[i]); break;
    case 't': i++; pParameters->m_terminal_start_idx =atoi(argv[i]); break;
    case 'c': i++; pParameters->m_max_cardinality=atoi(argv[i]); break;
    case 'l': i++; pParameters->m_max_length=atoi(argv[i]); break;
    case 'e': i++; pParameters->m_num_random_restarts=atoi(argv[i]); break;
    case 'f': i++; pParameters->m_numCVFolds = atoi(argv[i]); break;
    case 's': i++; pParameters->m_maxIterations = atoi(argv[i]); break;
    case 'a':
	  switch( (argv[i])[2] )
	  {
	  case 'D': pParameters->m_alphabet_size = 4;  pParameters->m_pSymbolMap = Alphabets::getDNASymbolMap(); break;
	  case 'X': pParameters->m_alphabet_size = 5;  pParameters->m_pSymbolMap = Alphabets::getDNASymbolMapWithGapCharacter(); break;
	  case 'A': pParameters->m_alphabet_size = 20; pParameters->m_pSymbolMap = Alphabets::getAminoAcidSymbolMap(); break;
	  case 'Y': pParameters->m_alphabet_size = 21; pParameters->m_pSymbolMap = Alphabets::getAminoAcidSymbolMapWithGapCharacter(); break;
	  case 'B': pParameters->m_alphabet_size = 2;  pParameters->m_pSymbolMap = Alphabets::getBinarySymbolMap(); break;
	  case 'M': pParameters->m_alphabet_size = 2;  pParameters->m_pSymbolMap = Alphabets::getMatlabBinarySymbolMap(); break;
	  default: printf("\nUnrecognized alphabet option %s!\n\n",argv[i]);
	  }
	break;
    default:
	  printf("\nUnrecognized option %s!\n\n",argv[i]);
	  usage();
	  exit(0);
    }

  }
  if((i+1)>argc) {
    printf("\nNot enough input parameters!\n\n ");
    usage();
  }
  strcpy (pParameters->m_alignment_filename, argv[i]);
  //strcpy (modelfile, argv[i+1]);


  // check the consistency of the parameters
  if( pParameters->check() ){
    usage();
  }

  return pParameters;
}

void usage(){
  printf( "score_segments\n" );

  printf( "-h help\n" );
  printf( "-r <rng seed>\n" );
  printf( "-i <initial segment start index>\n" );
  printf( "-t <terminal segment start index>\n" );
  printf( "-c <max cardinality>\n" );
  printf( "-l <max segment length>\n" );
  printf( "-e <number of random restarts>\n" );
  printf( "-f <number of CV folds>\n" );
  printf( "-s <max number of EM iterations>\n" );
  printf( "-aD use DNA alphabet\n" );
  printf( "-aX use DNA alphabet with gaps \n" );
  printf( "-aA use AA alphabet \n" );
  printf( "-aY use AA alphabet with gaps\n" );
  printf( "-aB use Binary alphabet\n" );
  
  exit(-1);
}
