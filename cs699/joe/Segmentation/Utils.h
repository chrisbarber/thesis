#pragma once

#include <vector>

class Utils
{
public:
  Utils(void);
  ~Utils(void);

  static void error( char *s );

  static double columnEntropy( int **a, int *rows, int numRows, int column );

  static double distributionEntropy( double *d, int length );

  //static double sum( double *d, int length );
  static double sum( std::vector<double>& d, int length );

  //static void perturb( double *d, int length );
  static void perturb( std::vector<double>& d, int length );

  static void normalize( std::vector<double>& d, int length );
  
  static void zeroout( double *x, int length );

  static bool file_exists( const char * filename);

  /** return a permutation of the integers from 0:max_v-1 */
  static int* get_random_permutation_of_integers( int max_v );

};
