#pragma once

#include <vector>

#define DEFAULT_RNG_SEED 2341679
#define DEFAULT_NUM_RANDOM_RESTARTS 10
#define DEFAULT_MAX_CARDINALITY 2
#define DEFAULT_MAX_ITERATIONS 10
#define DEFAULT_NUM_CV_FOLDS 5
#define DEFAULT_MAX_ITERATIONS 10

class Parameters{

 public:

  /** seed for the random number generator */
  unsigned int m_rng_seed;

  /** */
  char m_alignment_filename[256];

  /** */
  int* m_pSymbolMap;

  /** */
  int m_alphabet_size;

  /** */
  int m_initial_start_idx, m_terminal_start_idx;

  /** */
  int m_max_cardinality;

  /** */
  int m_max_length;

  /** the number of random restarts used during EM */
  int m_num_random_restarts;

  /** the number of CV folds (only used if using the CV scoring method) */
  int m_numCVFolds;

  /** the maximum number of EM iterations prior to declaring convergence */
  int m_maxIterations;

  Parameters();

  /** verify that the parameters are consistent */
  int check();
};
