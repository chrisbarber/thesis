#include "stdlib.h"
#include "stdio.h"
#include "assert.h"
#include "math.h"
#include "seg_common.h"
#include "Utils.h"

Utils::Utils(void)
{
}

Utils::~Utils(void)
{
}

void Utils::error( char *s ){
  printf( "ERROR: %s\n",s );
  exit(1);
}

double Utils::sum( std::vector<double> &d, int length ){
  double sum = 0.0;

  for (int i=0; i<length; i++) sum += d[i];
  
  return sum;
}


//void Utils::normalize( double *d, int length ){
void Utils::normalize( std::vector<double> &d, int length ){
  double sum = 0.0;

  for (int i=0; i<length; i++) sum += d[i];

  for (int i=0; i<length; i++) d[i] = d[i] / sum;
}

//void Utils::perturb( double *dist, int length ){
void Utils::perturb( std::vector<double> &d, int length ){
  for (int i=0; i<length; i++){
    double random = (double)rand() / RAND_MAX;

    //fprintf( STDERR, "random = %f\n", random );

    double a = d[i] - d[i]*0.1f;
    double b = d[i] + d[i]*0.1f;

    d[i] = a + (b-a) * random;
  }

  normalize( d, length );
}

double Utils::columnEntropy( int **a, int *rows, int numRows, int column ){

  /*
  double counts[ALPHABET_SIZE];

  zeroout( counts, ALPHABET_SIZE );

  //fprintf( STDERR, "Getting entropy of column %d from alignment with %d rows\n", column, numRows );

  for (int rowIdx=0; rowIdx<numRows; rowIdx++){
    int row = rows[rowIdx];
    //fprintf( STDERR, "\t on row %d\n", row );
    //fprintf( STDERR, "\t alignment value = %d\n", a[row][column] );

    if( a[row][column] >= 0 ){
      //fprintf( STDERR, "\t count so far = %d\n", counts[ a[row][column] ] );
      counts[ a[row][column] ]++;
    }
  }

  normalize( counts, ALPHABET_SIZE );

  return distributionEntropy( counts, ALPHABET_SIZE );
  */

  return -1.0;
}


void Utils::zeroout( double *x, int length ){
  for (int i=0; i<length; i++) x[i] = 0;
}

double Utils::distributionEntropy( double *d, int length ){

  double theSum = 0.0;

  for (int i=0; i<length; i++) if( d[i] != 0 ) theSum += d[i] * LOG2( d[i] );
    
  return -theSum;
}

bool Utils::file_exists(const char * filename)
{
  if (FILE * file = fopen(filename, "r")){
    fclose(file);
    return true;
  }
  return false;
}

int* Utils::get_random_permutation_of_integers( int max_v ){

  int *ret = new int[max_v];
  
  double *random_numbers = new double[max_v];
  double *used = new double[max_v];

  Utils::zeroout( used, max_v );

  // set the random numbers
  for( int i = 0; i < max_v; i++ ){
    random_numbers[i] = rand();
    assert( random_numbers[i] >= 0 );
  }

  for( int rank = 0; rank < max_v; rank++ ){
    double max_so_far = -1;
    int idx;

    for (int i=0; i<max_v; i++){

      if( used[i] == 0.0 && random_numbers[i] > max_so_far ){
	max_so_far = random_numbers[i];
	idx = i;
      }
      
    }

    assert( max_so_far != -1 );
    ret[rank] = idx;
    used[idx] = 1.0;
  }

  delete random_numbers;
  delete used;

  return ret;
}






