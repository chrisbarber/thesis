#include <assert.h>
#include "SegmentScores.h"

SegmentScores::SegmentScores( int numPositions, int maxLength, int maxCardinality ){
  
  m_numPositions = numPositions;
  m_maxLength=  maxLength;
  m_maxCardinality = maxCardinality;
  
  //printf( "ENTERX SegmentScores() %d %d %d", numPositions, maxLength, maxCardinality ); 

  init_scores();

  //printf( "LEAVE SegmetScores()" );
}

void SegmentScores::init_scores(){

  m_segmentScores.resize( m_numPositions+1 );
  m_segmentAlreadyScored.resize( m_numPositions+1 );

  for (int pos=1; pos<= m_numPositions; pos++){
    m_segmentScores[pos].resize( m_maxLength+1 );
    m_segmentAlreadyScored[pos].resize( m_maxLength+1 );

    for (int length=1; length<=m_maxLength; length++){
      m_segmentScores[pos][length].resize( m_maxCardinality + 1 );
      m_segmentAlreadyScored[pos][length].resize( m_maxCardinality + 1 );

      for (int cardinality=1; cardinality<=m_maxCardinality; cardinality++){
	m_segmentScores[pos][length][cardinality] = 0.0;
	m_segmentAlreadyScored[pos][length][cardinality] = false;
      }
    }
  }

}

void SegmentScores::setScore( int startpos, int length, int cardinality, double score ){
  m_segmentScores[startpos][length][cardinality] = score;
  m_segmentAlreadyScored[startpos][length][cardinality] = true;

#ifndef LPind
  printf( "SCORES( %d:%d (length = %d), (card = %d) ) = %f\n", startpos, (startpos+length-1), length, cardinality, score );
#endif
}

int SegmentScores::readFromFile( char *filename ){

  FILE *f = fopen( filename, "r" );
  int error = 0;
  int returnVal;
  int startpos, length, cardinality;
  float score;

  if( f == NULL ){
    error = 1;
    printf( "ERROR opening '%s'\n", filename );
  }

  fscanf( f, "%d %d %d %f", &startpos, &length, &cardinality, &score );

  while( (returnVal = fscanf( f, "%d %d %d %f", &startpos, &length, &cardinality, &score )) == 4 ){
    assert( !m_segmentAlreadyScored[startpos][length][cardinality] );
    m_segmentScores[startpos][length][cardinality] = score;
    m_segmentAlreadyScored[startpos][length][cardinality] = true;
  }
  
  if( returnVal != EOF ){
    printf( "ERROR reading '%s'\n", filename );
    error = 1;
    init_scores();
  }

  return error;
}

int SegmentScores::writeToFile( char *filename ){

  FILE *f = fopen( filename, "w" );
  int error = 0;

  if( f == NULL ){
    error = 1;
    printf( "ERROR opening '%s'\n", filename );
  }

  for (int pos=1; pos<= m_numPositions; pos++){
    for (int length=1; length<=m_maxLength; length++){
      for (int cardinality=1; cardinality<=m_maxCardinality; cardinality++){
	if( m_segmentAlreadyScored[pos][length][cardinality] ){
	  fprintf( f, "%d %d %d %f\n", pos, length, cardinality, m_segmentScores[pos][length][cardinality]  );
	}
      }
    }
  }

  fclose( f );

  return error;
}
