#include "stdafx.h"
//#include <stddef.h>
//#include <stdlib.h>
//#include <conio.h>
//#include <windows.h>
//#include <afx.h>
//#include <afxwin.h>
//#include <afxmt.h>
//#include "process.h"
#include "MixMod.h"
#include "TuneSets.h"
#include "math.h" 

int main(int argc, char* argv[]);

class TB{
public:
  int mixmodLength;
  int startpos, endpos;
  int numComp;
  TB();
  void set( int _startpos, int _endpos, int _numComp );
};

TB::TB(){
  mixmodLength = -1;
  numComp = -1;
  //printf( "TB c'tor\n" );
}

void TB::set( int _startpos, int _endpos, int _numComp ){
  this->startpos = _startpos;
  this->endpos = _endpos;
  this->mixmodLength = endpos - startpos + 1;
  this->numComp = _numComp;
}

// ********* START GLOBALS ***********

void score(int argc, char* argv[], int argIdx);
void traceback(int argc, char* argv[], int argIdx);
//void score_segments(int tid);
void score_segments(int tid, int initial_endpos, int final_endpos, FILE *SCORES_FILE);
void init_SCORES();
void score_one_interval(int argc, char *argv[], int argIdx);

#ifdef WINDOWS
CRITICAL_SECTION cs; 
#endif

#ifdef WINDOWS
const int NUM_THREADS = 2;
#else
const int NUM_THREADS = 1;
#endif

MixMod **mm = new MixMod*[NUM_THREADS];

int thread_idx = 0;

// the alignment
int **a = 0;  

// the number of sequences in the alignment
int ALIGN_N = 0;

// the length of the alignment
int ALIGN_L = 0;

// the segment scores;
double SCORES[MAX_SEQ_LENGTH+1][MAX_MIX_MODEL_LENGTH+1][MAX_COMPONENTS+1];

// the least upper bounds for the segment scores
double LUB[MAX_SEQ_LENGTH][MAX_MIX_MODEL_LENGTH][MAX_COMPONENTS];

// a small number
double SMALL = (double)-1e30;

// the name of the alignment file 
char *alignment_name = 0;

// the name of the fold 
char *cv_fold_name = 0;

// the indexes of the training sequences
int *training_seqs_IDs = 0;

// the number of training sequences
int num_training_seqs = -1;

// the tuning sets
TuneSets *tuneSets = NULL;

// entropies of the columns (of the training set)
double H[MAX_SEQ_LENGTH];  // the entropies

bool custom = false;

int maxMMLength = MAX_MIX_MODEL_LENGTH;

int maxNumComp = MAX_COMPONENTS;

int number = 0;

int tmp = 0;

// ********* END GLOBALS ***********

#ifdef WINDOWS
UINT thread_proc( LPVOID pParam ){
  
  int thread_id;

  EnterCriticalSection(&cs);
  thread_id = thread_idx++;
  LeaveCriticalSection(&cs);

  mm[thread_id] = new MixMod();

  HANDLE *pEvent = (HANDLE*)pParam;

  score_segments(thread_id);

  /*
  for (int i=0; i<100; i++){
    fprintf( STDERR, "number = %d, id = %d\n", number, thread_id);
    //fprintf( STDERR, "number = %d\n", number );
    number++;
  }

  fprintf( STDERR, "About to set the event\n" );
  */

  SetEvent(*pEvent);

  return 0;
}
#endif

/**
 * read an alignment from a file
 */
void read_alignmnt( ){
  char filename[500];

#ifdef WINDOWS
  sprintf( filename, "%s\\%s\\%s", ROOT_DIR, alignment_name, ALIGNMENT_FILE_NAME );
#else
  sprintf( filename, "%s/%s/%s", ROOT_DIR, alignment_name, ALIGNMENT_FILE_NAME );
#endif

  //fprintf( STDERR, "filename = %s\n", filename );

  FILE *F;

  F = fopen( filename, "r" );
  
  if( F == NULL ){
    printf( "cannot open %s\n", filename );
    exit(1);
  }
 
  fscanf( F, "%d %d", &ALIGN_N, &ALIGN_L );

  //printf( "%d rows and %d columns\n", ALIGN_N, ALIGN_L );

  a = new int*[ALIGN_N];

  for( int row = 0; row < ALIGN_N; row++ ){
	  a[row] = new int[ALIGN_L];
  }

  for( int row = 0; row < ALIGN_N; row++ ){
	  for( int col = 0; col < ALIGN_L; col++ ){
		  fscanf( F, "%d", &a[row][col] );
		  a[row][col]--; // subtract 1 because alignment file is in MATLAB form
		 // printf( "a(%d,%d) = %d\n", row, col, a[row][col] );
	  }
  }

  //fprintf( STDERR, "alignment read\n");
  fclose( F );
}


int read_folds_file(){
  int retVal = 0;

  char filename[500];
  const int bufferSize = 1024;
  char buffer[bufferSize];
  bool keep_going = true;
  bool found_fold = false;

  sprintf( filename, "%s\\%s\\%s", ROOT_DIR, alignment_name, FOLDS_FILE_NAME );

  FILE *F;

  F = fopen( filename, "r" );
  
  if( F == NULL ){
    printf( "cannot open %s\n", filename );
    exit(1);
  }

  do{
    char *r = fgets( buffer, bufferSize, F );
    
    if( r == NULL ){
      keep_going = false;
    }
    else{

      //char *strtok(  char *strToken,  const char *strDelimit ;
      //sscanf( buffer, "%s", &fold_name );

      char *fold_n = strtok( buffer, " " );

      if( strcmp( fold_n, cv_fold_name ) == 0 ){
	//fprintf( STDERR, "found fold '%s'\n", cv_fold_name );
	keep_going = false;

	training_seqs_IDs = new int[ALIGN_N];
	num_training_seqs = 0;
	char *tok = "1";

	while( tok != NULL ){
	  tok = strtok( NULL, " " );

	  if( tok != NULL ){
	    training_seqs_IDs[ num_training_seqs ] = atoi(tok);

	    //  fprintf( STDERR, "ID(%d) = %d\n", num_training_seqs, training_seqs_IDs[num_training_seqs] ); 
	    num_training_seqs++;
	  }
	  else{
	    fprintf( STDERR, "It is NULL\n" );
	  }
	}

	//fprintf( STDERR, "fold has %d traingin sequences\n", num_training_seqs );
      }

    }

  }while( keep_going );
 

  if( found_fold ) retVal = 0;

  fclose( F );

  // fprintf( STDERR, "fold file read\n" );

  return retVal;
}

double score_interval_mdl( int startpos, int mixmodLength, int numComponents, int num_restarts, int tid ){

  int endpos = startpos + mixmodLength - 1;

  int cntGoodRows = 0;
  int goodRows[ALIGN_N];

  for( int i = 0; i < ALIGN_N; i++ ){
    for( int pos = startpos; pos <= endpos; pos++ ){
      if( a[i][startpos] >= 0 ){
	goodRows[cntGoodRows++] = i;
	break;
      }
    }
  }

  double logMLPointLikelihood;

  double bestScore = -BIG_NUMBER;

  double d = (numComponents-1) + numComponents * mixmodLength * (ALPHABET_SIZE -1);

  for( int restart_id = 0; restart_id < num_restarts; restart_id++ ){

    //
    // train the mixture model using EM
    //
    mm[tid]->init( a, cntGoodRows, goodRows, mixmodLength, numComponents, startpos );
    logMLPointLikelihood = mm[tid]->train( a, cntGoodRows, goodRows, MAX_EM_ITERATIONS );

    double score = logMLPointLikelihood - (d/2 * log( cntGoodRows ));

    //fprintf( STDERR, "score %d %d %d restart %d = %f (%f)\n", startpos, endpos, numComponents, restart_id, logMLPointLikelihood, score );

    if( score > bestScore ){
      bestScore = score;
    }

  }

  return bestScore;
}


double score_interval_cheeseman( int startpos, int mixmodLength, int numComponents, int num_restarts, int tid ){

  int endpos = startpos + mixmodLength - 1;

  int cntGoodRows = 0;
  int goodRows[ALIGN_N];

  for( int i = 0; i < ALIGN_N; i++ ){
    for( int pos = startpos; pos <= endpos; pos++ ){
      if( a[i][startpos] >= 0 ){
	goodRows[cntGoodRows++] = i;
	break;
      }
    }
  }

  double logMLPointLikelihood, logMLPointLikelihoodCompleteData, logMargLikelihoodCompleteData, score;

  double bestScore = -BIG_NUMBER;

  for( int restart_id = 0; restart_id < num_restarts; restart_id++ ){

    //
    // train the mixture model using EM
    //
    mm[tid]->init( a, cntGoodRows, goodRows, mixmodLength, numComponents, startpos );
    logMLPointLikelihood = mm[tid]->train( a, cntGoodRows, goodRows, MAX_EM_ITERATIONS );

    /*  change counts for debugging
    for( int component_id = 0; component_id < mm[tid]->m_numComponents; component_id++ ){
      mm[tid]->m_componentCounts[component_id] *= 1;
      
      for( int pos = 0; pos < mm[tid]->m_mixmodLength; pos++ ){
	for (int symbol_id=0; symbol_id<ALPHABET_SIZE; symbol_id++){
	  mm[tid]->m_E_counts[component_id][pos][symbol_id] *= 1;
	}
      }
    }
    */

    
    //mm[tid]->dumpCounts();
    //mm[tid]->dumpDists();
    
    logMLPointLikelihoodCompleteData = mm[tid]->getLogOfPointLikelihoodOfCompleteData();
    
    logMargLikelihoodCompleteData = mm[tid]->getLogOfMarginalLikelihoodOfCompleteData();

    score = logMLPointLikelihood + logMargLikelihoodCompleteData - logMLPointLikelihoodCompleteData;

    //fprintf( STDERR, "score = (%.1f) + (%.1f) - (%.1f) = %f.1\n", 
    //     logMLPointLikelihood, logMargLikelihoodCompleteData, logMLPointLikelihoodCompleteData, score );

    if( score > bestScore ){
      bestScore = score;
    }

  }

  return bestScore;
}
  
double score_interval( int startpos, int mixmodLength, int numComponents, int num_restarts, int tid ){

  int endpos = startpos + mixmodLength - 1;
  double totalLogLike = 0;

  //fprintf( STDERR, "0\n" );

  // if the length is 1, do a full leave-one-out cross validation
  if( mixmodLength == 1 ){
    //fprintf( STDERR, "1\n" );
    int trainSeqs[MAX_SEQS];
    int numTrainSeqs = num_training_seqs-1;

    for (int leftOutIdx=0; leftOutIdx<num_training_seqs; leftOutIdx++){
      if( a[ training_seqs_IDs[leftOutIdx] ][ startpos ] >= 0 ){
	for( int i = 0; i < numTrainSeqs; i++ ){
	  trainSeqs[i] = training_seqs_IDs[ ((i < leftOutIdx) ? i : i+1) ];
	}

	//if( startpos == 2 ){
	// fprintf( STDERR, "start pos is 1\n" );
	//}
	//fprintf( STDERR, "1.1\n" );
	
	mm[tid]->init( a, numTrainSeqs, trainSeqs, 1, 1, startpos );
	mm[tid]->train( a, numTrainSeqs, trainSeqs, 1 );
	//mm[tid]->dumpDists();

	//fprintf( STDERR, "1.2\n" );

	double loglike = mm[tid]->parseSeq( a, training_seqs_IDs[leftOutIdx] );
	
	//fprintf( STDERR, "loglike of testseq %d = %f\n", testSeqIdx, loglike );
	totalLogLike += loglike;

	//fprintf( STDERR, "1.3\n" );
	
	//fprintf( STDERR, "%f %f\n", loglike, totalLogLike );
      }
    }
    //fprintf( STDERR, "2\n" );
  }
  else{ // otherwise use the tuning sets...
    //fprintf( STDERR, "3\n" );
    //
    // the score of an interval is the cross-validated log likelihood
    // 
    for( int tuneFold = 0; tuneFold < tuneSets->m_numFolds; tuneFold++ ){
      //fprintf( STDERR, "On TUNE fold %d, num restarts is %d\n", tuneFold, num_restarts );

      // get a mixture model by training with EM using multiple restarts
      double bestScore = -BIG_NUMBER;

      for( int restart_idx = 0; restart_idx < num_restarts; restart_idx++ ){
	mm[tid]->init( a, 
		       tuneSets->m_numTrainingSeqsInFold[tuneFold], 
		       tuneSets->m_trainingSeqs[tuneFold], 
		       mixmodLength, numComponents, startpos );	

	//fprintf( STDERR, "WARNING HARDCODING Emission distribution\n" ); mm->hardcode();

	//if( startpos == 2 && endpos == 3 ){
	// fprintf( STDERR, "YO" );
	//}

	double loglike = mm[tid]->train( a, 
					 tuneSets->m_numTrainingSeqsInFold[tuneFold], 
					 tuneSets->m_trainingSeqs[tuneFold],
					 MAX_EM_ITERATIONS );

	//mm[tid]->dumpDists();

	//if( custom ) fprintf( STDERR, "log like fold %d, restart %d = %f\n", tuneFold, restart_idx, loglike );

	if( loglike > bestScore ){
	  bestScore = loglike;
	  mm[tid]->cacheProbabilties();
	}
	
	//exit(1);
      }

      //fprintf( STDERR, "max loglike = %f\n", bestScore );

      mm[tid]->setProbabiltiesFromCache();

      //if( startpos == 3 && endpos == 4 ){
      //	fprintf( STDERR, "\tbest LL of fold is %f\n", bestScore );
      //	mm->dumpDists();
      //}

      for( int testSeqIdx = 0; testSeqIdx < tuneSets->m_numTestSeqsInFold[tuneFold]; testSeqIdx++ ){
	
	double loglike = mm[tid]->parseSeq( a, tuneSets->m_testSeqs[tuneFold][testSeqIdx] );
	
	//fprintf( STDERR, "loglike of testseq %d = %f\n", testSeqIdx, loglike );
	
	totalLogLike += loglike;
      }

    }
  }

  //fprintf( STDERR, "SCORE of (%d:%d), %d components is %.12f\n", (startpos), (endpos), numComponents, totalLogLike );

  //if( startpos == 2 && mixmodLength == 2 && numComponents == 2 ) exit(0);

  return totalLogLike;
}

/**
 * load the scores from a file.
 *
 * Only those values elements that are in the scores file are overwritten
 * 
 */
void load_scores_from_file( char *relative_file, double lambda ){
  char filepath[256];

  sprintf( filepath, "%s/%s/%s", ROOT_DIR, alignment_name, relative_file );
  fprintf( STDERR, "loading scores from '%s'\n", filepath );

  FILE *F = fopen( filepath, "r" );
  const int bufferSize = 80;
  char buffer[bufferSize];

  for( char *r = fgets( buffer, bufferSize, F ); r != NULL; r = fgets( buffer, bufferSize, F ) ){
  
    int startpos = atoi( strtok( buffer, " " ) );
    int mixmodLen = atoi( strtok( NULL, " " ) );
    int numComponents = atoi( strtok( NULL, " " ) );
    double score = atof(  strtok( NULL, " " ) );

    //SCORES[startpos-1][mixmodLen-1][numComponents-1] = score;
    if( mixmodLen > 1 ) SCORES[startpos][mixmodLen][numComponents] = score - lambda;
    else SCORES[startpos][mixmodLen][numComponents] = score;

    fprintf( STDERR, "scores(%d,%d,%d) = %f %f\n", startpos,  mixmodLen, numComponents, score, SCORES[startpos][mixmodLen][numComponents] );
  }
}

/**
 * 1) compute the score of all segments whose length is allowed.
 */
void score_segments(int tid, int initial_endpos, int final_endpos, FILE *SCORES_FILE){

  int lub_skip = 0;
  int use_cheesman = 0;
  int use_mdl = 1;

  // compute the scores for length 1 segments
  for( int pos = 0; pos < ALIGN_L; pos++ ){

    if( pos >= initial_endpos && pos <= final_endpos ){

      // 
      // in a thread safe way, check to see if this interval needs to be scored
      //
      if( (pos % NUM_THREADS) == tid ){
	double score;

	if( use_cheesman ){
	  score = score_interval_cheeseman(pos,1,1,1, tid);
	}
	else if( use_mdl ){
	  score = score_interval_mdl(pos,1,1,1, tid);
	}
	else{
	  score = score_interval(pos,1,1,1, tid);
	}

	SCORES[pos][0][0] = score;
#ifdef WINDOWS
	EnterCriticalSection(&cs);
#endif
	fprintf( SCORES_FILE, "%d %d %d %.20f\n", (pos+1), 1,1, score );
#ifdef WINDOWS
	LeaveCriticalSection(&cs);
#endif
      }
    }
  }

  fprintf( STDERR, "Done with length 1 sequences\n" );
    
  //
  // score non-singleton patches 
  //
  for( int endpos = 1; endpos < ALIGN_L; endpos++ ){
    if( endpos >= initial_endpos && endpos <= final_endpos ){
      fprintf( STDERR, "endpos = %d\n", endpos );
      if( endpos % 100 == 0 ){
	fprintf( STDERR, "endpos = %d\n", endpos );
      }

      if( (endpos % NUM_THREADS) == tid ){
	fprintf( STDERR, "SCORING endpos %d on thread %d\n", endpos, tid );

	for( int mixmodLen = 2, startpos=endpos-1; 
	     mixmodLen < maxMMLength && startpos>=0; 
	     mixmodLen++, startpos-- ){
	
	  //fprintf( STDERR, "Scoring(%d,%d) with %d components\n", startpos, endpos, mixmodLen );
	  //fprintf( STDERR, "H(start) = %f, H(end) = %f\n", H[startpos], H[endpos] );
	
	  if( H[startpos] == 0 || H[endpos] == 0 ){
	    // skip because segment begins or ends with a conserved column
	    for( int numComponents = 2; numComponents <= maxNumComp; numComponents++ ){
	      SCORES[startpos][mixmodLen-1][numComponents-1] = -71234784;
	    }
	  }
	  else{
	  
	    for( int numComponents = 2; numComponents <= maxNumComp; numComponents++ ){
	    
	      double score;

	      if( use_cheesman ){
		score = score_interval_cheeseman(startpos,mixmodLen,numComponents,NUM_RESTARTS,tid);
	      }
	      else if( use_mdl ){
		score = score_interval_mdl(startpos,mixmodLen,numComponents,NUM_RESTARTS,tid);
	      }
	      else{
		score = score_interval(startpos,mixmodLen,numComponents,NUM_RESTARTS,tid);
	      }
	    
	      // HACK: penalty for more values;
	      score -= numComponents*0.01;
	      //score -= numComponents*1;
	    
	      // 
	      // set the score in the "master" SCORES array
	      //
	      SCORES[startpos][mixmodLen-1][numComponents-1] = score;

#ifdef WINDOWS	    
	      EnterCriticalSection(&cs);
#endif
	      fprintf( SCORES_FILE, "%d %d %d %.20f\n", (startpos+1), mixmodLen, numComponents, score );
	      //fprintf( STDERR, "%d %d %d %.20f\n", (startpos+1), mixmodLen, numComponents, score );
#ifdef WINDOWS
	      LeaveCriticalSection(&cs);
#endif
	      //fprintf( STDERR, "%d %d %d %.20f\n", (startpos+1), mixmodLen, numComponents, score );
	    }

	    //fprintf( STDERR, "%d %d %d %.20f\n", (startpos+1), mixmodLen, numComponents, score );
	  }
	}
      }
    }
  }
  fprintf( STDERR, "DONE scoring\n" );
}

/**
 *
 */
void usage(){
  printf( "score_patches <alignment_name> OPTIONS\n" );

  printf( "  <fold_id> -score <scores_file> \n" );
  printf( "  <fold_id> -score <scores_file> -range <start> <end>\n" );
}

/*
 * score_patches <alignment_name> <training set source> <action>  [options... ]
 *
 * Examples:
 *
 * score_patches v2c_oct2 all -score all_scores.txt 
 * score_patches v2c_oct2 all -score scores1.txt -range 1 100
 * score_patches v2c_oct2 all -traceback tb.txt models.txt scores.txt
 */
int main(int argc, char* argv[]){

#ifdef WINDOWS
  /* Initialize the critical section -- This must be done before locking */
  InitializeCriticalSection(&cs);
#endif

  if( argc == 1 ){
    usage();
    exit(0);
  }

  int argIdx = 1;
  alignment_name = argv[argIdx++];  
  char *cv_fold_name = argv[argIdx++];
  char *action       = argv[argIdx++];

  //
  // *** read the alignment from "<alignment_name>/alignment.txt"
  //
  read_alignmnt();

  //int i;  fprintf( STDERR, "alignment read\n" ); scanf( "%d", &i );;

  //
  // set the training sequences based on the fold identifier and the folds.txt file (or all)
  //
  if( strcmp(cv_fold_name, "all") == 0 ){
    //fprintf( STDERR, "training on ALL sequences" );
    num_training_seqs = ALIGN_N;
    training_seqs_IDs = new int[ALIGN_N];
    for (int i=0; i<ALIGN_N; i++){
      training_seqs_IDs[i] = i;      
    }
  }
  else{
    assert( false ); // NOT (re)implemented yet
    fprintf( STDERR, "reading folds file\n" );
    read_folds_file();
  }


  //
  // not sure what this is doing
  //
  tuneSets = new TuneSets();

  //  fprintf( STDERR, "tune sets done\n" ); scanf( "%d", &i );;

  //
  // Compute the entropy
  //
  for (int column=0; column<ALIGN_L; column++){
    //fprintf( STDERR, "Getting column %d entropy\n", column );
    H[column] = Utils::columnEntropy( a, training_seqs_IDs, num_training_seqs, column );
  }


  if( strcmp( action, "-score" ) == 0){
    score( argc, argv, argIdx );
  }
  else if( strcmp( action, "-traceback" ) == 0 ){
    traceback( argc, argv, argIdx );
    //fprintf( STDERR, "DONE with traceback\n" );
  }
  else if( strcmp( action, "-scoreone" ) == 0 ){
    score_one_interval( argc, argv, argIdx );
  }
  else if( strcmp( action, "-interact" ) == 0 ){
    int startpos = 0, endpos = 0, num_comp = 0, num_restarts = 0;
    mm[0] = new MixMod();
    while(1){
      fprintf( STDERR, "startpos: " );
      scanf( "%d", &startpos );
      fprintf( STDERR, "endpos: " );
      scanf( "%d", &endpos );
      fprintf( STDERR, "num comp: " );
      scanf( "%d", &num_comp );
      fprintf( STDERR, "num restarts: " );
      scanf( "%d", &num_restarts );
      //double score = score_interval(startpos-1, endpos-startpos+1, num_comp, num_restarts, 0 );

      //double score = score_interval_cheeseman(startpos-1, endpos-startpos+1, num_comp, num_restarts, 0 );
      double score = score_interval(startpos-1, endpos-startpos+1, num_comp, num_restarts, 0 );

      fprintf( STDERR, "   SCORE = %f\n\n", score ); 
    }
  }
  else{
    fprintf( STDERR, "BAD ACTION %s\n", action );
  }
  
  return 0;
}

/**
 * SCORE some subset (or all) of the intervals
 * 
 * <score_filename> <training_set_source>
 *
 * The arguments indicate
 *  1) the name of the file to write the scores to
 *  
 *  and optionally,
 *
 *  2) the maximum number of components  (-mc <max components>)
 *  3) the initial and final end pos     (-range <initial_endpos> <final_endpos>)
 *  4) the seed for the RNG              (-seed <seed>) 
 * 
 */
void score(int argc, char* argv[], int argIdx){
  FILE *SCORES_FILE;  // file to save the scores to 

  // provides the range of positions to score
  int initial_endpos =1, final_endpos = ALIGN_L;

  // the seed of the RNG
  int seed = 77700;

  fprintf( STDERR, "BEGIN score()\n" );

  char scores_file_name[255];

  char *rel_name = argv[argIdx++];

  sprintf( scores_file_name, "%s/%s/%s", ROOT_DIR, alignment_name, rel_name );

  while( argIdx < argc ){

    if( strcmp( argv[ argIdx++ ], "-range" ) == 0){
      initial_endpos = atoi(argv[ argIdx++ ]);
      final_endpos = atoi(argv[ argIdx++ ]);
    }

    // change the seed of the random number generator.
    if( strcmp( argv[argIdx++], "-seed" ) == 0 ){  
      seed = atoi( argv[argIdx++] ); 
    }

    // set the number of components
    if( strcmp( argv[argIdx++], "-mc" ) == 0 ){
      maxNumComp = atoi( argv[argIdx++] );
      assert( maxNumComp <= MAX_COMPONENTS );
    }

  }

  //
  // open the SCORES file for writing
  //
  if( Utils::file_exists( scores_file_name ) ){
    fprintf( STDERR, "%s exists\n", scores_file_name );
    exit(0);
  }
  SCORES_FILE = fopen( scores_file_name, "w" );
  if( SCORES_FILE == NULL ){
    printf( "cannot open %s\n", scores_file_name );
    exit(1);
  }

  //
  // Seed the RNG
  //
  fprintf( STDERR, "seeding RNG with %d\n", seed );
  srand( seed );

  //
  // do everything on a single thread
  //
  mm[0] = new MixMod();

  fprintf( STDERR, "Initial endpos = %d, final endpos = %d\n", initial_endpos, final_endpos );
  score_segments(0,initial_endpos, final_endpos, SCORES_FILE);
  fprintf( STDERR, "DONE SCORING!\n" );

  fclose( SCORES_FILE );
}

void init_SCORES(){

  for( int i=0; i<=ALIGN_L;i++ ){
    for (int j=0; j<=MAX_MIX_MODEL_LENGTH; j++){
      for (int k=0; k<=MAX_COMPONENTS; k++){
	SCORES[i][j][k] = -BIG_NUMBER;
      }
    }
  }
}

/**
 * 1) LOAD SCORES from one or more files
 * 2) Find viterbi path
 * 3) find best segmentation
 * 4) train the mixture models
 */
void traceback(int argc, char *argv[], int argIdx){
  fprintf( STDERR, "BEGIN traceback()\n" );

  // the viterbi scores for patial alignments
  double V[MAX_SEQ_LENGTH];
 
  // traceback structures
  TB M[MAX_SEQ_LENGTH];

  FILE *TBFILE;

  FILE *MODELS_FILE;

  char *tb_rel_filename = argv[argIdx++];
  char *models_rel_filename = argv[argIdx++];

  char tb_filename[255];
  char models_filename[255];

  mm[0] = new MixMod();

  sprintf( tb_filename, "%s/%s/%s", ROOT_DIR, alignment_name, tb_rel_filename );
  sprintf( models_filename, "%s/%s/%s", ROOT_DIR, alignment_name, models_rel_filename );

  fprintf( STDERR,"loading scores from file\n" );
  init_SCORES();

  fprintf( STDERR, "SCORES(3,4,2) = %f\n", SCORES[4][2][4] );

  assert( argIdx < argc );
  while( argIdx < argc ){ load_scores_from_file( argv[argIdx++], 0); }
  //int i;  fprintf( STDERR,"DONE\nm" );scanf( "%d", &i );
  //exit(0);

  //
  // find the viterbi path
  //
  V[1] = SCORES[1][1][1];   M[1].set(1, 1, 1);
  if( V[1] == -BIG_NUMBER ) V[1] = 0.0; // hack to correct for the case where column 1 is conserved...
  
  fprintf( STDERR, "V[1] = %f\n", V[1] );

  for( int endpos = 2; endpos <= ALIGN_L; endpos++ ){
    //fprintf( STDERR, "endpos = %d\n", endpos );

    V[endpos] = V[endpos-1] + SCORES[endpos][1][1];   M[endpos].set(endpos, endpos, 1);
    fprintf( STDERR, "Default V[%d] = %f + %f = %f\n", endpos, V[endpos-1], SCORES[endpos][1][1], V[endpos] );

    for( int mixmodLen = 2, startpos=endpos-1; 
	 mixmodLen <= maxMMLength && startpos>=1; 
	 mixmodLen++, startpos-- ){

      if( H[startpos-1] == 0 || H[endpos-1] == 0 ){ // H uses start from zero indexing... (what a mess!)
      	//fprintf( STDERR, "skipping (%d:%d)\n", startpos, endpos ); 
      }
      else if( H[startpos-1] < 0.3228 || H[endpos-1] < 0.3228 ){
	// don't end with columns where there entropy is less than H(1/17, 16/17)
      }
      else{
	// viterbi value coming "in" to this segment...
	double vInVal = (startpos>0) ? V[startpos-1] : 0; 
	
	for( int numComponents = 2; numComponents <= maxNumComp; numComponents++ ){
	  double score = SCORES[startpos][mixmodLen][numComponents]; // the score for the segmnet

	  if( score != -BIG_NUMBER ){
	    //fprintf( STDERR, "SCORE for %d:%d, %d = %3.3f + %3.3f = %3.3f\n", startpos, endpos, numComponents, vInVal, score, (vInVal + score) );
	  }

	  if( vInVal + score > V[endpos] ){
	    V[endpos] = vInVal + score;
	    M[endpos].set(startpos, endpos,numComponents);
	  }
	}
      }
    }

    fprintf( STDERR, "V[%d] = %f  ", endpos, V[endpos] );
    fprintf( STDERR, "\t %d:%d, %d components\n", M[endpos].startpos, M[endpos].endpos, M[endpos].numComp );
    fprintf( STDERR, "\n" );

    //if( endpos == 693 ) exit(0);
  }

  for( int _endpos = 1; _endpos <= ALIGN_L; _endpos++ ){
    fprintf( STDERR, "V[%d] = %f  ", _endpos, V[_endpos] );
    fprintf( STDERR, "\t %d:%d, %d components\n", M[_endpos].startpos, M[_endpos].endpos, M[_endpos].numComp );
  }
  
  TBFILE = fopen( tb_filename, "w" );
    
  int pos = ALIGN_L;
    
  TB **models = new TB*[ALIGN_L];
  int segment_cnt = 0;
    
  while( pos > 0 ){
    models[ segment_cnt++ ] = &M[pos];
      
    fprintf( STDERR, "at %d, %d:%d %d components\n", pos, M[pos].startpos, M[pos].endpos, M[pos].numComp );
    pos = pos - M[pos].mixmodLength;

  }
    
  for( int i = segment_cnt-1; i>=0; i-- ){
    fprintf( STDERR, "%d %d %d\n", (models[i]->startpos), (models[i]->endpos), models[i]->numComp );
    fprintf( TBFILE, "%d %d %d\n", (models[i]->startpos), (models[i]->endpos), models[i]->numComp );
    //fprintf( STDERR, "%d %d %d\n", (models[i]->startpos+1), (models[i]->endpos+1), models[i]->mixmodLength );
    //fprintf( TBFILE, "%d %d %d\n", (models[i]->startpos+1), (models[i]->endpos+1), models[i]->mixmodLength );
  }
  fclose(TBFILE);
  fprintf( STDERR, "saved TB to '%s'\n", tb_filename );
  fprintf( STDERR, "Time to train\n" );

  //
  // train the mixture models on the viterbi path
  //
  fprintf( STDERR, "opening %s\n", tb_filename );
  TBFILE = fopen( tb_filename, "r" );
  assert( TBFILE != 0 );
  MODELS_FILE = fopen( models_filename, "w" );
  assert( MODELS_FILE != 0 );

  int keep_going = 1;

  int bufferSize = 255;
  char buffer[bufferSize];

  //fprintf( STDERR, "1\nn" );
  
  while( keep_going ){
    //fprintf( STDERR, "TOP\n" );
    char *r = fgets( buffer, bufferSize, TBFILE );
    //    fprintf( STDERR, "2\n" );
    //fprintf( STDERR, "read %s\n", r );

    if( r == NULL ){ keep_going = false; }
    else{
      //fprintf( STDERR, "3\n" );

      int startpos = atoi( strtok( buffer, " " ) );
      int endpos =  atoi( strtok( NULL, " " ) );
      int numComp = atoi( strtok( NULL, " " ) );
      int mixmodLength = endpos - startpos + 1;

      //fprintf( STDERR, "4\n" );
      
      //fprintf( STDERR, "training mixture model for (%d:%d) with %d components\n", startpos, endpos, numComp );

      //double MixMod::rrTrain( int **a, int num_training_seqs, int *training_seqs, 
      //		int mixmodLength, int numComp, int startpos,
      //		int maxIterations, int num_restarts ){
      mm[0]->rrTrain(a, num_training_seqs, training_seqs_IDs, 
		     mixmodLength, numComp, startpos-1,
		     25, NUM_RESTARTS );
      
      mm[0]->init( a, num_training_seqs, training_seqs_IDs, mixmodLength, numComp, startpos-1 );
      //fprintf( STDERR, "5\n" );
      mm[0]->train( a, num_training_seqs, training_seqs_IDs, 25 );
      //      fprintf( STDERR, "6\n" );
      
      fprintf( MODELS_FILE, "%d %d %d\n", startpos, endpos, numComp );
      
      for (int comp_idx=0; comp_idx<numComp; comp_idx++){
	fprintf( MODELS_FILE, "%f ", mm[0]->m_componentProbs[comp_idx] );
      }
      fprintf( MODELS_FILE, "\n" );

      //fprintf( STDERR, "8\n" );
      
      for (int rel_pos=0; rel_pos<mixmodLength; rel_pos++){
	for (int comp_idx=0; comp_idx<numComp; comp_idx++){
	  for (int symbol=0; symbol<ALPHABET_SIZE; symbol++){
	    fprintf( MODELS_FILE, "%f ", mm[0]->m_E[comp_idx][rel_pos][symbol] );
	  }
	  fprintf( MODELS_FILE, "\n" );
	}	  
      }

      //fprintf( STDERR, "9\n" );

      //fprintf( STDERR, "5\nn" );
    }

    fflush( MODELS_FILE );
  }

  fprintf( STDERR, "XX\n" );
  
  fclose( TBFILE );
  fclose( MODELS_FILE );
}

void score_one_interval(int argc, char *argv[], int argIdx){

  int startpos = atoi( argv[argIdx++] ) - 1;
  int length = atoi( argv[argIdx++] );
  int numRestarts = 25;
  int tid = 0;

  mm[0] = new MixMod();

  while( argIdx < argc ){
    int num_components = atoi( argv[argIdx++] );

    double score = score_interval( startpos, length, num_components, numRestarts, 0 );

    fprintf( STDOUT, "%f ", score );
  }

  fprintf( STDOUT, "\n" );

}
    
