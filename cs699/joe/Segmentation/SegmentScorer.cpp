#include "SegmentScorer.h"
#include <iostream>

SegmentScorer::SegmentScorer( Alignment *pAlignment, int range_min_startpos, int range_max_startpos, 
			      int maxLength,
			      int maxCardinality, int numCVFolds, 
			      int maxIterations, int numRestarts ){
  m_pAlignment = pAlignment;
  m_range_min_startpos = range_min_startpos;
  m_range_max_startpos = range_max_startpos;
  m_maxIterations = maxIterations;
  m_numRestarts = numRestarts;
  m_maxLength = maxLength;
  m_maxCardinality = maxCardinality;

  m_pScores = new SegmentScores( m_pAlignment->m_NCOLS, maxLength, maxCardinality );

  m_pMMM = new MultinomialMixtureModel( maxCardinality, pAlignment->m_alphabet_size, maxLength, pAlignment->m_NROWS );

  if( numCVFolds == -1 ){     // don't set up any tuning sets
    m_pTuneSets = NULL;
  }
  else{
    m_pTuneSets = new TuneSets(pAlignment->m_NROWS, numCVFolds);

    m_pTestSetAlignments.resize(numCVFolds);
    m_pTrainingSetAlignments.resize(numCVFolds);

    for (int fold=0; fold<numCVFolds; fold++){
      m_pTestSetAlignments[fold] = m_pAlignment->getSubAlignment( m_pTuneSets->m_testSeqs[fold] );
      m_pTrainingSetAlignments[fold] = m_pAlignment->getSubAlignment( m_pTuneSets->m_trainingSeqs[fold] );
    }
  }
}

/**
 * score a segment using the cross validation score 
 */
double SegmentScorer::score_segment_CV( int startpos, int length, int cardinality ){
  double totalLogLike = 0;
  int numRestarts = (length == 1 ) ? 1 : m_numRestarts;

  //printf( "SCORING %d %d %d\n", startpos, length, cardinality );

  //
  // the score of an interval is the cross-validated log likelihood
  // 
  for( int tuneFold = 0; tuneFold < m_pTuneSets->m_numFolds; tuneFold++ ){
    // get a mixture model by training with EM using multiple restarts

    m_pMMM->randomRestartTrain( m_pTrainingSetAlignments[tuneFold],
				length, cardinality, startpos, m_maxIterations, numRestarts );

    // get the CV log likelihood for this fold
    for( int seqIdx = 0; seqIdx < m_pAlignment->m_NROWS; seqIdx++ )
    {
      std::cout << "var";
      std::cout << "(" << tuneFold+1 << ",";
	  std::cout << startpos << "," << length << ",";
	  std::cout << cardinality << ",";
      std::cout << seqIdx+1 << ",";
      std::cout << "1:" << cardinality << ")";
      
      std::cout << "=[";
      double loglike = m_pMMM->logLikelihoodOfSegment( m_pAlignment->m_data[seqIdx] );
      std::cout << "];";
      
      //printf("  %% %s", m_pTrainingSetAlignments[tuneFold]->m_seq_name[seqIdx]);
      std::cout << std::endl;

      //fprintf( STDERR, "loglike of testseq %d = %f\n", testSeqIdx, loglike );
      
      totalLogLike += loglike;
    }
  }
  
  //fprintf( STDERR, "SCORE of (%d:%d), %d components is %.12f\n", (startpos), (startpos+length-1), cardinality, totalLogLike );

  //if( startpos == 2 && length == 2 && cardinality == 2 ) exit(0);

  return totalLogLike;
}

/**
 * score all segments in the alignment that start in [m_range_max_startpos:m_range_max_startpos]
 */
void SegmentScorer::score_segments_in_range( ){
  double score;

  std::cout << "var=zeros([";
  std::cout << m_pTuneSets->m_numFolds << " ";
  std::cout << m_range_max_startpos << " ";
  std::cout << m_maxLength << " ";
  std::cout << m_maxCardinality << " ";
  std::cout << m_pAlignment->m_NROWS << " ";
  std::cout << m_maxCardinality;
  std::cout << "]);" << std::endl;
  
  for (int startpos=m_range_min_startpos; startpos<=m_range_max_startpos; startpos++){
    for (int length=1; length<=m_maxLength; length++){
      int endpos = startpos+length-1;

      if( endpos <= m_pAlignment->m_NCOLS ){
	
	for (int cardinality=1; cardinality<=m_maxCardinality; cardinality++){

	  if( m_pScores->m_segmentAlreadyScored[startpos][length][cardinality] ){
	    // Do nothing: segment already scored.
	  }
	  else{
	    if( cardinality == 1 || length > 1 ){ // don't compute score for length 1 segments w/ cardinality > 1
	      score = score_segment_CV( startpos, length, cardinality );
	      m_pScores->setScore( startpos, length, cardinality, score );
	    }
	  }
	}
      }
    }
  }

}




