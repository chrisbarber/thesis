#pragma once

#include <vector>
#include "TuneSets.h"
#include "Alignment.h"
#include "SegmentScores.h"
#include "MMM.h"

class SegmentScorer{

 public:

  Alignment *m_pAlignment;

  TuneSets *m_pTuneSets;

  std::vector< Alignment* > m_pTestSetAlignments;

  std::vector< Alignment* > m_pTrainingSetAlignments;

  SegmentScores *m_pScores;

  int m_range_min_startpos, m_range_max_startpos;

  int m_maxIterations;

  int m_numRestarts;

  int m_maxLength;

  int m_maxCardinality;

  MultinomialMixtureModel *m_pMMM;

 private:


 public:

  SegmentScorer( Alignment *pAlignment, int range_min_startpos, int range_max_startpos, 
		 int maxLength, int maxCardinality, int numCVFolds,
		 int maxIterations, int numRestarts );

  double score_segment_CV( int startpos, int length, int cardinality );

  void score_segments_in_range( );

};
