#pragma once

#define VERBOSE 1
//#define MAX_SEQ_LENGTH 4000
//#define MAX_MIX_MODEL_LENGTH 15
//#define MAX_CARDINALITY 3

//#define MAX_SEQS 500
//#define ALPHABET_SIZE 21
#define NUM_RESTARTS  10
//#define INIT_PSEUDO_COUNT 0.05
#define ROOT_DIR "/data/projects/fomm/data/alignments/"
#define STDERR stderr
#define STDOUT stdout
//#define ALIGNMENT_FILE_NAME "alignment.txt"
//#define FOLDS_FILE_NAME "folds.txt"
//#define MAX_EM_ITERATIONS 15
#define BIG_NUMBER 1e307
#define LOG2(x) (log(x) * 1.442695040888963)
#define MIN(x,y) ( ((x) < (y)) ? (x) : (y) )
#define MAX(x,y) ( ((x) > (y)) ? (x) : (y) )

// the alignment
extern int **a;

// the number of sequences in the alignment
extern int ALIGN_N;

// the length of the alignment-
extern int ALIGN_L;

// the segment scores;
extern double SCORES[MAX_SEQ_LENGTH+1][MAX_MIX_MODEL_LENGTH+1][MAX_CARDINALITY+1];

extern double SMALL;

extern char *alignment_name;

extern char *cv_fold_name;

extern int *training_seqs_IDs;

extern int num_training_seqs;

extern int maxMMLength, maxNumComp;

//extern TuneSets *tuneSets;



