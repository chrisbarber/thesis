#pragma once

#include <vector>

class TuneSets
{
public:

  int m_numFolds;

  std::vector<int> m_numTrainingSeqsInFold;
  std::vector<int> m_numTestSeqsInFold;
  
  std::vector<std::vector<int> > m_trainingSeqs;
  std::vector<std::vector<int> > m_testSeqs;

  TuneSets(int num_training_sequences, int numFolds);
  ~TuneSets(void);
};
