#pragma once

#include <vector>

class Alphabets
{
public:

  static int *getAminoAcidSymbolMap();
  static int *getAminoAcidSymbolMapWithGapCharacter();
  static int *getDNASymbolMap();
  static int *getDNASymbolMapWithGapCharacter();
  static int *getBinarySymbolMap();
  static int *getMatlabBinarySymbolMap();

};
