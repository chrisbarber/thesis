function parse = parse_seq_with_mix_model_use_logs( model_in, seq, compute_counts )

%
% This function occupies the most time during a run on FOMM.
% It is written in an attempt to make it is as efficient as possible
%

% get the positions in seq that are observed (non-zero) 
nz_positions = find( seq );

E_probs_used = ones( model_in.num_components, model_in.L );
for pos=nz_positions
  E_probs_used(:,pos) = model_in.E(:, pos, seq(pos));
end

%for pos=1:model_in.L
%  if( seq(pos) > 0 )
%    E_probs_used(:,pos) = model_in.E(:, pos, seq(pos));
%  else
%    E_probs_used(:,pos) = ones(1,model_in.num_components);
%  end
%end

%likelihood_by_comp = zeros(1, model_in.num_components )
for component_idx=1:model_in.num_components
  %likelihood_by_comp(component_idx) = model_in.component_probs(component_idx)*prod(E_probs_used(component_idx,:) );

  temp = E_probs_used( component_idx, : ); temp = temp( find(temp) );

  %loglike_by_comp( component_idx ) =  log( model_in.component_probs(component_idx) ) + sum(log(E_probs_used(component_idx,:)));

  %if( model_in.component_probs(component_idx) == 0 ) keyboard; end

  % fix the problem that we may be doing a log of zero... other than the warning, no harm becuase log(0) = -inf accoring to matlab
    
  loglike_by_comp( component_idx ) =  log( model_in.component_probs(component_idx) ) + sum(log(temp));
  
  %if( model_in.component_probs(component_idx) ) fprintf( 'here now\n' ); keyboard; end

  % check to avoid the log of zero warning
  %if( likelihood_by_comp( component_idx ) == 0 )
  %  parse.component_loglike(component_idx) = -inf;      
  %else
  %  parse.component_loglike(component_idx) = log( likelihood_by_comp( component_idx ) );
  %end
  
end

parse.loglike = -inf;
for comp_idx=1:model_in.num_components
  parse.loglike = my_add_logs( parse.loglike, loglike_by_comp(comp_idx) );
end

parse.comp_count = exp( loglike_by_comp - parse.loglike ); 
parse.E_counts = zeros( size(model_in.E) );

%test_E_counts = zeros( size(model_in.E) );

if( compute_counts )
  for pos=nz_positions
    parse.E_counts(:,pos,seq(pos)) = parse.comp_count;
  end
  %keyboard;
  %if( do_check ) assert(length( find( parse.E_counts - linear_index_parse.E_counts ) ) == 0); end
else
  parse.E_counts = [];
end


