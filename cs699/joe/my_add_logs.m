function sum = my_add_logs( logP, logQ )
% return log( exp(logP) + exp(logQ) )
%
% use a table lookup if needed to avoid underflow...
%

%exact = 0;
%if( exact )

% swap if neccessary
if( logQ > logP )
  tmp = logP;
  logP = logQ;
  logQ = tmp;
end

if( logQ == -inf ) 
  sum = logP; 
elseif( logQ > -745 ) % this is a hack to check for underflow
  
  % do the exact calculation if it is possible...
  tmp =  exp(logP) + exp(logQ) ;
  if( tmp == 0 ) sum = -inf;
  else sum = log( tmp );
  end
  return;
else % use the table
  %fprintf( 'using table\n' );
  global LOG_OF_1_PLUS_X
  

  %fprintf( 'computing exp(%f) + exp(%f)\n', logP, logQ );

  %
  % special case when one of the summands is log(-inf) 
  %
  diff = logQ - logP; % must not be greater that zero
  low_idx = ceil( diff / LOG_OF_1_PLUS_X.stepSize );
  if( diff == 0 ) 
    low_idx = 1;
  end
  %fprintf( 'diff %f, idx %d\n', diff, low_idx );
  if( low_idx >= length( LOG_OF_1_PLUS_X.values ) )
    sum = logP;
  else
    assert( diff <= LOG_OF_1_PLUS_X.xValues(low_idx) && diff >= LOG_OF_1_PLUS_X.xValues(low_idx + 1) );
    %low_idx
    %fprintf( 'logQ %f logP %f\n', logQ, logP );
    %fprintf( 'diff = %f, low_idx %d\n', diff, low_idx );
    %keyboard;
    sum = logP + linearly_interpolate( LOG_OF_1_PLUS_X.xValues(low_idx), LOG_OF_1_PLUS_X.values(low_idx), ...
                                       LOG_OF_1_PLUS_X.xValues(low_idx+1), LOG_OF_1_PLUS_X.values(low_idx+1), ...
                                       diff ); 
    
    % ( LOG_OF_1_PLUS_X.values(low_idx) + LOG_OF_1_PLUS_X.values(up_idx) ) / 2;
  end
end

%
% check against real value
%
if( 0 )
  fprintf( 'computing exp(%f) + exp(%f)\n', logP, logQ );

  exact_sum =  exp(logP) + exp(logQ);

  %if( sum == 0 && (x1 ~= -inf || x2 ~= -inf) )
   % error 'underflow';
  % end

  exact_sum = log(exact_sum); 

  fprintf( 'table = %f, exact = %f, diff = %f\n', sum, exact_sum, (sum-exact_sum) );
end

