function loglike = get_full_log_likelihood( fomm, seq )

loglike = 0;

for mm_idx = 1:length(fomm)
  parse = parse_seq_with_mix_model_use_logs( fomm(mm_idx), seq( fomm(mm_idx).positions ), 0 );
  loglike = loglike + parse.loglike;
end
