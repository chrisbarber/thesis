function make_log_table()

global LOG_OF_1_PLUS_X
LOG_OF_1_PLUS_X.stepSize = -0.01;
LOG_OF_1_PLUS_X.minValue = -10;
LOG_OF_1_PLUS_X.maxValue = 0;
LOG_OF_1_PLUS_X.xValues = [0:LOG_OF_1_PLUS_X.stepSize:-10];
LOG_OF_1_PLUS_X.values = zeros(1,length(LOG_OF_1_PLUS_X.xValues));

for i = 1:length(LOG_OF_1_PLUS_X.xValues)
  LOG_OF_1_PLUS_X.values(i) = log( 1 + exp(LOG_OF_1_PLUS_X.xValues(i)) );
end




