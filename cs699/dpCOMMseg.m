function [seg_COMM LL] = dpCOMMseg(D, config)

% read in configuration structure
num_sym=config.num_sym;
EMconverge=config.EMconverge;
numFolds=config.numFolds;
maxCard=config.maxCard;
numRestarts=config.numRestarts;
r_A=config.r_A;
r_P=config.r_P;
useFixedE=config.useFixedE;
if useFixedE
    fixedE_EMconverge=config.fixedE_EMconverge;
    fixedE_numRestarts=config.fixedE_numRestarts;
end

% find # of sequences and their length
[num_seq seq_len]=size(D);

% generate test folds to be used throughout
[folds testSetIndices trainingSetIndices]=CVfolds(num_seq,numFolds);

% varargin args for COMMEMCV/COMMEM
COMMEMargs={'numRestarts',numRestarts};
if useFixedE
    fixedEargs={'numRestarts',fixedE_numRestarts};
end

% holds entire segmentation at each position
opt=cell([1 seq_len]);
optLL=zeros([1 seq_len]);
if useFixedE
    fixedE_COMM=cell([1 seq_len]);
    LPind=cell([1 seq_len]);
end

% recurrence
csaved=0;
lsaved=0;
for i=1:seq_len
    for l=1:min([i config.max_seg_len])
        for c=1:maxCard
            % set up mixture models for candidate segment
            mm.startpos=i-l+1;
            mm.endpos=i;
            mm.L=l;
            mm.num_components=c;
            mm.E=repmat(r_P,[l num_sym c]);
            mm.positions=mm.startpos:mm.endpos;
            mm.component_probs=repmat(r_A,[c 1]);
            
            if 1==mm.startpos
                % can't have an arc so we only have 1 choice
                LL=COMMEMCV(D,mm,EMconverge,folds,COMMEMargs{:});
                focomm=mm;
            else
                % set up another with an arc
                arc_mm=mm;
                arc_mm.component_probs=repmat(r_A,[c opt{arc_mm.startpos-1}(end).num_components]);
                
                % now pair up arc_mm with the preceding MM
                pomm=[opt{arc_mm.startpos-1}(end) arc_mm];
                pomm(1).component_probs=pomm(1).component_probs(:,1);
                
                % now score these segmentations
                noarc_LL=FOCOMMEMCV(D,mm,EMconverge,folds,COMMEMargs{:});
                arc_LL=COMMEMCV(D,pomm,EMconverge,folds,COMMEMargs{:},'single_seg',2);
                
                % choose between the two (if same LL, prefer noarc)
                if arc_LL>noarc_LL
                    better_mm=arc_mm;
                    cand_seg_LL=arc_LL;
                else
                    better_mm=mm;
                    cand_seg_LL=noarc_LL;
                end
                
                % attach to opt segmentation
                focomm=[opt{better_mm.startpos-1} better_mm];
                
                % compute overall score
                LL=cand_seg_LL+optLL(better_mm.startpos-1);
            end
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            numChains=length(findFOCOMMchains(focomm));
            disp(sprintf('i=%d, c=%d, l=%d, #chains=%d, LL=%d, arc?=%d',i,c,l,numChains,LL,(l~=i && arc_LL>noarc_LL)));
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if (c==1 && l==1) || LL>best_LL
                best_LL=LL;
                best_focomm=focomm;
            end
        end
    end
    
    % this is the optimal model for position i
    opt{i}=best_focomm;
    
    % score this model
    
    if useFixedE
        % train new mm by itself to find LPind for each fold
        fixedE_COMM{i}=cell([1 numFolds]);
        LPind{i}=cell([1 numFolds]);
        for fold=1:numFolds
            temp_mm=opt{i}(end);
            temp_mm.component_probs=repmat(r_A,[temp_mm.num_components 1]);
            temp_mm.E(:)=r_P;
            temp_mm=MMEM(D(trainingSetIndices{fold},:),temp_mm,fixedE_EMconverge,fixedEargs{:});
            temp_mm_LPind=LPCOMMind(D(trainingSetIndices{fold},:),temp_mm);

            % set up temp_mm since it will actually be needed
            temp_mm.component_probs(:)=r_A;

            if length(opt{i})>1
                % add to initial fixedE model, and LPind
                fixedE_COMM{i}{fold}=[fixedE_COMM{temp_mm.startpos-1}{fold} temp_mm];
                LPind{i}{fold}=[LPind{temp_mm.startpos-1}{fold} temp_mm_LPind];
            else        
                fixedE_COMM{i}{fold}=temp_mm;
                LPind{i}{fold}=temp_mm_LPind;
            end
        end
        
        % score opt{i} using fixedE
        fixedEargs=setarg('fixedE_LPind',LPind{i},fixedEargs);
        optLL(i)=FOCOMMEMCV(D,fixedE_COMM{i},fixedE_EMconverge,folds,fixedEargs{:});
    else
        optLL(i)=FOCOMMEMCV(D,opt{i},EMconverge,folds,COMMEMargs{:});
    end
end

seg_COMM=opt{end};
LL=FOCOMMEMCV(D,opt{end},EMconverge,folds,COMMEMargs{:});