function Pn=normalize(P, dim)

% normalize probability distribution 'P' along dimension 'dim'

repmat_size=ones([1 ndims(P)]);
repmat_size(dim)=size(P,dim);

% find normalizing constants
norm=sum(P,dim);

% if NC is zero set to a uniform distribution
zero_idx=find(0==norm);
norm(zero_idx)=1;
new_dist=zeros(size(norm));
new_dist(zero_idx)=1/size(P,dim);
new_dist=repmat(new_dist,repmat_size);
P=P+new_dist;

% normalize
Pn=P./repmat(norm,repmat_size);