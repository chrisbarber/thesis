function bwdVals = bwdCOMM(sequence, LPind, COMM)

%
%   Compute backward variable in a Chain of Mixture Models
%   -~==================================================~-
%
% Inputs
%
%   sequence is a single sequence used for the calculation
%
%   LPind{i}(k) is the precomputed value of Pr(S_i|Z_i=k)
%
%   COMM is the chain of mixture models (array of mixture models, except
%     the component_probs are conditioned on the components of the prec-
%     eding mixture)
%
% Outputs
%
%   bwdVals is the natural logarithm of the probabilities given by the
%     backward algorithm.  It is a cell array of vectors in which
%     bwdVals{i}(j) gives the backward variable at position i and state j.
%

% find the number of segments (mixture models) in COMM
num_seg=length(COMM);

LP=0; % log probability of sequence; initialize to log(1)

%initial step; b_k(num_seg)=1
b=cell([1 num_seg]);
b{num_seg}=zeros([1 COMM(num_seg).num_components]);

% recurrence
for i=num_seg-1:-1:1
    % compute b_k(i)=sum_l{Pr(S_i+1|Z_i+1=l)*b_l(i+1)*Pr(Z_i+1=l|Z_i=k)}
    b{i}=repmat(b{i+1}',[1 COMM(i).num_components]);
    b{i}=b{i}+COMM(i+1).component_probs;
    b{i}=b{i}+repmat(LPind{i+1}',[1 COMM(i).num_components]);
    b{i}=logsum(b{i},1);    
end

bwdVals=b;