function [LP LPseq fwdVals] = LPCOMM(D, LPind, COMM, varargin)

%
%   LPCOMM - Log Probability for a "Chain of Mixture Models"
%   -~====================================================~-
%     Computes log probability of sequences in D given a
%     "COMM," which is like a Hidden Markov Model where the
%     hidden variables are components of mixture models.
%
% Inputs
%
%   D is a set of sequences.  See function header in MMEM for format.
%
%   LPind{i}(k) is the precomputed value of Pr(S_i|Z_i=k)
%
%   COMM is a cell array of mixture models structures having at least
%     the following members:
%    .component_probs are the component probabilities, which are
%      conditional distributions over components of the preceding
%      MM.  component_probs(j,k) represents the probability of state
%      j in this MM given state k in the preceding MM.
%    .E is the CPD of observed variables over components; see func.
%      header in MMEM for format.
%    .startpos is the first observed position in this MM
%    .endpos is the last observed position in this MM
%    .L is the length of the corresponding segment
%    .num_components is the cardinality of the MM
%    .positions is a vector == startpos:endpos
%
% Outputs
%
%   LP is the log probability of all sequences in D
%
%   LPseq(j) is the log probability of the jth sequence D(j,:)
%
%   fwdVals(j) are the forward algorithm values used to compute the
%     other outputs.  This is convenient if the calling function
%     wants to reuse the forward values.
%

[num_seq seq_len]=size(D);
num_seg=length(COMM);

[use_single_seg single_seg]=getarg('single_seg',varargin);

% calculate LP

LP=0; % initialize to log(1)
LPseq=zeros([1 num_seq]);
fwdVals=cell([1 num_seq]);
for j=1:num_seq
    if ~use_single_seg
        f=fwdCOMM(D(j,:),LPind(j,:),COMM);
        fwdVals{j}=f;
        LPseq(j)=logsum(f{num_seg});
    else
        f=fwdCOMM(D(j,:),LPind(j,:),COMM);
        b=bwdCOMM(D(j,:),LPind(j,:),COMM);
        pr_seq_j=logsum(f{end});
        component_probs=f{single_seg}+b{single_seg}-pr_seq_j;
        temp_LPseq=LPind{j,single_seg}+component_probs;
        LPseq(j)=logsum(temp_LPseq);
    end
    LP=LP+LPseq(j);
end