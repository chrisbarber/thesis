function cv_out = CV(D, f, f_args, varargin)

%
% -~= Generic cross-validation =~-
%
%   Inputs:
%
%     ~ D is the data-set; folds occur in the 1st-dim i.e.
%     D(<test-set-indices>,:,:,...,:)
%     ~ f is a handle to the function that operates on each fold; it must
%     take a training set, a test set, and any number of other arguments
%     which are invariant across the folds
%     ~ f_args are the arguments to be passed to f besides the training and
%     test sets
%   (optional)
%     ~ 'num_folds' is the number of folds desired
%     ~ 'use_rand' specifies whether to arrange data randomly
%     OR
%     ~ 'folds' specifies the folds explicitly
%
%   Outputs:
%
%     ~ cv_out is a cell array containing the output of 'f' for each fold
%

num_datapoints=size(D,1);

[arg_num_folds num_folds]=getarg('num_folds',varargin{:});
[arg_use_rand use_rand]=getarg('use_rand',varargin{:});
[arg_folds folds]=getarg('folds',varargin{:});

if ~arg_folds
    if ~arg_use_rand
        use_rand=false;
    end
    folds=CVfolds(num_datapoints,num_folds,use_rand);
else
    num_folds=max(folds);
end

cv_out=cell([1 num_folds]);
for fold=1:num_folds
    % compute test set and training set; this is convoluted by the fact
    % that we must handle D with arbitrary number of dimensions
    testIdx=find(folds==fold);
    trainingIdx=setdiff(1:num_seq,testIdx{fold});
    
    testSetSize=size(D);
    testSetSize(1)=length(testIdx);
    trainingSetSize=size(D);
    trainingSetSize(1)=length(trainingIdx);
    
    testSet=D(testIdx,:);
    testSet=reshape(testSet,testSetSize);
    trainingSet=D(trainingIdx,:);
    trainingSet=reshape(trainingSet,trainingSetSize);
    
    % call function for this fold
    cv_out{fold}=f(trainingSet,testSet,f_args);
end
