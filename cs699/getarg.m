function [argfound arg new_argList] = getarg(stringName, argList)

for i=1:length(argList)-1
    if strcmp(stringName,argList{i})
        argfound=true;
        arg=argList{i+1};
        new_argList=argList([1:i-1 i+2:end]);
        return
    end
end

argfound=false;
arg=[];
new_argList=argList;