function FOMM = COMM2FOMM(COMM)

% converts a COMM to a FOMM using the same segmentation.  note that
%   in general the FOMM will not be equivalent.

FOMM=COMM;
for i=2:length(FOMM)
    new_component_probs=FOMM(i).component_probs;
    new_component_probs=logsum(new_component_probs, 2);
    new_component_probs=lognorm(new_component_probs, 1);
    FOMM(i).component_probs=new_component_probs;
end
