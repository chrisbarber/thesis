function [LP LPseq] = LPFOCOMM(D, LPind, FOCOMM)

%
%   LPFOCOMM - Log Probability for a "Family of Chains of
%                      Mixture Models"
%   -~=================================================~-
%     Computes log probability of sequences in D given a
%     "FOCOMM," which is like a family of Hidden Markov
%     Models where the hidden variables are components of
%     mixture models.
%
% Inputs
%
%   D is a set of sequences.  See function header in MMEM for format.
%
%   LPind{i}(k) is the precomputed value of Pr(S_i|Z_i=k)
%
%   FOCOMM is a cell array of mixture models structures having at least
%     the following members:
%    .component_probs are the component probabilities, which are either:
%      * conditional distributions over components of the preceding
%      MM.  component_probs(j,k) then represents the probability of state
%      j in this MM given state k in the preceding MM.
%      * marginal distribution.  component_probs(j) is the probability of
%      state j in this MM.
%    .E is the CPD of observed variables over components; see func.
%      header in MMEM for format.
%    .startpos is the first observed position in this MM
%    .endpos is the last observed position in this MM
%    .L is the length of the corresponding segment
%    .num_components is the cardinality of the MM
%    .positions is a vector == startpos:endpos
%
% Outputs
%
%   LP is the log probability of all sequences in D
%
%   LPseq(j) is the log probability of the jth sequence D(j,:)
%

[num_seq seq_len]=size(D);
num_seg=length(FOCOMM);

chains=findFOCOMMchains(FOCOMM);

% calculate LP

LP=0; % initialize to log(1)
LPseq=zeros([1 num_seq]);
for j=1:num_seq
    chain=1;
    for x=1:length(chains)
        f=fwdCOMM(D(j,:),LPind(j,chains{x}),FOCOMM(chains{x}));
        LPseq(j)=LPseq(j)+logsum(f{end});
    end
    LP=LP+LPseq(j);
end
