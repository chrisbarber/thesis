function new_D = missingData(D, FOCOMM)

[num_seq seq_len]=size(D);
new_D=D;
num_seg=length(FOCOMM);
LPind=LPCOMMind(D,FOCOMM);
chains=findFOCOMMchains(FOCOMM);

for j=1:num_seq
    for y=1:length(chains)
        f=fwdCOMM(D(j,:),LPind(j,chains{y}),FOCOMM(chains{y}));
        b=bwdCOMM(D(j,:),LPind(j,chains{y}),FOCOMM(chains{y}));
        pr_seq_j=logsum(f{end});
        for i=1:length(chains{y})
            component_probs=f{i}+b{i}-pr_seq_j;
            if 1==size(FOCOMM(chains{y}(i)).component_probs,2)
                currState=min(find(rand<cumsum(exp(component_probs))));
            else
                currState=min(find(rand<cumsum(exp(component_probs))));
            end
            for x=find(0==D(j,FOCOMM(chains{y}(i)).positions))
                x_inseq=x+FOCOMM(chains{y}(i)).startpos-1;
                new_D(j,x_inseq)=min(find(rand<cumsum(exp(FOCOMM(chains{y}(i)).E(x,:,currState)))));
            end
        end
    end
end