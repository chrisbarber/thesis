function [LP LPind] = LPMM(D, mm)

%
%         LPMM - Log Probability for a Mixture Model
%         -~======================================~-
%           Computes log probability of sequences
%           D given a mixture model mm.
%
% Inputs
%
%   D is a matrix where each row is a sequence.  Zeros are missing data.
%
%   mm is a mixture model.  It is a structure that should have at
%     least the following fields:
%       .component_probs
%       .E (emission probabilities)
%
% Outputs
%
%   LP is the log probability of all sequences in D
%
%   LPind contains the log probabilities of each sequence in D,
%     given each mixture component.  LPind{j}(z) specifies the log
%     probability of sequence j given component z.
%

% read model parameters into temporary variables (struct accesses are slow)
component_probs=mm.component_probs;
E=mm.E;

% trim down sequence data to what is needed if there is extra
D=D(:,mm.positions);

% find # of sequences & their length
if isnumeric(D) && (2==ndims(D))
    [num_seq, seq_len]=size(D);
else
    error('D must be a 2-dimensional integer array');
end

% find # of symbols
num_sym=size(E,2);

% calculate LL(j,z)=log(Pr(seq_j|Z=z,mm)) where mm is the model

% HERE IS TOTALLY VECTORIZED VERSION OF THE ITERATIVE CODE BELOW
% This code is much slower than the iterative version.  It would
% likely run faster in an environment supporting vector-processing.

%probs_used=repmat(D,[1 1 mm.num_components num_sym+1]);
%sym_table(1,1,1,:)=0:num_sym;
%sym_table=repmat(sym_table,[num_seq seq_len mm.num_components 1]);
%probs_used=(sym_table==probs_used);
%tempE=cat(2,zeros([seq_len 1 mm.num_components]),E);
%tempE=repmat(tempE,[1 1 1 num_seq]);
%tempE=permute(tempE,[4 1 3 2]);
%probs_used=logical(probs_used);
%probs=zeros(size(probs_used));
%probs(probs_used)=tempE(probs_used);
%LP=sum(probs,4);
%LP=sum(LP,2);
%LP=permute(LP,[1 3 2]);

LP=zeros([num_seq mm.num_components]);
for z=1:mm.num_components
    for j=1:num_seq
        for i=find(D(j,:))
            LP(j,z)=LP(j,z)+E(i,D(j,i),z);
        end
    end
end
LPind=LP;                % save individual probabilities
% would use logsum below but it is much slower
log_alpha=-max(LP(:));   % constant factor to ease calculations
LP=exp(LP+log_alpha);    % LP(j,z)=alpha*Pr(seq_j|Z=z,M)
LP=LP .* repmat(exp(component_probs)',[num_seq 1]); % LP(j,z)=alpha*Pr(seq_j|Z=z,M)Pr(Z=z|M)
LP=sum(LP,2);                     % LP(j)=alpha*Pr(seq_j|M)
LP=log(LP)-log_alpha;            % LP(j)=log(Pr(seq_j|M))
LP=sum(LP);                       % LP=log(Pr(D|M))