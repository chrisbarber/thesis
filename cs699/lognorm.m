function Pn=lognorm(P, dim)

% normalize probability distribution 'P' along dimension 'dim'
% when P is given in log-space

Pn=exp(P-max(P(:)));    % Pn=alpha*P
Pn=normalize(Pn, dim);  % Pn=NC*P
Pn=log(Pn);            % Pn=log(NC*P)
