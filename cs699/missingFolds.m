for trial=1:1000
    display(trial);

    % choose random missing data
    D1_missing=find(~reallymissing);
    D1_missing=D1_missing(randperm(length(D1_missing)));
    D1_missing=D1_missing(1:ceil(0.01*length(D1_missing)));
    D1_missing=sort(D1_missing);
    D1=D;
    D1(D1_missing)=0;

    D5_missing=find(~reallymissing);
    D5_missing=D5_missing(randperm(length(D5_missing)));
    D5_missing=D5_missing(1:ceil(0.01*length(D5_missing)));
    D5_missing=sort(D5_missing);
    D5=D;
    D5(D5_missing)=0;

    D10_missing=find(~reallymissing);
    D10_missing=D10_missing(randperm(length(D10_missing)));
    D10_missing=D10_missing(1:ceil(0.01*length(D10_missing)));
    D10_missing=sort(D10_missing);
    D10=D;
    D10(D10_missing)=0;

    jpredict_D1=D1;
    jpredict_D5=D5;
    jpredict_D10=D10;
    for f=1:10    
        % predict missing data
        seqs=find(f==PatilSmall.test_folds);
        jpredict_D1(seqs,:)=missingData(D1(seqs,:),joe2chris(PatilSmall.models{f}));
        jpredict_D5(seqs,:)=missingData(D5(seqs,:),joe2chris(PatilSmall.models{f}));
        jpredict_D10(seqs,:)=missingData(D10(seqs,:),joe2chris(PatilSmall.models{f}));
    end
    jerror1(trial)=length(find(jpredict_D1(D1_missing)~=D(D1_missing)))/length(D1_missing);
    jerror5(trial)=length(find(jpredict_D5(D5_missing)~=D(D5_missing)))/length(D5_missing);
    jerror10(trial)=length(find(jpredict_D10(D10_missing)~=D(D10_missing)))/length(D10_missing);
    
    cpredict_D1=D1;
    cpredict_D5=D5;
    cpredict_D10=D10;
    for f=1:10    
        % predict missing data
        seqs=find(f==PatilSmall.test_folds);
        cpredict_D1(seqs,:)=missingData(D1(seqs,:),models{f});
        cpredict_D5(seqs,:)=missingData(D5(seqs,:),models{f});
        cpredict_D10(seqs,:)=missingData(D10(seqs,:),models{f});
    end
    cerror1(trial)=length(find(cpredict_D1(D1_missing)~=D(D1_missing)))/length(D1_missing);
    cerror5(trial)=length(find(cpredict_D5(D5_missing)~=D(D5_missing)))/length(D5_missing);
    cerror10(trial)=length(find(cpredict_D10(D10_missing)~=D(D10_missing)))/length(D10_missing);
end
jerror1=mean(jerror1)
jerror5=mean(jerror5)
jerror10=mean(jerror10)
cerror1=mean(cerror1)
cerror5=mean(cerror5)
cerror10=mean(cerror10)
