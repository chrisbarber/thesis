function [LL testSet_loglike new_FOCOMMs Stats] = FOCOMMEMCV(D, FOCOMM, c, folds, varargin)

%
%         FOCOMMEMCV - Cross-Validated EM for a
%           Family of Chains Of Mixture Models
%         -~=================================~-
%
% Inputs
%
%   D, FOCOMM, c, & varargin are simply passed on to FOCOMMEM().  See
%     COMMEM.m function header descriptions of these parameters.
%
%   folds(j) specifies the fold in which sequence j should belong
%     to the test-set.  'folds' must be specified if 'k' is not.
%
% Outputs
%
%   testSet_loglike(j) is the log-likelihood of the sequence j, given the
%     model generated when sequence j was in the test-set
%
%   new_FOCOMMs{i} is the FOCOMM generated during the ith fold.
%
%   Stats{i} is the 'Stats' structure returned by FOCOMMEM() during the ith
%     fold.  See COMMEM.m function header for a description.
%

[num_seq seq_len]=size(D);

[useFixedE fixedE_LPind otherargs]=getarg('fixedE_LPind',varargin);

% reproduce folds from 'folds' parameter
k=max(folds(:));
for fold=1:k
    testSetIndices{fold}=find(folds==fold);
    trainingSetIndices{fold}=setdiff(1:num_seq,testSetIndices{fold});
end

if k<=1 || k>num_seq
    error('k must be between 1 and the number of sequences');
end

testSet_loglike=zeros([1 num_seq]);
new_FOCOMMs=cell([1 k]);
Stats=cell([1 k]);
for fold=1:k
    % train the model on the training-set
    if iscell(FOCOMM)
        temp_FOCOMM=FOCOMM{fold};
    else
        temp_FOCOMM=FOCOMM;
    end
    if useFixedE
        tempargs={otherargs, 'fixedE_LPind', fixedE_LPind{fold}};
    else
        tempargs=varargin;
    end
    [new_FOCOMMs{fold} Stats{fold}]=FOCOMMEM(D(trainingSetIndices{fold},:),temp_FOCOMM,c,tempargs{:});
    % evaluate log-likelihood of the test-set
    LPind=LPCOMMind(D(testSetIndices{fold},:),new_FOCOMMs{fold});
    [junk testSet_loglike(testSetIndices{fold})]=LPFOCOMM(D(testSetIndices{fold},:),LPind,new_FOCOMMs{fold});
end

LL=sum(testSet_loglike);