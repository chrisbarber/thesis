function fwdVals = fwdCOMM(sequence, LPind, COMM)

%
%   Compute forward variable in a Chain of Mixture Models
%   -~=================================================~-
%
% Inputs
%
%   sequence is a single sequence used for the calculation
%
%   LPind{i}(k) is the precomputed value of Pr(S_i|Z_i=k)
%
%   COMM is the chain of mixture models (array of mixture models, except
%     the component_probs are conditioned on the components of the prec-
%     eding mixture).  For the first mixture model, component_probs is
%     not a conditional distribution.
%
% Outputs
%
%   fwdVals is the natural logarithm of the probabilities given by the
%     forward algorithm.  It is a cell array of vectors in which
%     fwdVals{i}(j) gives the forward variable at position i and state j.
%

% find the number of segments (mixture models) in COMM
num_seg=length(COMM);

LP=0; % log probability of sequence; initialize to log(1)

%initial step; f_l(1)=Pr(S_1|Z_1=l)*Pr(Z_1=l)
f=cell([1 num_seg]);
f{1}=(LPind{1}(:)+COMM(1).component_probs)';

% recurrence
for i=2:num_seg
    % compute f_l(i)=Pr(S_i|Z_i=l)*sum_k{f_k(i-1)*Pr(Z_i=l|Z_i-1=k)}
    
    % calculate summation part
    f{i}=repmat(f{i-1},[COMM(i).num_components 1]);
    f{i}=f{i}+COMM(i).component_probs;
    f{i}=logsum(f{i},2)';
    % multiply by first factor Pr(S_i|Z_i=l)
    f{i}=f{i}+LPind{i};
end

fwdVals=f;