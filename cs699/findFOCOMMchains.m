function chains = findFOCOMMchains(FOCOMM)

num_seg=length(FOCOMM);

numChains=0;
for i=1:num_seg
    if 1==size(FOCOMM(i).component_probs,2)
        numChains=numChains+1;
        chains{numChains}=[];
    end
    chains{numChains}=[chains{numChains} i];
end